<?php
set_time_limit(120);
ini_set('max_execution_time', 120);
umask(0);

setlocale(LC_MONETARY, 'en_US');

date_default_timezone_set('Europe/Paris');

//defined('BASE_PATH')
//    || define('BASE_PATH', realpath(dirname(__FILE__)));

defined('DS')
    || define('DS', DIRECTORY_SEPARATOR);

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . DS . 'app'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure lib/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    get_include_path(),
    realpath(APPLICATION_PATH . DS . 'modules'),
    realpath(APPLICATION_PATH . DS . '..' . DS . 'lib'),
)));

/** Zend_Application */
require_once 'Zend/Application.php';
$ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';

$config=parse_ini_file($ini);


//mysql connexion
//define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
//define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
//define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
//define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
//define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));

//$dbhost = constant("DB_HOST"); // Host name 
//$dbport = constant("DB_PORT"); // Host port
//$dbusername = constant("DB_USER"); // Mysql username 
//$dbpassword = constant("DB_PASS"); // Mysql password 
//$db_name = constant("DB_NAME"); // Database name 


//function put_ini_file($file, $array, $i = 0){
  //$str="";
  //foreach ($array as $k => $v){
    //if (is_array($v)){
      //$str.=str_repeat(" ",$i*2)."[$k]\r\n";
      //$str.=put_ini_file("",$v, $i+1);
    //}else
     // $str.=str_repeat(" ",$i*2)."$k = $v\r\n";
  //}
  
  //$phpstr = "[production]\r\n".$str;
  
//if($file)
  //  return file_put_contents($file,$phpstr);
  //else
   // return $str;
//}
  
//if((isset($dbhost) and $dbhost!=false) and (isset($dbport) and $dbport!=false))
//{
//	$config["resources.db.params.host"] = $dbhost.":".$dbport;
 // put_ini_file($ini, $config);
//} else {
 //  $config["resources.db.params.host"] = $config["resources.db.params.host"];
  // put_ini_file($ini, $config);
//}


// Create application
$ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
$application = new Zend_Application(
    APPLICATION_ENV,
    $ini
);


session_cache_limiter(false);

// Run
$application->bootstrap()->run();
