//
//  Apps_Mobile_CompanyUITests.swift
//  Apps Mobile CompanyUITests
//
//  Created by Florent BEGUE on 07/12/15.
//  Copyright © 2015 Adrien Sala. All rights reserved.
//

import XCTest

class Apps_Mobile_CompanyUITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        setLanguage(app)
        app.launchArguments.append("MakeASmile");
        app.launch();
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSnapshots() {
        
        let app = XCUIApplication();
        
        while(app.activityIndicators.elementBoundByIndex(0).hittable) {
            // Wait for the loader to hide
            app.tap();
            if(app.alerts.element.collectionViews.buttons["OK"].exists) {
                app.alerts.element.collectionViews.buttons["OK"].tap();
            } else if(app.alerts.element.collectionViews.buttons["Allow"].exists) {
                app.alerts.element.collectionViews.buttons["Allow"].tap();
            }
        }
        sleep(1)
        snapshot("HomepageSnapshot")

        let max_snapshots : Int = 4;
        var i : Int = 0;
        
        while(i < max_snapshots) {
            sleep(6);
            print("snapshot");
            snapshot(String(i) + "Snapshot")
            i++;
        }
    }
    
}
