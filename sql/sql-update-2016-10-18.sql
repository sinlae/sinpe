
alter table website add promote_app TINYINT(1) default 1 after name;
alter table website add photo_slider TINYINT(0) default 0 after promote_app;
alter table website add video_description TINYINT(0) default 0 after photo_slider;
alter table website add video TINYINT(0) default 0 after photo_slider;


CREATE TABLE website_video_description (
video_id INT(11) NOT NULL AUTO_INCREMENT,
website_id INT(11) NOT NULL,
youtube_url VARCHAR(255),
PRIMARY KEY (`video_id`)
);

CREATE TABLE website_slider_image (
image_id INT(11) NOT NULL AUTO_INCREMENT,
website_id INT(11) NOT NULL,
image_url VARCHAR(255),
PRIMARY KEY (`image_id`)
);

alter table website add domain VARCHAR(255) after name;

CREATE TABLE event_price (
price_id INT(11) NOT NULL AUTO_INCREMENT,
event_id INT(11) unsigned NOT NULL,
title VARCHAR(255),
price FLOAT(22),
PRIMARY KEY (`price_id`)
);

CREATE TABLE website_newsletter_contact (
contact_id INT(11) NOT NULL AUTO_INCREMENT,
app_id INT(11) NOT NULL,
name VARCHAR(255),
firstname VARCHAR(255),
email VARCHAR(255),
PRIMARY KEY (`contact_id`)
);

alter table website add newsletter_subscription TINYINT(0) default 0 after domain;

alter table application add no_limit_user TINYINT(0) default 0 after is_locked;

ALTER TABLE `website` MODIFY `app_id` int(11) unsigned NOT NULL;
ALTER TABLE `cms_application_page_block_image_library` MODIFY `library_id` int(11) unsigned NOT NULL;
ALTER TABLE `customer` MODIFY `app_id` int(11) unsigned NOT NULL;
ALTER TABLE `website` ADD FOREIGN KEY `FK_WEBSITE` (`app_id`) REFERENCES `application` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `website_newsletter_contact` MODIFY `app_id` int(11) unsigned NOT NULL;
ALTER TABLE `social_twitter` MODIFY `value_id` int(11) unsigned NOT NULL;
ALTER TABLE `website_newsletter_contact` ADD FOREIGN KEY `FK_NEWSLETTER_WEBSITE` (`app_id`) REFERENCES `application` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `customer` ADD FOREIGN KEY `FK_CUSTOMER` (`app_id`) REFERENCES `application` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `event_custom` ADD FOREIGN KEY `FK_EVENTPRICE_ID` (`event_price.event_id`) REFERENCES `event` (`event.event_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `website_slider_image` ADD FOREIGN KEY `FK_WEBSITE_SLIDER_IMAGE` (`website_id`) REFERENCES `website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `website_social_content` ADD FOREIGN KEY `FK_WEBSITE_SOCIAL_CONTENT` (`website_id`) REFERENCES `website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `website_block_detail` ADD FOREIGN KEY `FK_WEBSITE_BLOCK_DETAIL` (`website_id`) REFERENCES `website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `website_video_description` ADD FOREIGN KEY `FK_WEBSITE_VIDEO` (`website_id`) REFERENCES `website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `weblink` ADD FOREIGN KEY `FK_WEBLINK` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `social_twitter` ADD FOREIGN KEY `FK_TWITTER` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `social_facebook` ADD FOREIGN KEY `FK_TWITTER` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `radio` ADD FOREIGN KEY `FK_RADIO` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `media_gallery_music` ADD FOREIGN KEY `FK_MUSIC` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `media_gallery_image_instagram` ADD FOREIGN KEY `FK_IMAGE_INSTAGRAM` (`gallery_id`) REFERENCES `media_gallery_image` (`gallery_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `event` ADD FOREIGN KEY `FK_EVENT` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `cms_application_page_block_image_library` ADD FOREIGN KEY `FK_CMS_BLOCK_IMAGE_LIBRARY` (`library_id`) REFERENCES `cms_application_page_block_image` (`library_id`) ON DELETE CASCADE ON UPDATE CASCADE;

