CREATE TABLE website_social (
social_id INT(11) NOT NULL AUTO_INCREMENT,
name VARCHAR(255),
image VARCHAR(255),
PRIMARY KEY (social_id)
);

insert into website_social (name,image) VALUES ("facebook","pictos/facebooklogo.png");
insert into website_social (name,image) VALUES ("twitter","pictos/twitterlogo.png");
insert into website_social (name,image) VALUES ("instagram","pictos/instagramlogo.png");
insert into website_social (name,image) VALUES ("youtube","pictos/youtubelogo.png");
insert into website_social (name,image) VALUES ("deezer","pictos/deezerlogo.png");
insert into website_social (name,image) VALUES ("spotify","pictos/spotifylogo.png");
insert into website_social (name,image) VALUES ("soundcloud","pictos/soundcloudlogo.png");
insert into website_social (name,image) VALUES ("vimeo","pictos/vimeologo.png");

CREATE TABLE website_social_content (
social_content_id INT(11) NOT NULL AUTO_INCREMENT,
website_id INT(11) NOT NULL,
social_id INT(11) NOT NULL,
social_url VARCHAR(255),
PRIMARY KEY (social_content_id)
);