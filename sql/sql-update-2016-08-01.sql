insert into acl_resource (parent_id, code, label, url) VALUES (22, "feature_twitter", "Twitter", "social/application_twitter/*");

insert into application_option (category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values (1, 23, 134, "twitter", "Twitter", "Social_Model_Twitter", "social/application_twitter/", "social/mobile_twitter_list/", NULL, NULL, 0, 1 ,210,0);	

CREATE TABLE social_twitter (
twitter_id INT(11) NOT NULL AUTO_INCREMENT,
value_id INT(11) NOT NULL,	
twitter_user varchar(255),
created_at datetime,
updated_at datetime,
PRIMARY KEY (twitter_id)
);


/*TWITTER*/
insert into media_library (name) VALUES ("Twiter")
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (42, "/social_twitter/twitter1.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (42, "/social_twitter/twitter2.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (42, "/social_twitter/twitter3.png", NULL,NULL, NULL, NULL, 1, 0 );
/*FACEBOOK*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (23, "/social_facebook/facebook2.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (23, "/social_facebook/facebook3.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (23, "/social_facebook/facebook4.png", NULL,NULL, NULL, NULL, 1, 0 );
/*CALENDAR*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (11, "/calendar/calendar4.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (11, "/calendar/calendar5.png", NULL,NULL, NULL, NULL, 1, 0 );
/*CUSTOM*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (6, "/custom_page/custom2.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (6, "/custom_page/custom3.png", NULL,NULL, NULL, NULL, 1, 0 );

update media_library_image SET link='/catalog/catalog5.png' WHERE image_id=54;
delete from media_library_image WHERE image_id=47; 
delete from media_library_image WHERE image_id=55;
delete from media_library_image WHERE image_id=56;
/*SOURCE CODE*/
update media_library_image SET link='/catalog/catalog5.png' WHERE image_id=143;
/*FANWALL*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (9, "/fanwall/fanwall5.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (9, "/fanwall/fanwall6.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (9, "/fanwall/fanwall7.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (9, "/fanwall/fanwall8.png", NULL,NULL, NULL, NULL, 1, 0 );
/*MUSIC*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music10.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music11.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music12.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music13.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music14.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music15.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music16.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (3, "/musics/music17.png", NULL,NULL, NULL, NULL, 1, 0 );
/*PUSHS NOTIFICATIONS*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (19, "/push_notifications/push6.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (19, "/push_notifications/push7.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (19, "/push_notifications/push8.png", NULL,NULL, NULL, NULL, 1, 0 );
delete from media_library_image WHERE image_id=117;
/*RSS FEED*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (22, "/rss_feed/rss4.png", NULL,NULL, NULL, NULL, 1, 0 );
/*ACCOUNT*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (41, "/tabbar/user_account1.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (41, "/tabbar/user_account2.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (41, "/tabbar/user_account3.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (41, "/tabbar/user_account4.png", NULL,NULL, NULL, NULL, 1, 0 );
/*WRDPRESS*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (35, "/wordpress/wordpress2.png", NULL,NULL, NULL, NULL, 1, 0 );
/*WEBLINK*/
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link4.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link5.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link6.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link7.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link8.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link9.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link10.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link11.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link12.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link13.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link14.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link15.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link16.png", NULL,NULL, NULL, NULL, 1, 0 );
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link17.png", NULL,NULL, NULL, NULL, 1, 0 );