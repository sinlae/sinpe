
insert into acl_resource (resource_id, parent_id, code, label, url) VALUES ("", 22, "feature_submenu", "Submenu", "weblink/application_multi/*");

insert into application_option (option_id, category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values ("", 6, 29, 155, "submenu", "Submenu", "Weblink_Model_Type_Multi", "weblink/application_multi/", "weblink/mobile_multi/", NULL, NULL, 0, 1, 160 ,0);	


insert into acl_resource (resource_id, parent_id, code, label, url) VALUES ("", 22, "feature_submenuplus", "SubmenuPlus", "weblink/application_multi/*");

insert into application_option (option_id, category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values ("", 6, 29, 155, "submenuplus", "SubmenuPlus", "Weblink_Model_Type_Multi", "weblink/application_multi/", "weblink/mobile_multi/", NULL, NULL, 0, 1, 160 ,0);	