<?php  

//get config
$config=parse_ini_file('app/configs/app.ini');

//mysql connexion
define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));

$dbhost = constant("DB_HOST"); // Host name 
$dbport = constant("DB_PORT"); // Host port
$dbusername = constant("DB_USER"); // Mysql username 
$dbpassword = constant("DB_PASS"); // Mysql password 
$db_name = constant("DB_NAME"); // Database name 



if(isset($dbhost) and $dbhost!=false)
{	
  $mysqli = new mysqli($dbhost, $dbusername, $dbpassword, $db_name, $dbport);
} else {
$mysqli = new mysqli($config["resources.db.params.host"], $config["resources.db.params.username"], $config["resources.db.params.password"], $config["resources.db.params.dbname"]);
}
//create new design, add images in /images/design_name
$mysqli->query("INSERT INTO `template_design` (`layout_id`,`layout_visibility`,`code`,`name`,`overview`,
`background_image`,
`background_image_hd`,
`background_image_tablet`,
`icon`,
`startup_image`,
`startup_image_retina`,
`startup_image_iphone_6`,
`startup_image_iphone_6_plus`,
`startup_image_ipad_retina`) 
	VALUES (1, 'homepage', 'blackandwhite', 'BlackAndWhite', '/blackandwhite/overview.png',
 '/../../images/templates/blackandwhite/640x1136.jpg', 
 '/../../images/templates/blackandwhite/1242x2208.jpg',
 '/../../images/templates/blackandwhite/1536x2048.jpg', 
 '/../../images/templates/blackandwhite/180x180.png', 
 '/../../images/templates/blackandwhite/640x960.png', 
 '/../../images/templates/blackandwhite/640x1136.jpg',
 '/../../images/templates/blackandwhite/750x1334.png', 
 '/../../images/templates/blackandwhite/1242x2208.jpg', 
 '/../../images/templates/blackandwhite/1536x2048.jpg');
");

//get design_id of the design created
$design_id = $mysqli->query("select `design_id` from `template_design` where `code`='blackandwhite';");
$design_id = $design_id->fetch_object()->design_id ;


//get entertainement category_id (for the other category change the code)
$entertainment_design_category_id = $mysqli->query("select `category_id` from `template_category` where `code`='entertainment';");
$entertainment_design_category_id = $entertainment_design_category_id->fetch_object()->category_id ;
$mysqli->query("INSERT INTO `template_design_category` (`design_id`, `category_id`) 
VALUES ('".$design_id."','".$entertainment_design_category_id."');");


//block_template
$header_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='header' and `type_id`='1' limit 1");
$subheader_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='subheader' and `type_id`='1' limit 1");
$connect_button_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='connect_button' and `type_id`='1' limit 1");
$background_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='background' and `type_id`='1' limit 1");
$discount_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='discount' and `type_id`='1' limit 1");
$button_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='button' and `type_id`='1' limit 1");
$news_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='news' and `type_id`='1' limit 1");
$comments_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='comments' and `type_id`='1' limit 1");
$tabbar_block_id = $mysqli->query("select `block_id` from `template_block` where `code`='tabbar' and `type_id`='1' limit 1");

$header_block_id = $header_block_id->fetch_object()->block_id ;
$subheader_block_id = $subheader_block_id->fetch_object()->block_id ;
$connect_button_block_id = $connect_button_block_id->fetch_object()->block_id ;
$background_block_id = $background_block_id->fetch_object()->block_id ;
$discount_block_id = $discount_block_id->fetch_object()->block_id ;
$button_block_id = $button_block_id->fetch_object()->block_id ;
$news_block_id = $news_block_id->fetch_object()->block_id ;
$comments_block_id = $comments_block_id->fetch_object()->block_id ;
$tabbar_block_id = $tabbar_block_id->fetch_object()->block_id ;

//insert default colors for the new design


// 1 -block template  header
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$header_block_id."', '#000000', '#ffffff', 'NULL');");

// 2 -block template subheader
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$subheader_block_id."', '#000000', '#ffffff', 'NULL');");

// 3 - block template connect button 
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$connect_button_block_id."', '#6fb7b1', '#323b40', 'NULL');");

// 4 - block template background 
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$background_block_id."', '#ffffff', '#000000', 'NULL');");

// 5 - block template discount
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$discount_block_id."', '#f9e4d1', '#ee4b63', 'NULL');");

// 6 - block template button
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$button_block_id."', '#000000', '#ffffff', 'NULL');");

// 7 - block template news
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$news_block_id."', '#000000', '#ffffff', 'NULL');");

// 8 - block template comment
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$comments_block_id."', '##969592', '#ffffff', 'NULL');");

// 9 - block template tabbar
$mysqli->query("INSERT INTO `template_design_block` (`design_id`, `block_id`, `background_color`, `color`, `image_color`) 
VALUES ('".$design_id."','".$tabbar_block_id."', '#000000', '#ffffff', '#ffffff');");


// Modules : get module option id
$booking_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='booking' limit 1");
$catalog_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='catalog' limit 1");
$cms_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='custom_page' limit 1");
$codescan_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='code_scan' limit 1");
$comment_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='newswall' limit 1");
$contact_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='contact' limit 1");
$event_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='calendar' limit 1");
$mcommerce_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='m_commerce' limit 1");
$message_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='inapp_messages' limit 1");
$push_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='push_notification' limit 1");
$radio_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='radio' limit 1");
$rss_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='rss_feed' limit 1");
$social_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='facebook' limit 1");
$sourcecode_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='source_code' limit 1");
$weather_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='weather' limit 1");
$wordpress_option_id = $mysqli->query("select `option_id` from `application_option` where `code`='wordpress' limit 1");

$booking_option_id = $booking_option_id->fetch_object()->option_id ;
$catalog_option_id = $catalog_option_id->fetch_object()->option_id ;
$cms_option_id = $cms_option_id->fetch_object()->option_id ;
$codescan_option_id = $codescan_option_id->fetch_object()->option_id ;
$comment_option_id = $comment_option_id->fetch_object()->option_id ;
$contact_option_id = $contact_option_id->fetch_object()->option_id ;
$event_option_id = $event_option_id->fetch_object()->option_id ;
$mcommerce_option_id = $mcommerce_option_id->fetch_object()->option_id ;
$message_option_id = $message_option_id->fetch_object()->option_id ;
$push_option_id = $push_option_id->fetch_object()->option_id ;
$radio_option_id = $radio_option_id->fetch_object()->option_id ;
$rss_option_id = $rss_option_id->fetch_object()->option_id ;
$social_option_id = $social_option_id->fetch_object()->option_id ;
$sourcecode_option_id = $sourcecode_option_id->fetch_object()->option_id ;
$weather_option_id = $weather_option_id->fetch_object()->option_id ;
$wordpress_option_id = $wordpress_option_id->fetch_object()->option_id ;

//set default modules for the app, for each module you choose to add you have to create data
//in associate dummy xml

$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
VALUES ('".$design_id."','".$booking_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$catalog_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$cms_option_id."'");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$codescan_option_id."');");
$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
VALUES ('".$design_id."','".$comment_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$contact_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$event_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$mcommerce_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$message_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$push_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$radio_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$rss_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$social_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$sourcecode_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$weather_option_id."');");
//$mysqli->query("INSERT INTO `template_design_content` (`design_id`, `option_id`) 
//VALUES ('".$design_id."','".$wordpress_option_id."');");


$mysqli->close();




