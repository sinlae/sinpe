alter table application add onesignal_app_id varchar(255) after `key`;
alter table application add onesignal_app_key varchar(255) after onesignal_app_id;


insert into acl_resource (parent_id, code, label, url) VALUES (22, "feature_home", "Home", "weblink/application_multi/*");

insert into application_option (category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values (6, 29, 155, "home", "HOME", "Weblink_Model_Type_Multi", "weblink/application_multi/", "weblink/mobile_multi/", NULL, NULL, 0, 1 ,160,0);	

alter table application add player_is_native  TINYINT(1) default 0 after use_ads;