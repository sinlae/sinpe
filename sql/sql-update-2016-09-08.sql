alter table application_option_value add count_view int(50) default 0 after social_sharing_is_active;

alter table media_gallery_music_track add nb_play int(50) default 0 after position;

CREATE TABLE website_block (
block_id INT(11) NOT NULL AUTO_INCREMENT,
block_type VARCHAR(255) NOT NULL,
PRIMARY KEY (block_id));

CREATE TABLE website_block_details (
website_id INT(11) NOT NULL,
block_id INT(11) NOT NULL,
name VARCHAR(255),
position INT(11) NOT NULL,
PRIMARY KEY (`website_id`, `block_id`)
);

insert into website_block (block_type) VALUES ("bio");
insert into website_block (block_type) VALUES ("image");
insert into website_block (block_type) VALUES ("video");
insert into website_block (block_type) VALUES ("event");
insert into website_block (block_type) VALUES ("music");
insert into website_block (block_type) VALUES ("shop");

ALTER TABLE website
 DROP COLUMN shop_name,
  DROP COLUMN image_name,
   DROP COLUMN music_name,
    DROP COLUMN video_name,
     DROP COLUMN event_name,
     DROP COLUMN bio_name
     ;

  insert into api_provider (code,name,icon) VALUES ("stripe","Stripe","fa-credit-card");   

  insert into api_key (`provider_id`,`key`) VALUES (7,"publish_key");
  	  insert into api_key (`provider_id`,`key`) VALUES (7,"secret_key");

  alter table subscription_application add customer_id VARCHAR(255) after payment_method;
  alter table subscription_application add stripe_id VARCHAR(255) after customer_id;
   alter table admin add customer_stripe_id VARCHAR(255) after vat_number;

   insert into system_config (code,label,value) VALUES ("company_siret", "Siret", NULL);

