update template_design_block SET color="#9f0030" where design_block_id=1;
update template_design_block SET color="#9f0030" where design_block_id=2;
update template_design_block SET background_color="#9f0030" where design_block_id=3;
update template_design_block SET background_color="#9f0030" where design_block_id=6;
update template_design_block SET color="#9f0030" where design_block_id=8;
update template_design_block SET color="#9f0030", image_color="#9f0030" where design_block_id=9;


insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (29, "/weblink/link18.png", NULL,NULL, NULL, NULL, 1, 0 );
update template_design_content SET option_icon=220 where content_id=3;

alter table media_gallery_image_instagram add api_key VARCHAR(255) after `order_by`;
alter table media_gallery_image_instagram add token VARCHAR(255) after `api_key`;

alter table media_gallery_image add api_key VARCHAR(255) after `name`;
alter table media_gallery_image add token VARCHAR(255) after `api_key`;

update applcation_option set icon_id=157 where code="weblink_multi";


insert into application_option_layout (code, option_id, template_level, preview) VALUES (22, "feature_website", "Website", NULL);

update application_option SET name="channel" where option_id=2;


