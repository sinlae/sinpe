alter table subscription add nb_subscribers int(11) after description;
alter table subscription add upload_file TINYINT(1) default 0 after nb_subscribers;
alter table subscription add template_level int(11) default 1 after upload_file;
alter table application_option_layout add template_level int(11) default 1 after option_id;

alter table website add layout_id int(11) default 1 after app_id;

alter table cms_application_page_block_video add title varchar(150) after type_id;

alter table subscription add user_account TINYINT(1) default 0 after description;