update template_design_block SET block_id=1, color="#ed0c0c", background_color="#000000" where design_block_id=1;
update template_design_block SET color="#ed0c0c", background_color="#000000" where design_block_id=2;
update template_design_block SET color="#000000", background_color="#ed0c0c" where design_block_id=3;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=4;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=5;
update template_design_block SET color="#000000", background_color="#ed0c0c" where design_block_id=6;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=7;
update template_design_block SET color="#ed0c0c", background_color="#bdbdbd" where design_block_id=8;
update template_design_block SET color="#ed0c0c", background_color="transparent", image_color="#ed0c0c" where design_block_id=9;

update template_design SET layout_id=9 where code="dj";
update template_design SET name="Festival" where code="dj";
update template_design SET layout_visibility="toggle" where code="dj";
update template_design SET layout_id=11 where code="fairground";
update template_design SET name="Stage" where code="backandwhite";

delete from template_design where design_id=30;

update template_design_content SET option_id=9, option_icon=NULL, option_tabbar_name="News" where content_id=10;
update template_design_content SET option_id=3, option_icon=NULL, option_tabbar_name="Music" where content_id=11;
update template_design_content SET option_id=2, option_icon=NULL, option_tabbar_name="Videos" where content_id=12;
update template_design_content SET option_id=12, option_icon=NULL, option_tabbar_name="Events" where content_id=13;
update template_design_content SET option_id=1, option_icon=NULL, option_tabbar_name="Photos" where content_id=14;

delete from template_design_content where content_id = 15; 
delete from template_design_content where content_id = 16; 
delete from template_design_content where content_id = 17; 
delete from template_design_content where content_id = 18;
delete from template_design_content where content_id = 19;  

update template_design_block SET block_id=1, color="#1cb5ba", background_color="#ebebeb" where design_block_id=19;
update template_design_block SET color="#1cb5ba", background_color="#ebebeb" where design_block_id=20;
update template_design_block SET color="#ebebeb", background_color="#1cb5ba" where design_block_id=21;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=22;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=23;
update template_design_block SET color="#ebebeb", background_color="#1cb5ba" where design_block_id=24;
update template_design_block SET color="#000000", background_color="#ffffff" where design_block_id=25;
update template_design_block SET color="#000000", background_color="#ebebeb" where design_block_id=26;
update template_design_block SET color="#1cb5ba", background_color="#ebebeb", image_color="#1cb5ba" where design_block_id=27;

update template_design_content SET option_id=9, option_icon=NULL, option_tabbar_name="News" where content_id=1;
update template_design_content SET option_id=12, option_icon=80, option_tabbar_name="Program" where content_id=2;
update template_design_content SET option_id=33, option_icon=156, option_tabbar_name="Tickets" where content_id=3;
update template_design_content SET option_id=3, option_icon=17, option_tabbar_name="Music" where content_id=4;
update template_design_content SET option_id=1, option_icon=5, option_tabbar_name="Pictures" where content_id=5;

delete from template_design_content where content_id = 6; 
delete from template_design_content where content_id = 7; 
delete from template_design_content where content_id = 8; 
delete from template_design_content where content_id = 9;


update template_design_block SET block_id=1, color="#5e045e", background_color="#ffffff" where design_block_id=28;
update template_design_block SET color="#5e045e", background_color="#ffffff" where design_block_id=29;
update template_design_block SET color="#ffffff", background_color="#5e045e" where design_block_id=30;
update template_design_block SET color="#ffffff", background_color="#000000" where design_block_id=31;
update template_design_block SET color="#ffffff", background_color="#5e045e" where design_block_id=32;
update template_design_block SET color="#ffffff", background_color="#000000" where design_block_id=33;
update template_design_block SET color="#ffffff", background_color="#000000" where design_block_id=34;
update template_design_block SET color="#ffffff", background_color="#5e045e" where design_block_id=35;
update template_design_block SET color="#5e045e", background_color="#ffffff", image_color="#5e045e" where design_block_id=36;

delete from template_design_block where design_block_id = 37; 
delete from template_design_block where design_block_id = 38; 
delete from template_design_block where design_block_id = 40; 

update template_design_content SET option_id=12, option_icon=80, option_tabbar_name="Ticketing" where content_id=20;
update template_design_content SET option_id=11, option_icon=72, option_tabbar_name="Useful info" where content_id=21;
update template_design_content SET option_id=26, option_icon=NULL, option_tabbar_name="Facebook" where content_id=22;

insert into template_design_content (design_id, option_id, option_tabbar_name, option_icon, option_background_image) VALUES (3,1,'Images',NULL,NULL);
insert into template_design_content (design_id, option_id, option_tabbar_name, option_icon, option_background_image) VALUES (3,2,'Videos',NULL,NULL);


