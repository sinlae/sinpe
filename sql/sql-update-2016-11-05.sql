alter table acl_resource add price  FLOAT(22) after url;

update acl_resource SET `price`=49 where `code`="feature_image_gallery";
update acl_resource SET `price`=49 where `code`="feature_video_gallery";
update acl_resource SET `price`=49 where `code`="feature_music_gallery";
update acl_resource SET `price`=49 where `code`="feature_catalog";
update acl_resource SET `price`=49 where `code`="feature_custom_page";
update acl_resource SET `price`=49 where `code`="feature_newswall";
update acl_resource SET `price`=49 where `code`="feature_fanwall";
update acl_resource SET `price`=39 where `code`="feature_contact";
update acl_resource SET `price`=49 where `code`="feature_calendar";
update acl_resource SET `price`=49 where `code`="feature_radio";
update acl_resource SET `price`=19 where `code`="feature_rss_feed";
update acl_resource SET `price`=19 where `code`="feature_facebook";
update acl_resource SET `price`=49 where `code`="feature_source_code";
update acl_resource SET `price`=49 where `code`="feature_topic";
update acl_resource SET `price`=49 where `code`="feature_weblink_multi";
update acl_resource SET `price`=19 where `code`="feature_woocommerce";
update acl_resource SET `price`=19 where `code`="feature_shopify";
update acl_resource SET `price`=49 where `code`="feature_wordpress";
update acl_resource SET `price`=49 where `code`="feature_cmsvideo";
update acl_resource SET `price`=49 where `code`="feature_submenu";
update acl_resource SET `price`=19 where `code`="feature_twitter";
update acl_resource SET `price`=49 where `code`="feature_website";

alter table subscription add app_id  INT(11) after subscription_id;

    CREATE TABLE `cms_application_page_block_video_vimeo` (
        `video_id` int(11) unsigned NOT NULL,
        `search` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `vimeo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        PRIMARY KEY (`video_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    
 ALTER TABLE `cms_application_page_block_video_vimeo`
        ADD FOREIGN KEY `FK_VIDEO_ID` (`video_id`) REFERENCES `cms_application_page_block_video` (`video_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE cms_application_page_block_video MODIFY COLUMN type_id ENUM('link','youtube','podcast','vimeo');
INSERT INTO system_config  (`code`,`label`,`value`) VALUES ("system_baseline","baseline", "Le repertoire national des scenes locales ! Musiques Actuelles")
update system_config SET value="" where `code`="system_baseline";
INSERT INTO system_config  (`code`,`label`,`value`) VALUES ("system_website","website", 1);



insert into acl_resource (parent_id, code, label, url) VALUES (22, "feature_scenes_locales", "Scenes Locales", "weblink/application_sceneslocales/*");

insert into application_option (category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values (1, 23, 134, "scenes_locales", "Scenes Locales", "Weblink_Model_Type_Mono", "weblink/application_sceneslocales/", "weblink/mobile_mono/", NULL, NULL, 0, 0 ,155,0);	

insert into media_library (name) VALUES ("Scenes Locales");
insert into media_library_image (library_id, link, secondary_link, thumbnail, option_id, app_id, can_be_colorized, position) VALUES (43, "/sceneslocales/sceneslocales.png", NULL,NULL, NULL, NULL, 1, 0 );
update application_option SET `icon`=219 where `code`="scenes_locales";
