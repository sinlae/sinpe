alter table customer add pseudo varchar(50) after civility NOT NULL;

insert into acl_resource (resource_id, parent_id, code, label, url) VALUES ("", 22, "feature_cmsvideo", "Video", "cms/applicatiom_page/*");

insert into application_option (option_id, category_id, library_id, icon_id, code, name, model, desktop_uri, mobile_uri, mobile_view_uri, mobile_view_uri_parameter, only_once, is_ajax, position, social_sharing_is_available)
Values ("", 2, 6, 8, "cmsvideo", "Video", "Cms_Model_Application_Page", "cms/application_page/", "cms/mobile_page_view/", "cms/mobile_page_view/", NULL, 0, 1, 70 ,0);	

update template_block SET color="#9F0030", background_color="#EFEFEF", color_on_hover=NULL, background_color_on_hover=NULL, 
color_on_active=NULL, background_color_on_active=NULL where block_id=10;

update template_block SET color="#FFFFFF", background_color="#404040", color_on_hover="#FFFFFF", background_color_on_hover="#404040", 
color_on_active=NULL, background_color_on_active="#404040" where block_id=11;

update template_block SET color="#FFFFFF", background_color="#9F0030", color_on_hover=NULL, background_color_on_hover=NULL, 
color_on_active=NULL, background_color_on_active=NULL where block_id=12;

update template_block SET color="#404040", background_color="#EFEFEF", color_on_hover=NULL, background_color_on_hover=NULL, 
color_on_active=NULL, background_color_on_active=NULL where block_id=13;

update template_block SET color="#FFFFFF", background_color="#2A2A2A", color_on_hover="#FFFFFF", background_color_on_hover="#9F0030", 
color_on_active="#FFFFFF", background_color_on_active="#9F0030" where block_id=14;

update template_block SET color="#2A2A2A", background_color="#FFFFFF", color_on_hover=NULL, background_color_on_hover=NULL, 
color_on_active=NULL, background_color_on_active=NULL where block_id=15;

update template_block SET color="#FFFFFF", background_color="#9F0030", color_on_hover="#9F0030", background_color_on_hover="#FFFFFF", 
color_on_active="#FFFFFF", background_color_on_active="#9F0030" where block_id=16;

update template_block SET color="#9F0030", background_color="#FFFFFF", color_on_hover="#9F0030", background_color_on_hover=NULL, 
color_on_active="#404040", background_color_on_active="#9F0030" where block_id=17;

update template_block SET color="#9F0030", background_color="#FFFFFF", color_on_hover="#FFFFFF", background_color_on_hover="#9F0030", 
color_on_active="#9F0030", background_color_on_active="#FFFFFF" where block_id=18;