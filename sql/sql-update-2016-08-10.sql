alter table event_custom add street  VARCHAR(255) after address;
alter table event_custom add postal_code  INT(11) after street;
alter table event_custom add city  VARCHAR(255) after postal_code;