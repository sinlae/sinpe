CREATE TABLE website (
website_id INT(11) NOT NULL AUTO_INCREMENT,
app_id INT(11) NOT NULL,	
biography LONGTEXT,
contact_email VARCHAR(255),
shop_url VARCHAR(255),
image TINYINT(1) default 0,
video TINYINT(1) default 0,
music TINYINT(1) default 0,
event TINYINT(1) default 0,
contact TINYINT(1) default 0,
bio TINYINT(1) default 0,
facebook_id VARCHAR(255),
PRIMARY KEY (website_id)
);

insert into acl_resource (parent_id, code, label, url) VALUES (22, "feature_website", "Website", NULL);