-- Twitter
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 43, 1, 'Layout 1', '/customization/layout/twitter/layout-1.png', 1);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (2, 43, 1, 'Layout 2', '/customization/layout/twitter/layout-2.png', 2);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (3, 43, 1, 'Layout 3', '/customization/layout/twitter/layout-3.png', 3);

-- Weblink
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 33, 1, 'Layout 1', '/customization/layout/weblink/links/layout-1.png', 1);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (2, 33, 1, 'Layout 2', '/customization/layout/weblink/links/layout-2.png', 2);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (3, 33, 1, 'Layout 3', '/customization/layout/weblink/links/layout-3.png', 3);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (4, 33, 1, 'Layout 4', '/customization/layout/weblink/links/layout-4.png', 4);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (5, 33, 1, 'Layout 4', '/customization/layout/weblink/links/layout-5.png', 5);

insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (6, 41, 1, 'Layout 1', '/customization/layout/weblink/submenu/layout-1.png', 1);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (7, 41, 1, 'Layout 2', '/customization/layout/weblink/submenu/layout-2.png', 2);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (8, 41, 1, 'Layout 3', '/customization/layout/weblink/submenu/layout-3.png', 3);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (9, 41, 1, 'Layout 4', '/customization/layout/weblink/submenu/layout-4.png', 4);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (10, 41, 1, 'Layout 4', '/customization/layout/weblink/submenu/layout-5.png', 5);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 41, 1, 'Layout 6', '/customization/layout/weblink/links/layout-1.png', 6);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (2, 41, 1, 'Layout 7', '/customization/layout/weblink/links/layout-2.png', 7);
-- Image
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 1, 1, 'Layout 1', '/customization/layout/image/layout-1.png', 1);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (2, 1, 1, 'Layout 2', '/customization/layout/image/layout-2.png', 2);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (3, 1, 1, 'Layout 3', '/customization/layout/image/layout-3.png', 3);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (4, 1, 1, 'Layout 4', '/customization/layout/image/layout-4.png', 4);

-- Topics
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 30, 1, 'Layout 1', '/customization/layout/topic/layout-1.png', 1);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (2, 30, 1, 'Layout 2', '/customization/layout/topic/layout-2.png', 2);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (3, 30, 1, 'Layout 3', '/customization/layout/topic/layout-3.png', 3);

-- Newswall / Fanwall
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 9, 1, 'Layout 3', '/customization/layout/topic/layout-3.png', 3);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 10, 1, 'Layout 3', '/customization/layout/topic/layout-3.png', 3);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 9, 1, 'Layout 4', '/customization/layout/topic/layout-4.png', 4);
insert into application_option_layout (code, option_id, template_level, name, preview, position) VALUES (1, 10, 1, 'Layout 4', '/customization/layout/topic/layout-4.png', 4);
