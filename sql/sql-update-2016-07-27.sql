alter table website add name varchar(255) after layout_id;
alter table website add general_conditions LONGTEXT after facebook_id;

ALTER TABLE website DROP COLUMN contact;

alter table website add image_name  varchar(50) after image;
alter table website add video_name  varchar(50) after video;
alter table website add music_name  varchar(50) after music;
alter table website add bio_name  varchar(50) after bio;
alter table website add event_name  varchar(50) after event;

alter table website add shop  TINYINT(1)  default 0 after shop_url;
alter table website add shop_name  varchar(50) after shop;
alter table subscription_application add ask_for_publish TINYINT(1)  default 0 after is_active;