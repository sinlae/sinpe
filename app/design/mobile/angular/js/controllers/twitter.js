App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/social/mobile_twitter_list/index/value_id/:value_id", {
        controller: 'TwitterListController',
        templateUrl: function(params) {
          return BASE_URL+"/social/mobile_twitter_list/template/value_id/"+params.value_id;
        },
        code: "twitter"
    });

}).controller('TwitterListController', function($scope, $http, $routeParams, $window, $location, $filter, $q, Customer, Pictos, Url, $timeout, Twitter, Pages) { 
    $scope.collection = new Array();
    $scope.factory = Twitter;
    $scope.value_id = Twitter.value_id = $routeParams.value_id;

    Twitter.find().success(function(data) {
        $scope.locked = data.locked;
        $scope.page_title = data.page_title;
        $scope.collection = data.collection;
        $scope.infos = data.infos;
        $scope.details = data.details;
        $scope.cover_image_url = data.cover_image_url;
        Pages.countView($scope.value_id);
    }).error(function() {

    }).finally(function() {
        $scope.is_loading = false;
    });

    $scope.getShare = function() {
        if(!Application.is_ios) {
           var logyou = true;
           $scope.share = true;
           document.getElementById("scrollable").style.overflowY = "initial";
           document.getElementById("scrollable").style.overflowX = "initial";
           return logyou;
        } else {
            $scope.sharing_data = {
                "page_name": $scope.page_title,
                "picture": null,
                "content": "a new twitter post"
            };
            Application.socialShareData($scope.sharing_data);
        }
    }

    $scope.getPopup = function() {
       var logyou = true;
       document.getElementById("scrollable").style.overflowY = "initial";
       document.getElementById("scrollable").style.overflowX = "initial";
       return logyou;
    }

    $scope.closePopup = function() {
       var logyou = false;
       document.getElementById("scrollable").style.overflowY = "auto";
       document.getElementById("scrollable").style.overflowX = "hidden";
       return logyou;
    }
});