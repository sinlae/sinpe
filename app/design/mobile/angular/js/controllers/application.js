App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/application/mobile_customization_colors", {
        controller: 'ApplicationCustomizationController',
        templateUrl: BASE_URL+"/application/mobile_customization_colors/template",
        code: "application"
    }).when(BASE_URL+"/application/mobile_tc_view/index/tc_id/:tc_id", {
        controller: 'ApplicationTcController',
        templateUrl: BASE_URL+"/application/mobile_tc_view/template",
        code: "application"
    }).when(BASE_URL+"/application/mobile_landing_view", {
        controller: 'ApplicationLandingController',
        templateUrl: BASE_URL+"/application/mobile_landing_view/template",
        code: "application"
    }).when(BASE_URL+"/website", {
        controller: 'ApplicationSiteController',
        templateUrl: function(params) {
            return BASE_URL+"/application/mobile_site_view/template/";
        },
        reloadOnSearch: false,
        code: "application"
    }).when(BASE_URL+"/application/mobile_landing_view/object_id/:object_id", {
        controller: 'ApplicationLandingController',
        templateUrl: BASE_URL+"/application/mobile_landing_view/template",
        code: "application"
    });

}).controller('ApplicationCustomizationController', function($window, $scope, $timeout) {

    $scope.is_loading = true;
    $scope.show_mask = false;
    $scope.elements = {
        "header": false,
        "subheader": false

    };
    $scope.nav = 0;

    $window.showMask = function(code) {

        $timeout(function() {
            $scope.show_mask = true;
            $scope.elements[code] = true;
        });

        //$scope.$apply();
    };

    $window.hideMask = function() {

        $timeout(function() {
            $scope.show_mask = false;
            for(var i in $scope.elements) {
                $scope.elements[i] = false;
            }
        });
        //$scope.$apply();

    }

}).controller('ApplicationTcController', function($scope, $routeParams, Tc) {

    $scope.$on("connectionStateChange", function(event, args) {
        if(args.isOnline == true) {
            $scope.loadContent();
        }
    });

    $scope.is_loading = true;

    $scope.loadContent = function () {

        Tc.find($routeParams.tc_id).success(function (data) {

            $scope.html_file_path = data.html_file_path;
            $scope.page_title = data.page_title;

        }).finally(function () {
            $scope.is_loading = false;
        });
    };

    $scope.loadContent();

}).controller('ApplicationLandingController', function($scope, $routeParams, Image, Url) {
    $scope.object_id = $routeParams.object_id;
}).controller('ApplicationSiteController', function($scope, $routeParams, $window,$location, $anchorScroll, Video, Youtube, MediaMusicAlbum, MediaMusicTrack, Event, Image, Site, Url) {
    $scope.locked = 2; 
    $scope.maxresults = 0;
    $scope.is_loading = true;
    $scope.imageloading = true;
    $scope.videolaoding = true;
    $scope.eventlaoding = true;
    $scope.musicloading = true;

    $scope.loadContent = function() {
        Site.findSiteAllowed().success(function(data){
            $scope.website = data.website; 

            Video.findVideoGalleries().success(function(data) {
                $scope.videogalleries = data;
                if(typeof($scope.videogalleries[0]) != "undefined"){
                    $scope.showvideos($scope.videogalleries[0], true);
                }
                $scope.maxresults = 0;
            }).error($scope.showError).finally(function() {
                $scope.videolaoding = false;
            }); 

            Event.findEventsOptionsValues().success(function(data) {
                $scope.eventoptionvalue = data;
                if(typeof($scope.eventoptionvalue[0]) != "undefined")
                { 
                   $scope.showevents($scope.eventoptionvalue[0], 0, true); 
                } else {
                    $scope.eventlaoding = false;
                }
            });


            MediaMusicAlbum.findAllAlbums().success(function(data) {
                $scope.albums = data;
                if(typeof(data[0]) != "undefined")
                {    
                    $scope.showTracks($scope.albums[0], true);
                } else {
                    $scope.musicloading = false;
                }
            }); 

            Image.findImagesGalleries().success(function(data) {
                $scope.galleries = data.galleries;
                if($scope.galleries && typeof($scope.galleries[0]) != "undefined")
                { 
                    $scope.showimages($scope.galleries[0],0,true);
                } else {
                    $scope.imageloading = false;
                }
            });
        }).error($scope.showError).finally(function() {
            $scope.is_loading = false;
        }); 
    };

    $scope.showblock = function(item, position){
        if(document.getElementById("home")){
            if(document.getElementById("home") && position != "0")
            {
                document.getElementById("home").style.display = "none";
            } else {
                document.getElementById("home").style.display = "block";
            }
        }
        if(document.getElementById("music")){
            document.getElementById("music").style.display = "none";
        }
        if(document.getElementById("bio")){
            document.getElementById("bio").style.display = "none"; 
        }
       if(document.getElementById("video")){
        document.getElementById("video").style.display = "none";
       }
       if(document.getElementById("event")){
        document.getElementById("event").style.display = "none";
       }
       if(document.getElementById("image")){
        document.getElementById("image").style.display = "none";
       }
       if(document.getElementById(item)){
          document.getElementById(item).style.display = "block";  
       }
        
    };

    $scope.showblockwidthhome = function(item, position){
        document.getElementById("music").style.display = "none";
        document.getElementById("bio").style.display = "none";
        document.getElementById("video").style.display = "none";
        document.getElementById("event").style.display = "none";
        document.getElementById("image").style.display = "none";
        document.getElementById(item).style.display = "block";
    };    

    $scope.loadMoreVideo = function(offset) {
        if(!offset) {
            offset = 1;
        }
        return $scope.showvideos($scope.item_active);
    };
      

    $scope.loadMoreImage = function(item) {
        var offset = $scope.imagecollection.length;
        $scope.showimages(item, offset);
    };

    $scope.loadMoreEvent = function(offset) {
        var offset = $scope.events.length;
        $scope.showevents($scope.item_event_active, offset);
    };

    $scope.showevents = function(item, offset, reset) {
        if(!offset) {
            offset = 0;
        }
         if(offset == 0) {
                $scope.events = null;
                $scope.eventlaoding = true;
         } 
         Event.findEvents(item.id, offset).then(function(data){
            $scope.item_event_active = item;
            if(offset == 0) {
                $scope.events = data.collection;
            } else {
                $scope.events = $scope.events.concat(data.collection);
            }
            if(reset) {
                if(!(document.getElementById("event_"+item.id) == null)){
                    document.getElementById("event_"+item.id).className = "square_event_selected";
                }
            }
        }).finally(function() {
            $scope.eventlaoding = false;
        }); 

        //set background color on selected gallery
        var f = document.getElementsByClassName('square_event_selected');
        if(!(f[0] == null)) {
            f[0].className = "square_event";
        }
        if(!reset) {
            document.getElementById("event_"+item.id).className = "square_event_selected";
        }
    }


    $scope.openMaps = function(item) {
        var address = item.address;
        encode_address = encodeURI(address);
        $window.open('https://www.google.fr/maps/place/'+encode_address, '_blank');
    };

    $scope.showvideos = function(item, reset) {
        $scope.videoscollection = null;
        if(typeof(item) != "undefined")
        {
            Video.findVideos(item.value_id).success(function(data) {
                if(data.collection[0] != undefined)
                {   
                    $scope.item_active = item;
                    $scope.search_by = item.search_by;
                    $scope.search_keyword = item.search_keyword;
                    Youtube.key = item.youtube_key;
                    if(!reset)
                    {    
                        $scope.maxresults =  $scope.maxresults + 5;
                    } else {
                        $scope.maxresults = 5;
                    } 
                    if($scope.maxresults < 21) {  
                        Video.findForSiteInYouTube($scope.search_by, $scope.search_keyword, null, $scope.maxresults).then(function(response) {
                            $scope.videoscollection = new Array();
                            for(var i = 0; i < response.collection.length; i++) {
                               if(!(response.collection[i].video_id == null)){
                                    $scope.videoscollection.push(response.collection[i]);
                               }
                            }
                            $scope.carousel_index = 4;

                            if(reset) {
                                if(!(document.getElementById("square_"+item.gallery_id)==null)){
                                    document.getElementById("square_"+item.gallery_id).className = "square_video_selected";
                                }
                            }
                        })
                    }

                    //set background color on selected gallery
                    var f = document.getElementsByClassName('square_video_selected');
                    if(!(f[0] == null)) {
                        f[0].className = "square_video";
                    }
                    var e = document.getElementsByClassName('square_video');
                    var i;
                    for(i=0; i<e.length; i++){
                        document.getElementById("square_"+item.gallery_id).className = "square_video";
                        if(e[i].id == 'square_'+item.gallery_id){
                            document.getElementById("square_"+item.gallery_id).className = "";
                            document.getElementById("square_"+item.gallery_id).className = "square_video_selected";
                        } 
                    }

                } else {
                    $scope.videoscollection = null;
                }       
            }).error($scope.showError)
            .finally(ajaxComplete);
        }
    }

    $scope.showTracks = function(item, reset) {
        var param = {};
        param.album_id = item.album_id;
        param.value_id = item.value_id;
        $scope.musiccollection = null;
        $scope.album_artwork = item.album_artwork;
        $scope.album_name = item.album_name;
        MediaMusicTrack.findTracksByAlbum(param).then(function (data) {
            $scope.musiccollection = data.collection;
            if(reset) {
                if(!( document.getElementById("music_"+item.album_id)==null)){
                    document.getElementById("music_"+item.album_id).className = "square_music_selected";
                }
            }
        }).finally(function() {
              $scope.musicloading = false;
        });
        var e = document.getElementsByClassName('album_infos');
        var i;
        for(i=0; i<e.length; i++){
            if(e[i].id == 'album_'+item.album_id){
                document.getElementById('album_'+item.album_id).style.display='block';

            } else {
                document.getElementById(e[i].id).style.display='none';
            }
        }

        //set background color on selected gallery
        var f = document.getElementsByClassName('square_music_selected');
        if(!(f[0] == null)) {
            f[0].className = "square_music";
        }
        if(!reset) {
            document.getElementById("music_"+item.album_id).className = "square_music_selected";
        }
    }

    $scope.showimages = function(item, offset, reset) {
        if(!offset) {
            offset = 0;
        }
        if(offset == 0) {
            $scope.imagecollection = null;
            $scope.imageloading = true;
        }
        Image.findImages(item.gallery_id,item.value_id, offset).then(function(data) {
                $scope.item_image_active = item;
                if(offset == 0) {
                    $scope.imagecollection = data.collection;
                } else {
                    $scope.imagecollection = $scope.imagecollection.concat(data.collection);
                }

            //set background color on selected gallery
            if(reset) {
                if(!(document.getElementById("image_"+item.gallery_id)==null)){
                    document.getElementById("image_"+item.gallery_id).className = "square_image_selected";
                }
            }
        }).finally(function() {
              $scope.imageloading = false;
        });

        var f = document.getElementsByClassName('square_image_selected');
        if(!(f[0] == null)) {
            f[0].className = "square_image";
        }
        if(!reset) document.getElementById("image_"+item.gallery_id).className = "square_image_selected";

    }

    $scope.setNavVisible = function(position) {
        if(position == "right"){
            document.getElementById("navigation").style.right = 0;
        } else {
            document.getElementById("navigation").style.left = 0;
        }
        document.getElementById("nav-close").style.display = 'inline';
        document.getElementById("nav-open").style.display = 'none';
        document.getElementById("logo").style.visibility = 'hidden';
    }

    $scope.setNavHidden = function(position) {
        if(position == "right"){
            document.getElementById("navigation").style.right = "-47%";
        } else {
            document.getElementById("navigation").style.left = "-47%";
        }
        document.getElementById("nav-close").style.display = 'none';
        document.getElementById("nav-open").style.display = 'inline';
        document.getElementById("logo").style.visibility = 'visible';
    }

    $scope.goToItem = function(item) {
        $scope.nav = 0;
        $location.hash(item);
        $anchorScroll();
    }

    $scope.showGallery = function(url, index) {
        $scope.showimage = url;
        $scope.bigimage = true;
        $scope.i = index;
        $scope.prev = true;
        $scope.next = true;
        if($scope.i == 0){
            $scope.prev = false;
        }
        if($scope.i == ($scope.imagecollection.length-1)){
            $scope.next = false;
        }
    }
    $scope.showNextImage = function() {
        if($scope.i < ($scope.imagecollection.length-1)){
            $scope.i = $scope.i+1;
            $scope.showimage = $scope.imagecollection[$scope.i].url;
            //$scope.$apply();
        } 
        if($scope.i == 0){
            $scope.prev = false;
        } else {
            $scope.prev = true;
        }
        if($scope.i == ($scope.imagecollection.length-1)){
            $scope.next = false;
        } else {
            $scope.next = true;
        }
    }

    $scope.showPrevImage = function() {
        if($scope.i>0){
            $scope.i = $scope.i-1;
            $scope.showimage = $scope.imagecollection[$scope.i].url;
            //$scope.$apply();
        }
        if($scope.i == 0){
            $scope.prev = false;
        } else {
            $scope.prev = true;
        }
        if($scope.i == ($scope.imagecollection.length-1)){
            $scope.next = false;
        } else {
            $scope.next = true;
        }
    }
    $scope.closeModal = function(){
        $scope.bigimage = false;
        $scope.mention = false;
    }

    $scope.openNewsletter = function(){
        $scope.newsletter = true;
        $scope.newsletter_save = false;
    }
    $scope.closeNewsletter = function(){
        $scope.newsletter = false;
    }

    $scope.openModal = function(){
        $scope.mention = true;
    }

    $scope.subscribe = function(){
        var name = document.getElementById("newsletter_name").value;
        var firstname = document.getElementById("newsletter_firstname").value;
        var email = document.getElementById("newsletter_email").value;
        if($scope.validateEmail(email)){
            Site.subscribe(name,firstname,email).success(function(data){
                 $scope.newsletter_save = true;
            });
        } else {
            $scope.validate_email = true;
        }
    }
    $scope.validateEmail = function(email){ 
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email); 
    }  

    $scope.loadContent();
});