App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/push/mobile_list/index/value_id/:value_id", {
        controller: 'PushController',
        templateUrl: BASE_URL+"/push/mobile_list/template",
        code: "push"
    }).when(BASE_URL+"/push/mobile_edit/value_id/:value_id", {
        controller: 'PushEditController',
        templateUrl: BASE_URL+"/push/mobile_edit/template",
        code: "newswall"
    });

}).controller('PushController', function($rootScope, $scope, $routeParams, $location, Url, PUSH_EVENTS, Push, Customer) {

    $scope.$on("connectionStateChange", function(event, args) {
        if(args.isOnline == true) {
            $scope.loadContent();
        }
    });

    $scope.is_loading = true;
    $scope.value_id = Push.value_id = $routeParams.value_id;

    $scope.factory = Push;
    $scope.collection = new Array();

    $scope.loadContent = function() {

        Push.findAll().success(function(data) {
            $scope.is_admin = data.is_admin;
            $scope.picto_url = data.picto_url;
            $scope.page_title = data.page_title;
            if($scope.is_admin == true)
            {
               $scope.can_post = true;
            } 

            if ($scope.can_post) {
                if (Customer.isLoggedIn()) {
                    rightAction = $scope.addPush;
                }
                $scope.header_right_button = {
                    action: rightAction,
                    hide_arrow: true,
                    picto_url: $scope.picto_url
                };
            }
            if(data.collection.length) {
                $scope.collection = data.collection;
                $rootScope.$broadcast(PUSH_EVENTS.readPushs);
                Application.call("markPushAsRead");
            } else {
                $scope.collection_is_empty = true;
            }
            $scope.collection.length = data.collection.length;
        }).finally(function() {
            $scope.is_loading = false;
        });

    }

    $scope.addPush = function () {
        if(!$scope.is_loading) {
            $scope.is_loading = true;
            $location.path(Url.get("push/mobile_edit", {
                value_id: $routeParams.value_id
            }));
        }
    };

    $scope.loadContent();

}).controller('PushEditController', function($scope, $http, $routeParams, $location, $q, $timeout, $window, Application, Push, Url, Message) {

    $scope.text = "";
    $scope.readyToPost = false;
    $scope.handle_camera_picture = Application.handle_camera_picture;

    $scope.header_right_button = {
        action: function() {
            $scope.createPush();
        },
        hide_arrow: true,
        title: "OK"
    };

    $scope.sendPush = function() {

        if (!$scope.readyToPost) return;

        $scope.is_loading = true;

        Push.createPush($scope.text, $scope.image).success(function(data) {
            $scope.message = new Message();
            $scope.message.setText(data.message)
            .isError(false)
            .show()
            ;
            $window.history.back();
        }).error(function() {

        }).finally(function() {
            $scope.is_loading = false;
        });

        $scope.readyToPost = false;
    };

    $scope.createPush = function() {

        $scope.value_id = Push.value_id = $routeParams.value_id;

        $scope.readyToPost = true;
        $scope.sendPush();
    };

    $scope.imageSelected = function(element) {

        if(element.files.length > 0) {
            var file = element.files[0];

            var reader = new FileReader();
            var img = document.getElementById('image');
            reader.onload = (function(aImg) {
                return function(e) {
                    var content = e.target.result;
                    $timeout(function() {
                        aImg.src = content;
                        $scope.image = content;
                        $scope.preview_src = content;
                    });
                };
            }) (img);

            reader.readAsDataURL(file);

        } else {
            // Only needed on Chrome when pressing Cancel
            $scope.product.imageContent = undefined;
        }
    };

    $scope.openCamera = function() {
        Application.openCamera(function(image_url) {
            image_url = "data:image/jpg;base64,"+image_url;
            $timeout(function() {
                $scope.preview_src = image_url;
                $scope.image = image_url;
            });
        }, function() {

        });
    };
});
