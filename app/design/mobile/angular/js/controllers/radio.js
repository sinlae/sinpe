App.config(function ($routeProvider) {

    $routeProvider.when(BASE_URL + "/radio/mobile_radio/index/value_id/:value_id", {
        controller: 'RadioController',
        template: ''
    });

}).controller('RadioController', function ($window, $interval, $q, $scope, $routeParams, $location, $timeout, Url, Radio, MediaMusicTracksLoaderService, MediaMusicPlayerService, Pages) {

    Radio.value_id = $routeParams.value_id;

    $scope.loadContent = function () {
            Radio.find().success(function (data) {
                //alert(JSON.stringify(data));
           // $scope.song_title = ",dbf,s,fbs";
            if(!Application.is_ios  || !Application.player_is_native) {
                //alert($scope.song_title);

//alert(data.radio.image);
                 MediaMusicPlayerService.init(document);

                var tracksLoader = MediaMusicTracksLoaderService.loadSingleTrack({
                    name: data.radio.title,
                    bigArtwork: data.image_url,
                    streamUrl: data.radio.url
                });

                MediaMusicPlayerService.playTracks(tracksLoader, 0, true, true);

               // $timeout(function() {
                 //   $scope.getTitle();
                //}, 2000);

                 // var theInterval = $interval(function(){
                 //      $scope.song_title = $scope.getTitle();
                 //      $scope.$apply()
                 //   }.bind(this), 20000);

            } else {
                 $window.audio_player_data = JSON.stringify(
                    {
                        tracks: [{
                            name: data.radio.title,
                            streamUrl: data.radio.url,
                            artWorkUrl: data.image_url,
                            bigArtwork: data.image_url
                        }],
                        isRadio: true
                    }
                );
                Application.call("openAudioPlayer", $window.audio_player_data);

                $timeout(function() {
                    $window.history.back();
                }, 2000);
               
            }
            Pages.countView($routeParams.value_id);

        });
    };

    $scope.getTitle=function(){
        Radio.gettitle().then(function (data) {
            $scope.song_title = data.song_title;
        });
        return $scope.song_title;
    }
    $scope.loadContent();

});