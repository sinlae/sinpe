"use strict";

App.directive("youtube", function ($window,$rootScope) {
return {
    restrict: "E",

    scope: {
      height:   "@",
      width:    "@",
      videoid:  "@"  
    },

    template: '<div></div>'+
    '<div style="height:30px" class="muteyoutube"><img id="mute" ng-click="mute()"src="https://cdn1.iconfinder.com/data/icons/mini-solid-icons-vol-1/16/30-512.png" width="30px"/>'+
    '<img id="unmute" ng-click="unmute()"src="https://cdn3.iconfinder.com/data/icons/line/36/sound-512.png" width="30px" style="display:none"/>'+
    '<div>',


    link: function(scope, element, $scope) {
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
      var player;

      $window.onYouTubeIframeAPIReady = function() {
        player = new YT.Player(element.children()[0], {

          playerVars: {
            autoplay: 1,
            loop: 1,
            html5: 1,
            theme: "light",
            modesbranding: 0,
            color: "white",
            iv_load_policy: 3,
            showinfo: 1,
            controls: 1,
          },

          height: scope.height,
          width: scope.width,
          videoId: scope.videoid
        });
      };

      scope.mute = function(){
        document.getElementById("unmute").style.display="block";
        document.getElementById("mute").style.display="none";
        player.mute();
      }
      scope.unmute = function(){
          document.getElementById("mute").style.display="block";
          document.getElementById("unmute").style.display="none";
          player.unMute();
      }

    },  
  }
});
