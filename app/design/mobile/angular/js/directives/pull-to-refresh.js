'use strict';

App.directive('pullToRefresh', function ($timeout, $rootScope, $http, Url, httpCache) {
      return {
        restrict: 'A',
        transclude: true,
        scope: {
          factory: "=",
          collection: "=",
          valueId: "=",
          loadMore: "&",
          refreshFunction: '&'
        },
        template: '<center><div   class="ng-hide" ng-show="pullToRefreshActive" id="pull-to-refresh">' +
                  '<img src="http://equistro.fr/images/chargement_en_cours.gif" width="30px"/>' +
                  '</div></center>' +
                  '<div ng-transclude style="height:103%"></div>',

        link: function (scope, element, attrs) {

            scope.loader_is_visible = false;
            if(angular.isDefined(scope.collection) && angular.isDefined(scope.factory)) {
                var collectionWatcher = scope.$watch("collection", function() {
                    scope.checkStatus(scope.collection);
                });
            }

            if(angular.isDefined(scope.factory)) {
                angular.element(element).bind("scroll", function (e) {
                    if (this.scrollHeight - this.clientHeight - this.scrollTop <= 20) {
                        if (!scope.is_active) return;
                        scope.LoadCollectionMore();
                    }

                });
            }

            scope.$on("$destroy", function() {
                angular.element(element).unbind('scroll');
                if(angular.isDefined(collectionWatcher)) {
                    collectionWatcher();
                }
            });


            //pulltorefresh
            scope.hasCallback = angular.isDefined(attrs.refreshFunction);
            scope.isAtTop = false;
            scope.pullToRefreshActive = false;
            scope.lastScrollTop = 0;

            scope.pullToRefresh = function () {
                if (scope.hasCallback) {
                    if (!scope.pullToRefreshActive) {
                        scope.pullToRefreshActive = true;
                        scope.refreshFunction().then(function () {
                            scope.pullToRefreshActive = false;
                        });
                        scope.$digest();
                    }
                }
            };

          // Wait 1 second and then add an event listener to the scroll events on this list- this enables pull to refresh functionality
            $timeout(function () {
                var onScroll = function (event) {
                    if (element[0].scrollTop <= 0 && scope.lastScrollTop <= 0) {
                        //uncomment this line for desktop testing
                        //if (element[0].scrollTop <= 0) {
                        if (scope.isAtTop) {
                            scope.pullToRefresh();
                        } else {
                            scope.isAtTop = true;
                        }
                    }
                    scope.lastScrollTop = element[0].scrollTop;
                };
                if(!(Application.is_ios)) {
                    element[0].addEventListener('touchmove', onScroll);
                } else {
                    element[0].addEventListener('scroll', onScroll);
                }
            }, 2000);


            scope.refreshFunction= function() {
                var $refreshpost = $http({
                        method: 'GET',
                        url: Url.get("comment/mobile_list/findall", {value_id: scope.valueId}),
                        cache: false,
                        responseType:'json'
                        }).success(function(data) {
                           scope.collection = data.collection;
                           scope.details = data.details;
                    }); 
                httpCache.remove(Url.get("comment/mobile_list/findall", {value_id: scope.valueId}));
                return $refreshpost; 
            };

            scope.LoadCollectionMore = function() {
                scope.loader_is_visible = true;
                scope.is_active = false;
                var promise = scope.loadMore({offset: scope.collection.length});

                if(promise && typeof(promise) == "object") {

                    promise.then(function(response) {

                        if(response) {
                            scope.checkStatus(response.data.collection);
                        }

                        scope.loader_is_visible = false;
                    }, function() {
                        scope.loader_is_visible = false;
                    });

                } else if(!promise) {

                    scope.factory.findAll(scope.collection.length).success(function (data) {

                        angular.forEach(data.collection, function (value, key) {
                            scope.collection.push(value);
                        });

                        if(data) {
                            scope.checkStatus(data.collection);
                        }

                        scope.loader_is_visible = false;

                    }).error(function () {
                        scope.loader_is_visible = false;
                    });

                } else {
                    scope.is_active = false;
                    scope.loader_is_visible = false;
                }

            };

            scope.checkStatus = function(collection) {

                if(!collection || !collection.length) {
                    scope.is_active = false;
                } else if(!isNaN(scope.factory.displayed_per_page)) {
                    scope.is_active = collection.length >= scope.factory.displayed_per_page;
                }

                return this;

            }
        }
    }
});