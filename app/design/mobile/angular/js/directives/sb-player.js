"use strict";

App.directive("sbPlayer", function($timeout) {
    var audioElement = [];
    var duration = [];
    return {
        restrict: "A",
        replace:true,
        scope: {
            track: "="
        },
        template:
            '<div class="audio">'
                +'<div ng-show="show_player">'
                    +'<div id="audio_player_view" class="player">'
                        +'<div style="width:20px; display:inline;" ng-click="playTrack()"><i class="icon-play"></i>'
                        +'</div>'
                        +'<div style="width:20px; display:inline;" ng-click="pauseTrack()"><i class="icon-pause"></i>'
                        +'</div>'
                        +'<div class="progressbar-content" style="">'
                            +'<p style="width:100%; height:10px; border-radius:5px;" class="progress_bar">'
                            +'</p>'
                            +'<span style="width:0px; height:11px; display:inline-block;border-radius:5px;" class="progress_bar_intern">'
                            +'</span>'
                        +'</div>'
                    +'</div>'
                +'</div>'
            +'</div>'
        ,
        controller: function($scope) {
            $scope.show_player = true;
        },
        link: function(scope, element) {
            element.find('span').attr('id', "span-"+scope.track.id);
            element.find('p').attr('id', "div-"+scope.track.id);
            scope.animate = function(element,size) {
                element.style.width = size
            };

            scope.playTrack = function() {
                if (typeof audioElement[scope.track.id] == 'undefined') {
                    audioElement[scope.track.id] = document.createElement("audio");
                }
                audioElement.forEach(function(element,index) {
                    if(index != scope.track.id) {
                        element.pause();
                        duration[index] = 0;
                        element.currentTime = 0;
                    }
                });
                //audioElement[scope.track.id].addEventListener("durationchange", function(){
                  //  document.getElementById("span-"+scope.track.id).style.width = audioElement[scope.track.id].duration+"px";
                //});

                //document.body.appendChild(audioElement);
                audioElement[scope.track.id].addEventListener("timeupdate", function() {
                     var currentTime = audioElement[scope.track.id].currentTime;
                     var durate = audioElement[scope.track.id].duration;
                     var size = (currentTime +.25)/durate*99+'%';
                     scope.animate(document.getElementById("span-"+scope.track.id), size);
                 });

                document.getElementById("div-"+scope.track.id).addEventListener('click', function(e) {
                    var durate = audioElement[scope.track.id].duration;
                    var width = document.getElementById("div-"+scope.track.id).offsetWidth;
                    audioElement[scope.track.id].currentTime = (durate*e.offsetX)/width;
                });

                if (typeof duration[scope.track.id] == 'undefined') {
                    audioElement[scope.track.id].setAttribute("src", scope.track.streamUrl);
                    audioElement[scope.track.id].play();
                    audioElement[scope.track.id].addEventListener("durationchange", function() {
                        document.getElementById( "span-"+scope.track.id).width=audioElement[scope.track.id].duration;
                    });
                } else {
                    audioElement[scope.track.id].play(duration[scope.track.id]);
                }

            };
            scope.pauseTrack = function() {
                audioElement[scope.track.id].pause();
                duration[scope.track.id] = audioElement[scope.track.id].duration;
                //document.body.appendChild(audioElement);
            };

            // $timeout(function () {
            //     audioElement[scope.track.id] = document.createElement("audio");
            //     audioElement[scope.track.id].addEventListener("timeupdate", function() {
            //         var currentTime = audioElement[scope.track.id].currentTime;
            //         var duration = audioElement[scope.track.id].duration;
            //         var size = (currentTime +.25)/duration*100+'%';
            //         scope.animate(document.getElementById("span-"+scope.track.id), size);
            //     });
            // });
        }    
    };
});