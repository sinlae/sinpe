"use strict";

App.directive("sinCarousel", function () {
	 return {
	         	restrict: 'A',
	          	transclude: true,
	         	scope: {
	            	slides: '=',
	            	currentItemIndex: '=',
	            	onNext: '&',
	            	onPrevious: '&',
	          	},
	          	template: '<div class="wallop-slider">'+
              	  				'<ul class="wallop-slider__list">'+
                						'<li class="wallop-slider__item" ng-repeat="video in slides">'+
                							'<div sb-video video="video" id="{{video.video_id}}"></div>'+
                						'</li>'+
                					'</ul>'+
                					'<ul style="text-align: center;">'+
                					  '<li ng-repeat="video in slides">'+
                							'<div class="no-current" ng-click="setCurrent(\'{{video.video_id}}\')" id="current_{{video.video_id}}"></div>'+
                					   '</li>'+
                					'</ul>'+
                        '</div>',
          		controller: function($scope, $timeout) {
        },
        link: function(scope) {
        	 var collectionWatcher = scope.$watch("slides", function() {
                  if(!(scope.slides == null)){
                  	var elmt = document.getElementsByClassName("wallop-slider__item");
                  	var curt = document.getElementsByClassName("no-current");

                  	if(scope.slides.length > 5){
                  		elmt[(Math.ceil(scope.slides.length/5)-1)*5].className="wallop-slider__item--current";
                  		curt[(Math.ceil(scope.slides.length/5)-1)*5].className="current-item";
                  	} else {
                  		elmt[0].className="wallop-slider__item--current";
                  		curt[0].className="current-item";
                  	}
                  }
                });
        	scope.setCurrent  = function (id) {
        		var elements = document.getElementsByClassName("wallop-slider__item--current");
        		var requiredElement = elements[0];
        		if(!(elements[0] == null)) {
        			requiredElement.className="";
        			requiredElement.className="wallop-slider__item";
        		}
        		var no_current = document.getElementsByClassName("current-item");
        		var requiredNoCurrent = no_current[0];
        		if(!(no_current[0] == null)) {
        			requiredNoCurrent.className="";
        			requiredNoCurrent.className="no-current";
        		}
        		var x = document.getElementById(id).parentElement;
        		x.className = "wallop-slider__item--current";
        		var current = document.getElementById("current_"+id);
        		current.className = "current-item";
        	}

        }
    };
 });
