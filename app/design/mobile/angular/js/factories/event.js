
App.factory('Event', function($rootScope, $http, $q, Url) {

    var factory = {};

    factory.value_id = null;
    factory.displayed_per_page = null;

    factory.findAll = function(offset) {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("event/mobile_list/findall", {value_id: this.value_id, offset: offset}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        }).success(function(data) {
            if(data.displayed_per_page) {
                factory.displayed_per_page = data.displayed_per_page;
            }
        });
    };


    factory.findEvents = function(value_id, offset) {
        //if(!this.value_id) return;

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: Url.get("event/mobile_list/findall", {value_id: value_id, offset:offset}),
            responseType: 'json'
        }).success(function(response) {
            var data = response;
            if(data.displayed_per_page) {
                factory.displayed_per_page = data.displayed_per_page;
            }
            return deferred.resolve(data);
        }).error(function (data) {
            return deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.findEventsOptionsValues = function() {
        return $http({
            method: 'GET',
            url: Url.get("event/mobile_list/findeventsoptionsvalues"),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    factory.findById = function(event_id) {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("event/mobile_view/find", {value_id: this.value_id, event_id: event_id}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    return factory;
});
