App.factory('Twitter', function($window, $rootScope, $http, Url) {
	var factory = {};

    factory.value_id = null;
    factory.token = null;
    factory.username = null;

    factory.find = function() {
        if(!this.value_id) return;

        var url = Url.get('social/mobile_twitter_list/find', {value_id: this.value_id});

        return $http({
            method: 'GET',
            url: url,
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    return factory;
});