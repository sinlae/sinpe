App.factory('Pages', function($rootScope, $http, Url) {

    var factory = {};

    factory.findAll = function() {
        return $http({
            method: 'GET',
            url: BASE_URL+'/front/mobile_home/findall',
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    factory.countView = function(value_id) {
        return $http({
            method: 'GET',
            url: Url.get("front/mobile/countview", {value_id: value_id}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    return factory;
});
