App.factory('Image', function($rootScope, $http, $q, Url) {

    var factory = {};

    factory.value_id = null;
    factory.displayed_per_page = 0;

    factory.findAll = function() {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("media/mobile_gallery_image_list/findall", {value_id: this.value_id}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        }).success(function(data) {
            if(data.displayed_per_page) {
                factory.displayed_per_page = data.displayed_per_page;
            }
        });
    };

    factory.find = function(item) {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("media/mobile_gallery_image_view/find", {value_id: this.value_id, gallery_id: item.id, offset: item.current_offset}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    factory.findImages = function(gallery_id, value_id, offset) {
        //if(!this.value_id) return;

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: Url.get("media/mobile_gallery_image_view/find", {value_id: value_id, gallery_id: gallery_id, offset: offset}),
            cache: !$rootScope.isOverview,
            responseType: 'json'
        }).success(function(response) {
             var data = response;
            return deferred.resolve(data);
        }).error(function (data) {
            return deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.findImagesGalleries = function() {
        return $http({
            method: 'GET',
            url: Url.get("media/mobile_gallery_image_view/findgalleries"),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    }

    return factory;
});
