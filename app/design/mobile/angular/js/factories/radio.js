App.factory('Radio', function($rootScope, $http, Url, $q) {

    var factory = {};

    factory.value_id = null;

    factory.find = function(product_id) {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("radio/mobile_radio/find", {value_id: this.value_id}),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    factory.gettitle = function()
    {
        var deferred = $q.defer();
        var data =  $http({
             method: 'GET',
            url: Url.get("radio/mobile_radio/gettitle", {value_id: this.value_id}),
            responseType:'json' 
        }).success(function(response) {
             var data = response;
            return deferred.resolve(data);
        }).error(function (data) {
            return deferred.reject(data);
        });
        return deferred.promise;
        }
        
    return factory;
});
