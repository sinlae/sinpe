"use strict";

App.factory('MediaMusicTrack', function ($rootScope, $http, $q, Url) {

    var factory = {};

    factory.value_id = null;

    factory.findByAlbum = function (element) {

        console.log("element: ", element);
        if (!this.value_id) {
            console.error('value_id is not defined.');
            return;
        }

        if (!element) {
            console.error('album_id is not defined.');
            return;
        }

        var params = {
            value_id: this.value_id
        };
        if(element.album_id) {
            params.album_id = element.album_id;
        } else {
            params.track_id = element.track_id;
        }

        return $http({
            method: 'GET',
            url: Url.get("media/mobile_api_music_track/findbyalbum", params),
            cache: !$rootScope.isOverview,
            responseType: 'json'
        });
    };

    factory.findTracksByAlbum = function (params) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: Url.get("media/mobile_api_music_track/findbyalbum", params),
            cache: true,
            responseType: 'json'
        }).success(function(response) {
             var data = {
                collection: new Array()
            };
            angular.forEach(response.tracks, function (item) {
                var track = {
                    id : item.id,
                    name :item.name,
                    artistName : item.artistName,
                    albumName  : item.albumName,
                    albumId : item.albumId,
                    duration : item.duration,
                    streamUrl : item.streamUrl,
                    purchaseUrl : item.purchaseUrl,
                    artWorkUrl : item.artWorkUrl,
                    bigArtwork : item.bigArtwork
                };

                data.collection.push(track);
            });

            return deferred.resolve(data);
        }).error(function (data) {
            return deferred.reject(data);
        });

        return deferred.promise;;
    };

    factory.addPlay = function (track_id) {
        $http({
            method: 'GET',
            url: Url.get("media/mobile_api_music_track/addplay", {track_id:track_id}),
            responseType: 'json'
        });

    };
    factory.getLyrics = function (track_id) {
        return $http({
            method: 'GET',
            url: Url.get("media/mobile_api_music_track/getlyrics", {track_id:track_id}),
            responseType: 'json'
        });

    };

    return factory;
});