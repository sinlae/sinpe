
App.factory('Site', function($rootScope, $http, Url) {

    var factory = {};

    factory.findSiteAllowed = function() {

        return $http({
            method: 'GET',
            url: Url.get("application/mobile_site_view/findsiteallowed"),
            cache: !$rootScope.isOverview,
            responseType:'json'
        });
    };

    factory.subscribe = function(name,firstname,email) {
        data = {firstname:firstname, name:name, email:email};
        return $http({
            method: 'POST',
            url: Url.get("application/mobile_site_view/subscribe"),
            data: data,
            responseType:'json'
        });
    };

    return factory;
});
