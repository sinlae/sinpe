App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/template/backoffice_category_edit/:layout_id", {
        controller: 'TemplateEditController',
        templateUrl: BASE_URL+"/template/backoffice_category_edit/template"
    }).when(BASE_URL+"/template/backoffice_category_list", {
        controller: 'TemplateCategoryListController',
        templateUrl: BASE_URL+"/template/backoffice_category_list/template"
    });

}).controller("TemplateCategoryListController", function($scope, $location, Header, TemplateCategory, Template, Label) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;
    $scope.template = {};
    $scope.templatebis = {};

    TemplateCategory.loadData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
    });

    TemplateCategory.findAll().success(function(data) {
        $scope.template_category = data;
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    Template.findAll().success(function(data) {
        $scope.templates = data;
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.save = function() {

        $scope.form_loader_is_visible = true;

        TemplateCategory.save($scope.getCategories()).success(function(data) {
            $scope.message.setText(data.message)
                .isError(false)
                .show()
            ;
        }).error(function(data) {

            $scope.message.setText(data.message)
                .isError(true)
                .show()
            ;

        }).finally(function() {
            $scope.form_loader_is_visible = false;
        });
    }

    $scope.update = function() {
    var e = document.getElementById("sort_option");
    var option = e.options[e.selectedIndex].value;

     Template.findByOption(option).success(function(data) {
            if(data.success) {
               $scope.templates = data.templates;
            }
        }).error($scope.showError)
        .finally();
    }

        $scope.saveTemplate = function() {

        $scope.form_loader_is_visible = true;

        Template.save($scope.template).success(function(data) {
            $location.path("template/backoffice_category_list");
            $scope.message.setText(data.message)
                .isError(false)
                .show()
            ;
        }).error(function(data) {
            var message = Label.save.error;
            if(angular.isObject(data) && angular.isDefined(data.message)) {
                message = data.message;
            }

            $scope.message.setText(message)
                .isError(true)
                .show()
            ;
        }).finally(function() {
            $scope.form_loader_is_visible = false;
        });
    }

    $scope.getCategories = function() {
        var categories = new Array();
        angular.forEach($scope.template_category.columns, function(columns) {
            angular.forEach(columns, function(category) {
                categories.push(category);
            });
        });
        return categories;
    };

}).controller("TemplateEditController", function($scope, $location, Header, $routeParams, Template, Label) {
    var layout_id = $routeParams.layout_id;
    Template.find(layout_id).success(function(data) {
        $scope.template = data.template;
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.saveTemplate = function() {

        $scope.form_loader_is_visible = true;

        Template.save($scope.template).success(function(data) {
            $location.path("template/backoffice_category_list");
            $scope.message.setText(data.message)
                .isError(false)
                .show()
            ;
        }).error(function(data) {
            var message = Label.save.error;
            if(angular.isObject(data) && angular.isDefined(data.message)) {
                message = data.message;
            }

            $scope.message.setText(message)
                .isError(true)
                .show()
            ;
        }).finally(function() {
            $scope.form_loader_is_visible = false;
        });
    }
});
