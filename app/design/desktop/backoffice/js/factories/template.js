
App.factory('Template', function($http, Url) {

    var factory = {};

    factory.findAll = function() {

        return $http({
            method: 'GET',
            url: Url.get("template/backoffice_template_list/findall"),
            cache: true,
            responseType:'json'
        });
    };

    factory.save = function(data) {
        return $http({
            method: 'POST',
            data: data,
            url: Url.get("template/backoffice_template_list/save"),
            responseType:'json'
        });
    };

    factory.findByOption = function(option_id) {
        return $http({
            method: 'GET',
            url: Url.get("template/backoffice_template_list/findbyoption", {option_id:option_id}),
            responseType:'json'
        });
    };

    factory.find = function(layout_id) {
        return $http({
            method: 'GET',
            url: Url.get("template/backoffice_template_edit/find", {layout_id:layout_id}),
            responseType:'json'
        });
    };

    return factory;
});