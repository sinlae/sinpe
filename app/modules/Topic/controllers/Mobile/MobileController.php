<?php

class Topic_MobileController extends Application_Controller_Mobile_Default {
	public function templateAction() {
        $this->loadPartials($this->getFullActionName('_').'_l'.$this->_layout_id, false);
    }
}