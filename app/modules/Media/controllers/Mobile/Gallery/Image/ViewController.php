<?php

class Media_Mobile_Gallery_Image_ViewController extends Application_Controller_Mobile_Default {

    public function findAction() {

        if ($gallery_id = $this->getRequest()->getParam("gallery_id")) {

            try {

                $offset = $this->getRequest()->getParam('offset', 0);
                $data = array("collection" => array());

                $image = new Media_Model_Gallery_Image();
                $image->find($gallery_id);

                if (!$image->getId() OR $image->getValueId() != $this->getCurrentOptionValue()->getId()) {
                    throw new Exception($this->_($image->getId()));
                }

                $images = $image->setOffset($offset)->getImages();

                foreach ($images as $key => $link) {
                    $key+=$offset;
                    $data["collection"][] = array(
                        "offset" => $link->getOffset(),
                        "gallery_id" => $key,
                        "is_visible" => false,
                        "url" => $link->getImage(),
                        "title" => $link->getTitle(),
                        "description" => $link->getDescription(),
                        "author" => $link->getAuthor()
                    );
                }

                if($image->getTypeId() != "custom") {
                    $data["show_load_more"] = count($data["images"]) > 0;
                } else {
                    $data["show_load_more"] = (($key - $offset) + 1) > (Media_Model_Gallery_Image_Abstract::DISPLAYED_PER_PAGE - 1) ? true : false;
                }

            } catch (Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($data);
        }

    }

    public function findgalleriesAction() 
    {
        try 
        {
            $option = new Application_Model_Option();
            $option->find('image_gallery', 'code');
            $option_value = new Application_Model_Option_Value();
            $option_value = $option_value->findFolderValues($this->getApplication()->getId(),$option->getId());
            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());
            $data = array();
            foreach ($option_value as $key => $value) {
                if(!in_array($value->getId(), $lock_values_id))
                {
                    $galleries = new Media_Model_Gallery_Image();
                    $galleries = $galleries->findAll(array("value_id"=>$value->getId()));

                    foreach ($galleries as $key => $gallery) {
                        $images = $gallery->setOffset(null)->getImages();
                        if(count($images) > 0)
                        {    
                            $data["galleries"][] = array(
                                "value_id" => $gallery->getValueId(),
                                "gallery_id" => $gallery->getId(),
                                "name" => $gallery->getName()
                            );
                        }
                    }
                }
            }


        }  catch (Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage()); 
        }

        $this->_sendHtml($data);
    }

}
