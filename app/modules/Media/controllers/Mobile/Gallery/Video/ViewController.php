<?php

class Media_Mobile_Gallery_Video_ViewController extends Application_Controller_Mobile_Default {

    public function findAction() {

        if ($gallery_id = $this->getRequest()->getParam("gallery_id") AND $offset = $this->getRequest()->getParam('offset', 1)) {

            try {

                $data = array("collection" => array());

                $video = new Media_Model_Gallery_Video();
                $video->find($gallery_id);

                if (!$video->getId() OR $video->getValueId() != $this->getCurrentOptionValue()->getId()) {
                    throw new Exception($this->_('An error occurred while loading pictures. Please try later.'));
                }

                $videos = $video->setOffset($offset)->getVideos();
                $icon_url = $this->_getColorizedImage($this->getCurrentOptionValue()->getIconId(), $this->getApplication()->getBlock('background')->getColor());

                foreach ($videos as $key => $link) {

                    $key += $offset;
                    
                    $data["collection"][] = array(
                        "offset" => $key,
                        "video_id" => $link->getVideoId(),
                        "is_visible" => false,
                        "url" => $link->getLink(),
                        "cover_url" => $link->getImage(),
                        "title" => $link->getTitle(),
                        "description" => $link->getDescription(),
                        "icon_url" => $icon_url
                    );

                }


            } catch (Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($data);
        }

    }

    public function findvideogalleriesAction() 
    {
        try  
        {
            $option = new Application_Model_Option();
            $option->find('video_gallery', 'code');
            $option_value = new Application_Model_Option_Value();
            $option_value = $option_value->findFolderValues($this->getApplication()->getId(),$option->getId());
            $data = array();
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());
            foreach ($option_value as $key => $value) {
                if(!in_array($value->getId(), $lock_values_id))
                {
                    $video = new Media_Model_Gallery_Video();
                    $videos = $video->findAll(array('value_id' => $value->getId()));

                    foreach ($videos as $key => $videogallery) {
                        if($videogallery->getTypeId() == "youtube") {
                            $youtube_gallerie = new Media_Model_Gallery_Video_Youtube();
                            $youtube_gallerie = $youtube_gallerie->find(array("gallery_id" => $videogallery->getGalleryId()));
                            $data[] = array(
                                "value_id"  => $value->getValueId(),  
                                "name" => $videogallery->getName(),
                                "gallery_id" => $youtube_gallerie->getGalleryId(),
                                "search_by" => $youtube_gallerie->getType(),
                                "search_keyword" => $youtube_gallerie->getParam(),
                                "youtube_key" =>  Api_Model_Key::findKeysFor('youtube')->getApiKey()
                            );
                        }
                    }
                }
            }
        }  catch (Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage()); 
        }

        $this->_sendHtml($data);
    }


}