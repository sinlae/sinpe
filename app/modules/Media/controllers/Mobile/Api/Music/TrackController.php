<?php

class Media_Mobile_Api_Music_TrackController extends Application_Controller_Mobile_Default {


    public function _toJson($track) {
        if($track->getType() == 'itunes'){
            $bigArtwork = str_replace('100x100bb', '400x2400bb', $track->getArtworkUrl());
        } else if($track->getType() == 'soundcloud'){
            $bigArtwork = str_replace('large', 't500x500', $track->getArtworkUrl());
        } else {
            $bigArtwork = $track->getArtworkUrl();
        }
        $json = array(
            "id" => $track->getId(),
            "name" => $track->getName(),
            "artistName" => $track->getArtistName(),
            "albumName" => $track->getAlbumName(),
            "albumId" => $track->getAlbumId(),
            "duration" => $track->getDuration(),
            "streamUrl" => $track->getStreamUrl(),
            "purchaseUrl" => $track->getPurchaseUrl(),
            "artWorkUrl" => $track->getArtworkUrl(),
            "bigArtwork" => file_get_contents($bigArtwork) ? $bigArtwork : $track->getArtworkUrl(),
            "lyrics" =>  $track->getLyrics() ? $track->getLyrics()."<br/><br/>": null,
        );

        if($track->getType() != 'podcast'){
            if($track->getType() == 'itunes' && $track->getPrice() > 0) {
                $json["duration"] = "29000";
            }
            $json["formatedDuration"] = $track->getFormatedDuration($track->getDuration());
            if($track->getType() == "soundcloud") {
                $json["streamUrl"] = $json["streamUrl"]."?client_id=".Api_Model_Key::findKeysFor("soundcloud")->getClientId();
            }
        }else{
            $json["formatedDuration"] = $track->getFormatedDuration();
        }

        return $json;
    }

    public function findbyalbumAction() {

        if($value_id = $this->getRequest()->getParam('value_id')
            && ($album_id = $this->getRequest()->getParam('album_id') OR $track_id = $this->getRequest()->getParam('track_id'))) {

            try {

                $album_tracks = array();
                $json = array();

                if($album_id) {

                    $album = new Media_Model_Gallery_Music_Album();
                    $album->find($album_id);

                    $album_tracks = $album->getAllTracks(true);

                } else if($track_id) {

                    $track = new Media_Model_Gallery_Music_Track();
                    $track->find($track_id);

                    $album_tracks = array($track);

                }

                foreach($album_tracks as $track) {
                    $json[] = $this->_toJson($track);
                }

                $data = array("tracks" => $json);

            } catch(Exception $e) {

            }

        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }
        $this->_sendHtml($data);
    }

    public function addplayAction() {
        if($track_id = $this->getRequest()->getParam('track_id')){
            $track = new Media_Model_Gallery_Music_Track();
            $track = $track->find(array("track_id"=>$track_id));
            $track->setNbPlay($track->getNbPlay()+1)
                  ->save();
            $data = array('success' => 1);  
            $this->_sendHtml($data);   
        } 
    }

    public function getlyricsAction(){
        if($track_id = $this->getRequest()->getParam('track_id')) {

            try {

                $album_tracks = array();
                $json = array();

                if($track_id) {

                    $track = new Media_Model_Gallery_Music_Track();
                    $track->find($track_id);
                    $json = $this->_toJson($track);
                }

                $data = $json;

            } catch(Exception $e) {

            }

        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }
        $this->_sendHtml($data);
    }

}