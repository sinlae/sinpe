<?php

class Media_Mobile_Api_Music_AlbumController extends Application_Controller_Mobile_Default {


    public function _toJson($album) {

        if($album instanceof Media_Model_Gallery_Music_Album) {
            $total_duration = $album->getTotalDuration();
            $total_tracks = $album->getTotalTracks();
            $url = $this->getPath("media/mobile_gallery_music_album/index", array("value_id" => $this->getRequest()->getParam("value_id"), "album_id" => $album->getId()));
            $element = "album";
        } else {
            $total_duration = $album->getFormatedDuration();
            $total_tracks = 1;
            $url = $this->getPath("media/mobile_gallery_music_album/index", array("value_id" => $this->getRequest()->getParam("value_id"), "track_id" => $album->getId()));
            $element = "track";
        }

        $json = array(
            "id" => $album->getId(),
            "name" => $album->getName(),
            "artworkUrl" => $album->getArtworkUrl(),
            "artistName" => $album->getArtistName(),
            "totalDuration" => $total_duration,
            "totalTracks" => $total_tracks,
            "path" => $url,
            "type" => $album->getType(),
            "element" => $element
        );

        return $json;
    }

    public function findAction() {

        if($value_id = $this->getRequest()->getParam('value_id')
           && ($album_id = $this->getRequest()->getParam('album_id') OR $track_id = $this->getRequest()->getParam('track_id'))) {

            try {

                $elements = new Media_Model_Gallery_Music_Elements();
                if($album_id) {

                    $element = $elements->find($album_id, "album_id");

                    $album = new Media_Model_Gallery_Music_Album();
                    $album->find($element->getAlbumId());

                    $data = array("album" => $this->_toJson($album));

                } else if($track_id) {
                    $element = $elements->find($track_id, "track_id");

                    $track = new Media_Model_Gallery_Music_Track();
                    $track->find($element->getTrackId());

                    $data = array("album" => $this->_toJson($track));

                } else {
                    $data = array('error' => 1, 'message' => $this->_("This element is not an album."));
                }
            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        } else {
            $data = array('error' => 1, 'message' => $this->_("An error occurred while loading. Please try again later."));
        }

        $this->_sendHtml($data);

    }

    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try {

                $playlists = new Media_Model_Gallery_Music();
                $playlists = $playlists->findAll(array('value_id' => $value_id), 'position ASC');


                $json = array();

                foreach($playlists as $playlist) {

                    $elements = new Media_Model_Gallery_Music_Elements();
                    $elements = $elements->findAll(array('gallery_id' => $playlist->getId()), 'position ASC');

                    foreach($elements as $element) {

                        if($element->getAlbumId()) {

                            $album = new Media_Model_Gallery_Music_Album();
                            $album->find($element->getAlbumId());

                            $json[] = $this->_toJson($album);

                        } else if($element->getTrackId()) {

                            $track = new Media_Model_Gallery_Music_Track();
                            $track->find($element->getTrackId());

                            $json[] = $this->_toJson($track);

                        }
                    }
                }

                $data = array("albums" => $json);


            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }


        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }
        $this->_sendHtml($data);
    }

    public function findbyplaylistAction() {


        if($value_id = $this->getRequest()->getParam('value_id')
           && $playlist_id = $this->getRequest()->getParam('playlist_id')) {

            try {

                $elements = new Media_Model_Gallery_Music_Elements();
                $elements = $elements->findAll(array('gallery_id' => $playlist_id), 'position ASC');

                $json = array();

                foreach($elements as $element) {


                    if($element->getAlbumId()) {

                        $album = new Media_Model_Gallery_Music_Album();
                        $album->find($element->getAlbumId());

                        $json[] = $this->_toJson($album);
                    } else if($element->getTrackId()) {

                        $track = new Media_Model_Gallery_Music_Track();
                        $track->find($element->getTrackId());

                        $json[] = $this->_toJson($track);

                    }

                }

                $data = array("albums" => $json);

            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }


        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }
        $this->_sendHtml($data);
    }

    public function findallalbumsAction()
    {
        try 
        {
            $option = new Application_Model_Option();
            $option->find('music_gallery', 'code');
            $option_value = new Application_Model_Option_Value();
            $option_value = $option_value->findFolderValues($this->getApplication()->getId(),$option->getId());
            $data = array();

            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

            foreach ($option_value as $key => $value) {
                if(!in_array($value->getId(), $lock_values_id))
                {
                    $playlists = new Media_Model_Gallery_Music();
                    $playlists = $playlists->findAll(array('value_id' => $value->getId()), 'position ASC');
                    foreach($playlists as $playlist) {
                        $elements = new Media_Model_Gallery_Music_Elements();
                        $elements = $elements->findAll(array('gallery_id' => $playlist->getId()), 'position ASC');
                        foreach($elements as $element) 
                        {
                            if($element->getAlbumId()) 
                            {
                                $album = new Media_Model_Gallery_Music_Album();
                                $album = $album->find($element->getAlbumId());

                                if($album->getType() == 'itunes'){
                                    $bigArtwork = str_replace('100x100bb', '400x2400bb', $album->getArtworkUrl());
                                } else if($album->getType() == 'soundcloud'){
                                    $bigArtwork = str_replace('large', 't500x500', $album->getArtworkUrl());
                                } else {
                                    $bigArtwork = $album->getArtworkUrl();
                                }

                                $data[] = array(
                                    "album_id" => $album->getAlbumId(),
                                    "value_id" => $value->getId(),
                                    "album_artwork"=> $bigArtwork,
                                    "album_name" => $album->getName()
                                );
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
             $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }

        $this->_sendHtml($data);
    }
}
