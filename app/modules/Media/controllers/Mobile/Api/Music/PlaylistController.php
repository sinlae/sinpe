<?php

class Media_Mobile_Api_Music_PlaylistController extends Application_Controller_Mobile_Default {

    public function _toJson($playlist){

        $artworkUrl = null;
        if(is_file(Application_Model_Application::getBaseImagePath().$playlist->getArtworkUrl())) {
            $artworkUrl = Application_Model_Application::getImagePath().$playlist->getArtworkUrl();
        } elseif (file_get_contents($playlist->getArtworkUrl())) {
            $artworkUrl = $playlist->getArtworkUrl();
        } else {
            $artworkUrl = "/images/library/musics/default_album.jpg";
        }

        $json = array(
            "id" => $playlist->getId(),
            "name" => $playlist->getName(),
            "artworkUrl" => $artworkUrl,
            "totalDuration" => $playlist->getTotalDuration(),
            "totalTracks" => $playlist->getTotalTracks()
        );
        return $json;
    }

    public function findAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try 
            {
                $locker_option = $this->getApplication()->getOption("padlock");
                $locker = new Padlock_Model_Padlock();
                $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

                if(in_array($value_id, $lock_values_id))
                {
                   $locked= 1;
                } else {
                   $locked = 2;
                }
                $playlists = new Media_Model_Gallery_Music();

                $playlist = $playlists->getGallery($value_id);
                $option = $this->getCurrentOptionValue();
                $playlist_name = $option->getTabbarName(); 

                $data = array("playlist" => $this->_toJson($playlist), "playlist_id"=>$playlist->getGalleryId(), "playlist_name"=>$playlist_name, "locked"=>$locked);

            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        } else {
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }

        $this->_sendHtml($data);

    }

    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try {


                $locker_option = $this->getApplication()->getOption("padlock");
                $locker = new Padlock_Model_Padlock();
                $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

                if(in_array($value_id, $lock_values_id))
                {
                   $locked= 1;
                } else {
                   $locked = 2;
                }

                $playlists = new Media_Model_Gallery_Music();
                $playlists = $playlists->findAll(array('value_id' => $value_id), 'position ASC');

                $json = array();
                foreach($playlists as $playlist) {
                    $json[] = $this->_toJson($playlist);
                }

                $data = array(
                    "playlists" => $json,
                    "artwork_placeholder" => Media_Model_Library_Image::getImagePathTo("/musics/default_album.jpg"),
                    "locked" => $locked
                );


            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        } else {
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }

        $this->_sendHtml($data);

    }

    public function getpagetitleAction() {
        $option = $this->getCurrentOptionValue();
        $data['page_title'] = $option->getTabbarName();
        $this->_sendHtml($data);
    }

}