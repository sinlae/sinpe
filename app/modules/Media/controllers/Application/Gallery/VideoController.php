<?php

class Media_Application_Gallery_VideoController extends Application_Controller_Default
{

    public function editpostAction() {

        if($datas = $this->getRequest()->getPost()) {

            $html = '';

            try {

                // Test s'il y a un value_id
                if(empty($datas['value_id'])) throw new Exception($this->_("An error occurred while saving your videos gallery. Please try again later."));

                // Récupère l'option_value en cours
                $option_value = new Application_Model_Option_Value();
                $option_value->find($datas['value_id']);

                $isNew = true;
                $video = new Media_Model_Gallery_Video();
                if(!empty($datas['id'])) {
                    $video->find($datas['id']);
                    $isNew = false;
                }
                else {
                    $datas['value_id'] = $option_value->getId();
//                    $datas['type_id'] = 'youtube';
                }
                if($video->getId() AND $video->getValueId() != $option_value->getId()) {
                    throw new Exception($this->_("An error occurred while saving your videos gallery. Please try again later."));
                }

                $video->setData($datas)->save();

                $html = array(
                    'success' => 1,
                    'is_new' => (int) $isNew,
                    'id' => (int) $video->getId(),
                    'success_message' => $this->_('Videos gallery has been saved successfully'),
                    'message_timeout' => 2,
                    'message_button' => 0,
                    'message_loader' => 0
                );

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($html));

        }

    }

    public function uploadvideoAction() {
    try 
    {
        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        if ( 0 < $_FILES['file']['error'] ) 
        {
            throw new Exception($this->_("File error"));
        } if($extension != "mp4" && $extention != "3gp") {
            throw new Exception($this->_("Bad extention mp4 or 3gp only")); 
        } elseif($_FILES['file']['size'] > 10485760) {
            throw new Exception($this->_("File is too big")); 
        }
        else {

            $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
            $config=parse_ini_file($ini);

            $relativePath = "/tmp/";
            $fullPath = Application_Model_Application::getBaseImagePath().$relativePath;
            $image = $_FILES['file']['name'];
            $fileName = $this->getApplication()->getAppId().'-'.uniqid() . '.' . $extension;
            if (!is_dir($fullPath)) mkdir($fullPath, 0777, true);
            $filePath = $fullPath . '/' . $fileName;

            move_uploaded_file($_FILES['file']['tmp_name'], $filePath);

            $bucket=$config["bucketVideoName"];

            $awsAccessKey = $config["awsAccessKey"];
            $awsSecretKey = $config["awsSecretKey"];
         
            //create s3 client
            $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

            $result = $client->putObject($bucket."/".$fileName, file_get_contents($filePath),
                    array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
                     Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));

            unlink($filePath);

            $html = array(
                'success'=> 1,
                'url' => $config["cloudFrontVideoUrl"].$fileName
                );
        }
    } catch(Exception $e) {
        $html = array(
            "error" => 1,
            "message" => $e->getMessage()
            );
    }

       $this->_sendHtml(json_encode($html));
    }


}