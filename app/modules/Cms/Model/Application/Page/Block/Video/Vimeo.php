<?php

class Cms_Model_Application_Page_Block_Video_Vimeo extends Core_Model_Default {

 	private $_videos;
    private $_link;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Cms_Model_Db_Table_Application_Page_Block_Video_Vimeo';
        return $this;
    }

    public function isValid() {
        if($this->getVimeo()) return true;
        return false;
    }

     public function getList($search, $type = "video_id") {

        if(!$this->_videos) {
            $id = explode(".com/", $search);
            $id = $id[1];
        	$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$id.".php"));
	    	$image =  $hash[0]['thumbnail_medium']; 
            $title = $hash[0]['title']; 
            $description = $hash[0]['description'];
            $this->_videos = array();
                        $video = new Core_Model_Default(array(
                            'id'     => $id,
                            'title'        => $title,
                            'description'  => $description,
                            'link'         => "http://player.vimeo.com/video/".$id."?autoplay=1",
                            'image'        => $image
                        ));

    		$this->_videos[0] = $video;

        }

         return $this->_videos;
     }

    public function getVideo($id) {

    }

 	public function getImageFromVimeo() {
    	    $id = $this->getVimeo();
    	    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$id.".php"));
    	    return $hash[0]['thumbnail_medium']; 
  	}	

    public function getImageUrl() {
        if(!$this->getImage()) {
            $id = $this->getVimeo();
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$id.".php"));
            return $hash[0]['thumbnail_medium']; 
        } else {
            return $this->getImage();
        }
    }

    protected function setLink($url) {
        $this->_link = $url;
    }

    protected function getLink() {
        return $this->_link;
    }
}