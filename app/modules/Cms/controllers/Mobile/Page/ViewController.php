<?php

class Cms_Mobile_Page_ViewController extends Application_Controller_Mobile_Default
{


    public function _toJson($block) {

        $block->unsMobileTemplate()->unsTemplate();
        $block_data = $block->getData();

        switch($block->getType()) {
            case "text":
                $block_data["image_url"] = $block->getImageUrl();
            break;
            case "image":
                $library = new Cms_Model_Application_Page_Block_Image_Library();
                $libraries = $library->findAll(array('library_id' => $block->getLibraryId()), 'image_id ASC', null);
                $block_data["gallery"] = array();
                foreach($libraries as $image) {
                    $block_data["gallery"][] = array(
                        "id" => $image->getId(),
                        "url" => $image->getImageFullSize()
                    );
                }
            break;
            case "video":
                $video = $block->getObject();
                if($video->getTypeId() == "youtube") {
                    $youtube_video = new Cms_Model_Application_Page_Block_Video_Youtube();
                    $youtube_video = $youtube_video->find(array('video_id'=>$video->getVideoId()));
                    $block_data["cover_url"] = $youtube_video->getImageUrl();
                } elseif($video->getTypeId() == "vimeo"){
                    $vimeo_video = new Cms_Model_Application_Page_Block_Video_Vimeo();
                    $vimeo_video = $vimeo_video->find(array('video_id'=>$video->getVideoId()));
                    $block_data["cover_url"] = $vimeo_video->getImageUrl();
                }else {
                    $block_data["cover_url"] = $video->getImageUrl();
                }
                $url = $video->getLink();
                $video_id = $video->getId();
                if($video->getTypeId() == "youtube") {
                    $url = "http://www.youtube.com/embed/{$video->getYoutube()}?autoplay=1";
                    $video_id = $video->getYoutube();
                }
                if($video->getTypeId() == "vimeo") {
                    $url = "http://player.vimeo.com/video/{$video->getVimeo()}?autoplay=1";
                    $video_id = $video->getVimeo();
                }

                $block_data["url"] = $url;
                $block_data["video_id"] = $video_id;
                $block_data["description"] = $video->getDescription();
                $block_data["title"] = $video->getTitle();
            break;
        }

        return $block_data;

    }


    public function findAction() {
        if($value_id = $this->getRequest()->getParam('value_id')
           && $page_id = $this->getRequest()->getParam('page_id')) {

            try {

                $option_value = $this->getCurrentOptionValue();

                $pageRepository = new Cms_Model_Application_Page();
                $page = $pageRepository->find($page_id);

                $json = array();

                $blocks = $page->getBlocks();
                $json = array();

                foreach($blocks as $block) {
                    $json[] = $this->_toJson($block);
                }
                $data = array(
                    "blocks" => $json,
                    "page_title" => $page->getTitle(),
                    "picture" => $page->getPictureUrl(),
                    "social_sharing_active" => $option_value->getSocialSharingIsActive()
                );
            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }

        $this->_sendHtml($data);
    }

    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            $option = $this->getCurrentOptionValue();
            $application_option = new Application_Model_Option();
            $application_option = $application_option->find(array("option_id"=>$option->getOptionId()));
            $page = new Cms_Model_Application_Page();
            $page->find($option->getId(), 'value_id');
            if($application_option->getCode() == "cmsvideo"){
                $blocks = $page->getBlocks(true);
            } else {
                $blocks = $page->getBlocks();
            }
            
            $data = array("blocks" => array());

            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

            if(in_array($value_id, $lock_values_id))
            {
               $data["locked"] = 1;
            } else {
               $data["locked"] = 2;
            }

            foreach($blocks as $block) {
                $data["blocks"][] = $this->_toJson($block);
            }

            $data["page_title"] = $option->getTabbarName();
            $data["social_sharing_active"] = $option->getSocialSharingIsActive();
            $data["cover"] = $option->hasCoverImage();

            $this->_sendHtml($data);
        }

    }

}