<?php

class Radio_Mobile_RadioController extends Application_Controller_Mobile_Default {

    public function _toJson($radio){
        $json = array(
            "url" => $url = addslashes($radio->getLink()),
            'title' => $radio->getTitle()
        );

        return $json;
    }

    public function findAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {
            
            try {
                
                $radio_repository = new Radio_Model_Radio();
                $radio = $radio_repository->find(array('value_id' => $value_id));

                if(substr($radio->getLink(), -1) == "/") {
                    $stream_tag = ";stream.nsv&type=mp3";
                } else {
                    if(mb_stripos($radio->getLink(), "/", 8) > 0) {
                        $stream_tag = "?stream.nsv&type=mp3";
                    } else {
                        $stream_tag = "/;stream.nsv&type=mp3";
                    }
                }
                $url = $radio->getLink();
                $radio->setLink($radio->getLink().$stream_tag);

                $data = array("radio" => $this->_toJson($radio),"url"=>$url, "image_url"=>$radio->getImage());
            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        } else {
            $data = array('error' => 1, 'message' => $this->_("An error occurred while loading. Please try again later."));
        }

        $this->_sendHtml($data);

    }

    public function gettitleAction(){
        if($value_id = $this->getRequest()->getParam('value_id')) {
            $radio_repository = new Radio_Model_Radio();
            $radio = $radio_repository->find(array('value_id' => $value_id));
            $radio_model = new Radio_Model_Radio();
            if(strpos($radio->getLink(),'coreradio')!== false){
                $title="Unknown";
            } else {
                $title = $radio_model->getMp3StreamTitle($radio->getLink(), 19200);
            }
            $data = array("song_title" => $title); 
        } else {
             $data = array('error' => 1, 'message' => $this->_("An error occurred while loading. Please try again later."), "song_title" => "Unknown");
        }
         //$data = array("song_title" => "sdsdsds"); 
        $this->_sendHtml($data);
    }
}