<?php

class Radio_ApplicationController extends Application_Controller_Default
{

    public function editpostAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                // Test s'il y a un value_id
                if(empty($data['value_id'])) throw new Exception($this->_('An error occurred while saving. Please try again later.'));

                // Récupère l'option_value en cours
                $option_value = new Application_Model_Option_Value();
                $option_value->find($data['value_id']);

                // Test s'il y a embrouille entre la value_id en cours de modification et l'application en session
                if(!$option_value->getId() OR $option_value->getAppId() != $this->getApplication()->getId()) {
                    throw new Exception($this->_('An error occurred while saving. Please try again later.'));
                }

                // Test s'il y a embrouille entre la value_id en cours de modification et l'application en session
                if(empty($data['link']) OR !Zend_Uri::check($data['link'])) {
                    throw new Exception($this->_('Please enter a valid url'));
                }

                $radio = new Radio_Model_Radio();
                $radio->find($option_value->getId(), 'value_id');
                if(!$radio->getId()) {
                    $radio->setValueId($data['value_id']);
                }

                if(empty($data["image"])) {

                    $data["image"] = null;

                } else if(file_exists(Core_Model_Directory::getTmpDirectory(true)."/".$data["image"])) {

                    $application = $this->getApplication();
                    $relative_path = $option_value->getImagePathTo();
                    $folder = Application_Model_Application::getBaseImagePath() . $relative_path;
                    $path = Application_Model_Application::getBaseImagePath() . $relative_path;
                    $file = Core_Model_Directory::getTmpDirectory(true).'/'.$data['image'];
                    if(!is_dir($path)) mkdir($path, 0777, true);
                    if(!copy($file, $folder.$data['image'])) {
                        throw new exception($this->_('An error occurred while saving. Please try again later.'));
                    } else {
                        $url = $this->_saveImageContent($folder.$data['image'], $option_value, $file);
                        $data['image'] = $url;
                    }

                }

                $radio->addData($data)
                    ->save()
                ;

                $html = array(
                    'success' => '1',
                    'success_message' => $this->_('Info successfully saved'),
                    'message_timeout' => 2,
                    'message_button' => 0,
                    'message_loader' => 0
                );
                if(!$radio->getIsDeleted()) {
                    $html['link'] = $radio->getLink();
                }

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->getResponse()
                ->setBody(Zend_Json::encode($html))
                ->sendResponse()
            ;
            die;

        }

    }

    protected function _saveImageContent($image, $option_value, $img_src) {
      $relativePath = $this->getCurrentOptionValue()->getImagePathTo();
      $fullPath = Application_Model_Application::getBaseImagePath().$relativePath;
      $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
      $config=parse_ini_file($ini);
      $id_filename = str_replace('/','', $relativePath);
      $awsAccessKey = $config["awsAccessKey"];
      $awsSecretKey = $config["awsSecretKey"];

       //create s3 client
      $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);
       
       $uploader = new Core_Model_Lib_Uploader();
       $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client, false);
       unlink($img_src);  
       unlink($image);
       return $url;
    }

}