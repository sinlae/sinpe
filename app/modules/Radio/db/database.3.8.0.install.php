<?php

$this->query('
    CREATE TABLE `radio` (
        `radio_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `value_id` INT(11) UNSIGNED NOT NULL,
        `title` VARCHAR(60) NULL DEFAULT NULL,
        `link` VARCHAR(255) NULL DEFAULT NULL,
        `created_at` DATETIME NOT NULL,
        `updated_at` DATETIME NOT NULL,
        PRIMARY KEY (`radio_id`)
    ) ENGINE=InnoDB CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;
');

$library = new Media_Model_Library();
$library->setName("Radio")->save();

$images = array(
    '/radio/radio1.png',
    '/radio/radio2.png',
    '/radio/radio3.png',
    '/radio/radio4.png',
    '/radio/radio5.png',
    '/radio/radio6.png',
    '/radio/radio7.png',
    '/radio/radio8.png'
);

$icon_id = 0;
foreach($images as $key => $image) {
    $data = array('library_id' => $library->getId(), 'link' => $image, 'can_be_colorized' => 1);
    $image = new Media_Model_Library_Image();
    $image->setData($data)->save();
    if($key == 0) $icon_id = $image->getId();
}


$data = array(
    'library_id' => $library->getId(),
    'icon_id' => $icon_id,
    'code' => "radio",
    'name' => "Radio",
    'model' => "Radio_Model_Radio",
    'desktop_uri' => "radio/application/",
    'mobile_uri' => "radio/mobile_radio/",
    'only_once' => 0,
    'is_ajax' => 1,
    'position' => 105
);
$option = new Application_Model_Option();
$option->setData($data)->save();
