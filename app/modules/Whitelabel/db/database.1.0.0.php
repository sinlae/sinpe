<?php

$this->query("
    CREATE TABLE `whitelabel_editor` (
        `editor_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `admin_id` INT(11) UNSIGNED NOT NULL,
        `host` VARCHAR(100) DEFAULT NULL,
        `is_active` TINYINT(1) NOT NULL DEFAULT 0,
        `logo` VARCHAR(255) NULL DEFAULT NULL,
        `favicon` VARCHAR(255) NULL DEFAULT NULL,
        `created_at` DATETIME NOT NULL,
        `updated_at` DATETIME NOT NULL,
        PRIMARY KEY (`editor_id`),
        KEY KEY_ADMIN_ID (`admin_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `template_block_white_label_editor` (
        `block_id` INT(11) NOT NULL,
        `white_label_editor_id` INT(11) UNSIGNED NOT NULL,
        `color` VARCHAR(10) NOT NULL,
        `color_on_hover` VARCHAR(10) NOT NULL,
        `color_on_active` VARCHAR(10) NOT NULL,
        `background_color` VARCHAR(10) NOT NULL,
        `background_color_on_hover` VARCHAR(10) NOT NULL,
        `background_color_on_active` VARCHAR(10) NOT NULL,
        PRIMARY KEY (`block_id`, `white_label_editor_id`),
        KEY KEY_WHITE_LABEL_EDITOR_ID (`white_label_editor_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    ALTER TABLE `whitelabel_editor`
        ADD CONSTRAINT `FK_WHITE_LABEL_EDITOR_ADMIN_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `template_block_white_label_editor`
        ADD CONSTRAINT `FK_TEMPLATE_BLOCK_WHITE_LABEL_EDITOR_WHITE_LABEL_EDITOR_ID`
            FOREIGN KEY (`white_label_editor_id`) REFERENCES `whitelabel_editor` (`editor_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");