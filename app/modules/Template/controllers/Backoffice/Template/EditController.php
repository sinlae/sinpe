<?php

class Template_Backoffice_Template_EditController extends Backoffice_Controller_Default
{
	    public function findAction() {
        try{
                if(!$this->getRequest()->getParam("layout_id")) throw new Exception($this->_('No layout selected'));

                $template = new Application_Model_Option_Layout();
                $template = $template->find(array("layout_id"=>$this->getRequest()->getParam("layout_id")));
                $data = array();
                $data["template"] = array(
                     "layout_id" => $template->getLayoutId(),
                     "code" => $template->getCode(),
                     "option_id" => $template->getOptionId(),
                     "name" => $template->getName(),
                     "preview" => $template->getPreview(),
                     "position" => $template->getPosition(),
                     "template_level" => $template->getTemplateLevel()
                 );
                 $data["success"] = 1;

             } catch(Exception $e) {
                 $data = array(
                     "error" => 1,
                     "message" => $e->getMessage()
                 );
             } 

            //]$data["template"] = $template;

            $this->_sendHtml($data); 
    }
}
?>