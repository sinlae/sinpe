<?php

class Template_Backoffice_Template_ListController extends Backoffice_Controller_Default
{

    public function findallAction() {

        $templates = new Application_Model_Option_Layout();
        $templates = $templates->findAll();
        $data = array();
        $option = new Application_Model_Option();
        foreach($templates as $template) {
            $option = $option->find(array("option_id"=>$template->getOptionId()));
            $data[] = array(
                "layout_id" => $template->getLayoutId(),
                "code" => $template->getCode(),
                "option_id" => $template->getOptionId(),
                "option_name" => $option->getName(),
                "name" => $template->getName(),
                "preview" => $template->getPreview(),
                "position" => $template->getPosition(),
                "template_level" => $template->getTemplateLevel()
            );
        }

        $this->_sendHtml($data);
    }

    public function findbyoptionAction() {
        try{
                if(!$this->getRequest()->getParam("option_id")) throw new Exception($this->_('No option selected'));

                $templates = new Application_Model_Option_Layout();
                $templates = $templates->findAll(array("option_id"=>$this->getRequest()->getParam("option_id")));
                $data = array();
                $option = new Application_Model_Option();
                foreach($templates as $template) {
                    $option = $option->find(array("option_id"=>$template->getOptionId()));
                    $data["templates"][] = array(
                        "layout_id" => $template->getLayoutId(),
                        "code" => $template->getCode(),
                        "option_id" => $template->getOptionId(),
                        "option_name" => $option->getName(),
                        "name" => $template->getName(),
                        "preview" => $template->getPreview(),
                        "position" => $template->getPosition(),
                        "template_level" => $template->getTemplateLevel()
                    );
                }
                $data["success"] = 1;

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            } 

            $this->_sendHtml($data); 
    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                $template = new Application_Model_Option_Layout();



                $template->setData($data)
                    ->save()
                ;

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Template successfully saved")
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
