<?php

$this->query("

    ALTER TABLE `template_block`

        ADD `type_id` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `block_id`,

        ADD `use_color_on_hover` TINYINT(1) NOT NULL DEFAULT '0' AFTER `background_color`,
        ADD `color_on_hover` VARCHAR(10) NULL DEFAULT NULL AFTER `use_color_on_hover`,
        ADD `background_color_on_hover` VARCHAR(10) NULL DEFAULT NULL AFTER `color_on_hover`,

        ADD `use_color_on_active` TINYINT(1) NOT NULL DEFAULT '0' AFTER `background_color_on_hover`,
        ADD `color_on_active` VARCHAR(10) NULL DEFAULT NULL AFTER `use_color_on_active`,
        ADD `background_color_on_active` VARCHAR(10) NULL DEFAULT NULL AFTER `color_on_active`

    ;

");

$this->_db->update("template_block", array("type_id" => 1), array());

$block_data = array(array(
        "code" => "general",
        "name" => "General",
        "color" => "#12A7FF",
        "background_color" => "#FFFFFF",
        "use_color_on_hover" => 0,
        "color_on_hover" => null,
        "background_color_on_hover" => null,
        "use_color_on_active" => 0,
        "color_on_active" => null,
        "background_color_on_active" => null,
        "position" => 10
    ), array(
        "code" => "header",
        "name" => "Header",
        "color" => "#FFFFFF",
        "background_color" => "#404040",
        "use_color_on_hover" => 1,
        "color_on_hover" => "#FFFFFF",
        "background_color_on_hover" => "#404040",
        "use_color_on_active" => 1,
        "color_on_active" => "#FFFFFF",
        "background_color_on_active" => "#404040",
        "position" => 20
    ), array(
        "code" => "app_name",
        "name" => "App Name",
        "color" => "#FFFFFF",
        "background_color" => "#4DBDFF",
        "use_color_on_hover" => 0,
        "color_on_hover" => null,
        "background_color_on_hover" => null,
        "use_color_on_active" => 0,
        "color_on_active" => null,
        "background_color_on_active" => null,
        "position" => 30
    ), array(
        "code" => "inner_content",
        "name" => "Page",
        "color" => "#404040",
        "background_color" => "#FFFFFF",
        "use_color_on_hover" => 0,
        "color_on_hover" => null,
        "background_color_on_hover" => null,
        "use_color_on_active" => 0,
        "color_on_active" => null,
        "background_color_on_active" => null,
        "position" => 40
    ), array(
        "code" => "editor_menu",
        "name" => "Menu",
        "color" => "#FFFFFF",
        "background_color" => "#4DBDFF",
        "use_color_on_hover" => 1,
        "color_on_hover" => "#FFFFFF",
        "background_color_on_hover" => "#12A7FF",
        "use_color_on_active" => 1,
        "color_on_active" => "#FFFFFF",
        "background_color_on_active" => "#12A7FF",
        "position" => 50
    ), array(
        "code" => "area",
        "name" => "Area",
        "color" => "#FFFFFF",
        "background_color" => "#4DBDFF",
        "use_color_on_hover" => 0,
        "color_on_hover" => null,
        "background_color_on_hover" => null,
        "use_color_on_active" => 0,
        "color_on_active" => null,
        "background_color_on_active" => null,
        "position" => 60
    ), array(
        "code" => "default_button",
        "name" => "Buttons",
        "color" => "#FFFFFF",
        "background_color" => "#4DBDFF",
        "use_color_on_hover" => 1,
        "color_on_hover" => "#4DBDFF",
        "background_color_on_hover" => "#FFFFFF",
        "use_color_on_active" => 1,
        "color_on_active" => "#404040",
        "background_color_on_active" => "#4DBDFF",
        "position" => 70
    ), array(
        "code" => "delete",
        "name" => "Delete/Cancel buttons",
        "color" => "#FB5384",
        "background_color" => "#FFFFFF",
        "use_color_on_hover" => 1,
        "color_on_hover" => "#FFFFFF",
        "background_color_on_hover" => "#FB5384",
        "use_color_on_active" => 1,
        "color_on_active" => "#404040",
        "background_color_on_active" => "#FB5384",
        "position" => 80
    ), array(
        "code" => "select",
        "name" => "Drop Down",
        "color" => "#4DBDFF",
        "background_color" => "#FFFFFF",
        "use_color_on_hover" => 1,
        "color_on_hover" => "#FFFFFF",
        "background_color_on_hover" => "#4DBDFF",
        "use_color_on_active" => 1,
        "color_on_active" => "#4DBDFF",
        "background_color_on_active" => "#FFFFFF",
        "position" => 90
    )
);

foreach($block_data as $data) {
    $data["type_id"] = 2;
    $block = new Template_Model_Block();
    $block->setData($data)->save();
}
