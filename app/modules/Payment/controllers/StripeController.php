<?php
require_once('Stripe/init.php');
class Payment_StripeController extends Core_Controller_Default
{
  public function checkstripepaymentAction(){
  	$now = Zend_Date::now(); 
    
  	$subscriptions = new Subscription_Model_Subscription_Application();
  	$subscriptions = $subscriptions->findAll(array("payment_method"=>"stripe", "is_active"=>1));
  	foreach ($subscriptions as $key => $subscription) {
  		 if($now->get('YYYY-MM-dd HH:mm:ss') > $subscription->getExpireAt()) {
  		 	$is_recurrent = new Subscription_Model_Subscription_Application_Detail();
           	$is_recurrent = $is_recurrent->find(array("subscription_app_id" => $subscription->getAppId(), "code" => "is_recurrent"));
  		 	if($is_recurrent->getValue()=="1"){
  		 		 $api_key = Api_Model_Key::findKeysFor('stripe');
  		 		\Stripe\Stripe::setApiKey($api_key->getSecretKey()); 
	  	 		 $cu = \Stripe\Customer::retrieve($subscription->getCustomerId());
	  			 $application = new Application_Model_Application();
	  	 		 $application = $application->find(array("app_id"=>$subscription->getAppId()));
	  	 		 if($cu->id && ($cu->delinquent == false)){
	  	 		 	$expire_at = new Siberian_Date($subscription->getExpireAt(),"y-MM-dd HH:mm:ss");
	  	 		 	$expire_at->addYear(1);
	  	 		 	$subscription->setExpireAt($expire_at);
	  	 		 	$subscription->save();
	  	 		 } else {
	  	 		 	$application->setIsLocked(true)->save();
	     		 	$subscription->setIsActive(0)->save();
	  	 		 }
	  	 	} else {
	  	 		$subscription->setIsActive(0)->save();
	  	 		$application->setIsLocked(true)->save();
	  	 	}
  		 }
  	}
  	die;
  }
}