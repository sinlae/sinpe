<?php

class Payment_Model_2Checkout extends Payment_Model_Abstract {

    private $__base_url = "https://sandbox.2checkout.com/";
    private $__add_purchase_url = "checkout/purchase";
    private $__add_list_sale = "api/sales/detail_sale";
    private $__stop_line_item ='api/sales/stop_lineitem_recurring';

    public function __construct($sid = false, $secret = false, $user = false, $password = false) {

        $this->_code = "2checkout";

        if($sid AND $secret AND $user AND $password) {

            $this->__sid = $sid;
            $this->__secret = $secret;
            $this->__user = $user;
            $this->__password = $password;

        } else {
            $provider_name = new Api_Model_Provider();
            $provider_name->find($this->_code, "code");
            $keys = $provider_name->getKeys();

            foreach ($keys as $key) {
                switch ($key->getKey()) {
                    case "sid":
                        $this->__sid = $key->getValue();
                        break;
                    case "secret":
                        $this->__secret = $key->getValue();
                        break;
                    case "username":
                        $this->__user = $key->getValue();
                        break;
                    case "password":
                        $this->__password = $key->getValue();
                        break;
                }
            }

            if (!$this->__sid OR !$this->__secret OR !$this->__user OR !$this->__password) {
                throw new Exception("Error, 2Checkout is not properly set up.");
            }
        }

        if($this->isProduction()) {
            //$this->__base_url = str_replace('sandbox.', 'www.', $this->__base_url);
        }

        $this->__purchase_url = $this->__base_url.$this->__add_purchase_url;
        $this->__list_sale_url = $this->__base_url.$this->__add_list_sale;
        $this->__stop_line_item_url = $this->__base_url.$this->__stop_line_item;
        
    }

    public function getPaymentData($order) {
        $return_url = parent::getUrl('subscription/application/success');
        $cancel_url = parent::getUrl('subscription/application/cancel', array('payment_method' => $this->_code));

        $this->setOrder($order)
             ->setReturnUrl($return_url)
             ->setCancelUrl($cancel_url);

        $lines = $order->getLines();
        $customer_mail = $this->getSession()->getAdmin()->getEmail();
        $customer_address = $this->getSession()->getAdmin()->getAddress();
        $customer_city = $this->getSession()->getAdmin()->getCity();
        $customer_name = $this->getSession()->getAdmin()->getFirstname()." ".$this->getSession()->getAdmin()->getLastname();
        $customer_country = $this->getSession()->getAdmin()->getCountry();
        $customer_region = $this->getSession()->getAdmin()->getRegion();
        $customer_zip = $this->getSession()->getAdmin()->getZipCode();
        $customer_phone = $this->getSession()->getAdmin()->getPhone();

        $tax_rate = $order->getTaxRate();
        $data = array();
        $data["sid"] = $this->__sid;
        $data["mode"] = "2CO";
        $data["email"] = $customer_mail;
        $data["street_address"] = $customer_address;
        $data["city"] = $customer_city;
        $data["zip"] = $customer_zip;
        $data["country"] = $customer_country;
        $data["state"] = $customer_region;
        $data["card_holder_name"] = $customer_name;
        $data["purchase_step"] = "payment-method";
        $data["payment_method"] = $this->_code;
        $data["demo"] = "false";
        $data["currency_code"] = "";
        $data["phone"] = $customer_phone;

        foreach ($lines as $k => $item) {
            $data["li_" . $k . "_type"] = "product";
            $data["li_" . $k . "_name"] = $item->getName();
            $data["li_" . $k . "_price"] = $item->getTotalPriceExclTax() * (1+ ($tax_rate/100));
            $data["li_" . $k . "_quantity"] = $item->getQty();
            $data["li_" . $k . "_tangible"] = "N";
            if($item->getIsRecurrent()=="1"){
                $data["li_" . $k . "_recurrence"] = "1 ".str_replace("ly","",$order->getSubscription()->getPaymentFrequency());
                $data["li_" . $k . "_duration"] = "Forever";
            }
        }

        $form = $this->_getFormHtml($data);

        return array("form" => $form);
    }

    public function success() {

        if($order = $this->getOrder()) {

            if($key = $this->getKey()) {

                if(!$order->getId()) {
                    throw new Exception($this->_("An error occurred while processing your order. Please, try again later."));
                }

                $payment_is_ok = $this->checkPayment(number_format($order->getTotal(), 2, ".", ""));

                if($payment_is_ok) {
                    $data = array(
                        'payment_data' => array(
                            'order_number' => $this->getOrderNumber(),
                            'is_recurrent' => $order->isRecurrent()
                        )
                    );
                    return $data;
                } else {
                    return false;
                }

            }
            else {
                throw new Exception($this->_("An error occurred while processing your order. Please, try again later."));
            }
        } else {
            return false;
        }
    }

    public function manageRecurring() {

        $data = array("sale_id" => $this->getData("order_number"));
        $lines = $this->doCall($this->__list_sale_url, $data);
        $lines = $this->objectToArray($lines);

        if(!empty($lines["errors"])) {
            throw new Exception($this->_("An error occurred while processing your order. Please, check your 2Checkout credentials or try again later."));
        } else {

            $recurring_lines = $this->getRecurringLines($lines);
            if (isset($recurring_lines[0])) {

                $i = 0;
                $stoppedLineitems = array();
                foreach( $recurring_lines as $value ) {

                    $params = array('lineitem_id' => $value);
                    $result = $this->doCall($this->__stop_line_item_url, $params);
                    $result = Zend_Json::decode($result, true);

                    if ($result['response_code'] == "OK") {
                        $stoppedLineitems[$i] = $value;
                    }

                    $i++;

                }

            } else {
                Zend_Registry::get("logger")->log("No recurring line items to stop for the subscription_id " . $this->getSubscriptionAppId(), Zend_Log::DEBUG);
                // throw new Exception($this->_("No recurring line items to stop."));
            }

            return true;
        }
    }

    protected function getRecurringLines($saleDetail) {
        $i = 0;
        $invoiceData = array();
        while (isset($saleDetail['sale']['invoices'][$i])) {
            $invoiceData[$i] = $saleDetail['sale']['invoices'][$i];
            $i++;
        }
        $invoice = max($invoiceData);
        $i = 0;
        $lineitemData = array();
        while (isset($invoice['lineitems'][$i])) {
            if ($invoice['lineitems'][$i]['billing']['recurring_status'] == "active") {
                $lineitemData[$i] = $invoice['lineitems'][$i]['billing']['lineitem_id'];
            }
            $i++;
        };
        return $lineitemData;
    }

    protected function doCall($url,$params) {
        $ch = curl_init($url);
        $header = array("Accept: application/json");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->__user}:{$this->__password}");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, "2Checkout PHP/0.1.0%s");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $resp = curl_exec($ch);
        curl_close($ch);
        if ($resp === FALSE) {
            return false;
        } else {
            return $resp;
        }
    }

    protected function objectToArray($object) {
        $object = Zend_Json::decode($object, true);
        $array=array();
        foreach($object as $member=>$data) {
            $array[$member]=$data;
        }
        return $array;
    }

    protected function checkPayment($hashAmount) {
        
        if ($hashOrder = $this->getOrderNumber() AND $hashKey = $this->getKey()) {
            
            $hashSecret = $this->__secret;
            $StringToHash = strtoupper(md5($hashSecret.$this->__sid.$hashOrder.$hashAmount));
            
            if($StringToHash != $hashKey) {
                $message = "2Checkout HashKey verification failed.";
                $message .= " Should be: $hashKey but was $StringToHash.";
                $message .= " -- Order: $hashOrder - Amount: $hashAmount";
                Zend_Registry::get("logger")->log($message, Zend_Log::DEBUG);
                return false;
            } else {
                return true;
            }

        } else {
            return false;
        }
    }

    protected function _getFormHtml($data) {

        $html = array();
        $html[] = '<form action="'.$this->__purchase_url.'" method="post">';
        foreach($data as $name => $value) {
            $html[] = sprintf('<input type="hidden" name="%s" value="%s" />', $name, $value);
        }
        $html[] = "</form>";

        return join("<br />", $html);

    }

}

