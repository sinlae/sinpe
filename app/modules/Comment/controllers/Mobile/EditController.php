<?php
class Comment_Mobile_EditController extends Application_Controller_Mobile_Default {

    public function createAction() {

        if ($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                $comment = new Comment_Model_Comment();

                if (!$this->getSession()->getCustomerId())
                    throw new Exception($this->_("You need to be connected to create a post"));

                $comment->setText($data['text']);
                $comment->setCustomerId($this->getSession()->getCustomerId());
                $comment->setValueId($data['value_id']);

                $position = $data['position'];
                if ($position) {
                    $comment->setLatitude($position['latitude']);
                    $comment->setLongitude($position['longitude']);
                }

                $image = $data['image'];
                if ($image) {
                    $url = $this->_saveImageContent($image);
                    $comment->setImage($url);
                }

                $comment->save();

                $message = $this->_('Your post was successfully added');
                $html = array('success' => 1, 'message' => $message);

            } catch (Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($html);
        }
    }

    // Reference parameter
    protected function _saveImageContent($image) {

        $relativePath = $this->getCurrentOptionValue()->getImagePathTo();
        $fullPath = Application_Model_Application::getBaseImagePath() . $relativePath;
        $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
        $config=parse_ini_file($ini);

        $awsAccessKey = $config["awsAccessKey"];
        $awsSecretKey = $config["awsSecretKey"];
         
         //create s3 client
         $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);
         
         $id_filename = $this->getApplication()->getAppId()."/comment-mobile";
         $uploader = new Core_Model_Lib_Uploader();
         $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client);
         return $url;
    }

    public function deleteAction() {
        $html = '';
        if($id = $this->getRequest()->getParam('comment_id')) {
            try {
                $comment = new Comment_Model_Comment();
                $comment->find($id)->delete();
                $html = array(
                    'success' => '1',
                    'success_message' => $this->_('Information successfully deleted'),
                    'message_timeout' => 2,
                    'message_button' => 0,
                    'message_loader' => 0
                );
            } catch (Exception $e) {
                $html = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }
            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }
    }

}