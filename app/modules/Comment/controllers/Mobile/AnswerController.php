<?php

class Comment_Mobile_AnswerController extends Application_Controller_Mobile_Default {

    public function findallAction() {

        if($comment_id = $this->getRequest()->getParam('comment_id')) {

            $comment = new Comment_Model_Comment();
            $comment->find($comment_id);
            $customer = new Customer_Model_Customer(); 

            if($comment->getId()) {

                $answer = new Comment_Model_Answer();
                $answers = $answer->findByComment($comment->getId());
                $data = array();
                $array_id = 0;
                foreach($answers as $answer) {
                    if($answer->getCustomer()->getImage())
                    {
                       $icon_url =  $answer->getCustomer()->getImage();
                    } 
                    else
                    {
                        $icon_url = $this->getApplication()->getIcon(74);
                    }   
                    $data[] = array(
                        "id" => $answer->getId(),
                        "array_id" => $array_id,
                        "author" => $answer->getCustomer()->getPseudo(),
                        "author_id" => $answer->getCustomer()->getId(),
                        "picture" => $icon_url,
                        "message" => $answer->getText(),
                        "created_at" => $this->_durationSince($answer->getCreatedAt())
                    );
                    $array_id = $array_id + 1;
                }

                $this->_sendHtml($data);
            }

        }

    }

    public function addAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {
            try {

                $customer_id = $this->getSession()->getCustomerId();

                if(empty($customer_id) OR empty($data['comment_id']) OR empty($data['text'])) {
                    throw new Exception($this->_("An error occurred while saving"));
                }

                $comment_id = $data['comment_id'];
                $text = $data['text'];

                $answer = new Comment_Model_Answer();
                $answer->setCommentId($comment_id)
                    ->setCustomerId($customer_id)
                    ->setText($text)
                    ->save()
                ;
                
                $html = array('success' => 1);

                $message = $this->_('Your message has been successfully saved.');
                if(!$answer->isVisible()) $message .= ' ' . $this->_('It will be visible only after validation by our team.');
                else {

                    $customer = $this->getSession()->getCustomer();
                    if($customer->getImage())
                    {
                       $icon_url =  $customer->getImage();
                    } 
                    else
                    {
                        $icon_url = $this->getApplication()->getIcon(74);
                    }  

                    $answermodel = new Comment_Model_Answer();
                    $answers = $answermodel->findByComment($comment_id);  

                    $html["answer"] = array(
                        "id"     => $answer->getId(),
                        "array_id" => count($answers)-1,
                        "author" => $customer->getPseudo(),
                        "author_id" => $customer->getId(),
                        "picture" => $icon_url,
                        "message" => $answer->getText(),
                        "created_at" => $answer->getFormattedCreatedAt()
                    );

                }

                if($comment_id) {
                    $answer = new Comment_Model_Answer();
                    $answers = $answer->findByComment($comment_id);
                    $data = array();
                    $array_id = 0;
                    foreach($answers as $answer) {
                        if($answer->getCustomer()->getImage())
                        {
                           $icon_url =  $answer->getCustomer()->getImage();
                        } 
                        else
                        {
                            $icon_url = $this->getApplication()->getIcon(74);
                        }   
                        $html["comments"][] = array(
                            "id" => $answer->getId(),
                            "array_id" => $array_id,
                            "author" => $answer->getCustomer()->getPseudo(),
                            "author_id" => $answer->getCustomer()->getId(),
                            "picture" => $icon_url,
                            "message" => $answer->getText(),
                            "created_at" => $this->_durationSince($answer->getCreatedAt())
                        );
                        $array_id = $array_id + 1;
                    }
                }

                $html["message"] = $message;
            }
            catch(Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($html);
        }

    }

    public function addlikeAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                $customer_id = $this->getSession()->getCustomerId();
                $ip = md5($_SERVER['REMOTE_ADDR']);
                $ua = md5($_SERVER['HTTP_USER_AGENT']);
                $like = new Comment_Model_Like();

                if(!$like->findByIp($data['comment_id'], $customer_id, $ip, $ua)) {

                    $like->setCommentId($data['comment_id'])
                        ->setCustomerId($customer_id)
                        ->setCustomerIp($ip)
                        ->setAdminAgent($ua)
                    ;

                    $like->save();

                    $message = $this->_('Your like has been successfully added');
                    $html = array('success' => 1, 'message' => $message);

                } else {
                    throw new Exception($this->_("You can't like more than once the same news"));
                }

            }
            catch(Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($html);
        }

    }

    public function deleteAction() {
        $html = '';
        if($id = $this->getRequest()->getParam('answer_id')) {
            try {
                $answer = new Comment_Model_Answer();
                $answer->find($id)->delete();
                $html = array(
                    'success' => '1',
                    'success_message' => $this->_('Information successfully deleted'),
                    'message_timeout' => 2,
                    'message_button' => 0,
                    'message_loader' => 0
                );
            } catch (Exception $e) {
                $html = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }
            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }
    }

}