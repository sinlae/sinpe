<?php

$this->query("
    CREATE TABLE `subscription_acl_resource` (
        `subscription_resource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `subscription_id` int(11) unsigned NOT NULL,
        `resource_id` int(11) unsigned NOT NULL,
        PRIMARY KEY(subscription_resource_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");