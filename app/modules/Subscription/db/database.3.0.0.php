<?php

$this->query("
    CREATE TABLE `subscription_application_detail` (
        `detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `subscription_app_id` int(11) unsigned NOT NULL,
        `code` varchar(50) NOT NULL,
        `value` varchar(255) NOT NULL,
        PRIMARY KEY (`detail_id`),
        INDEX IDX_SUBSCRIPTION_APP_ID (`subscription_app_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$this->query("
    ALTER TABLE `subscription_application_detail`
        ADD CONSTRAINT FK_SUBSCRIPTION_APPLICATION_DETAIL_SUBSCRIPTION_APP_ID 
            FOREIGN KEY (`subscription_app_id`) REFERENCES `subscription_application` (`subscription_app_id`) ON DELETE CASCADE ON UPDATE CASCADE
    ;
");

$this->query("
    ALTER TABLE `subscription_application`
        ADD `payment_method` VARCHAR(15) NULL DEFAULT NULL AFTER `app_id`
    ;
");
