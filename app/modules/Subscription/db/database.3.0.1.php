<?php

$subscription = new Subscription_Model_Subscription_Application();
$subscriptions = $subscription->findAll(array("is_paypal" => new Zend_Db_Expr("profile_id IS NOT NULL")));

$can_be_dropped = true;

foreach($subscriptions as $subscription) {

    $subscription->setPaymentMethod("paypal")
        ->save()
    ;

    foreach(array("profile_id", "correlation_id") as $paypal_key) {
        $detail = new Subscription_Model_Subscription_Application_Detail();
        $detail->setSubscriptionAppId($subscription->getId())
            ->setCode($paypal_key)
            ->setValue($subscription->getData($paypal_key))
            ->save()
        ;
    }
}

if(!$can_be_dropped) {
    throw new Exception("An error occurred while saving the payment methods. Please, try again later.");
}

$this->query("
    ALTER TABLE `subscription_application`
        DROP profile_id,
        DROP correlation_id
    ;
");