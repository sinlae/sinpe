<?php

$this->query("
    CREATE TABLE `subscription` (
        `subscription_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `name` varchar(100) NOT NULL,
        `description` TEXT NOT NULL,
        `setup_fee` decimal(12,4) NULL DEFAULT NULL,
        `payment_frequency` VARCHAR(10) NULL DEFAULT NULL,
        `regular_payment` decimal(12,4) NULL DEFAULT NULL,
        `is_active` TINYINT(1) NOT NULL DEFAULT 1,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`subscription_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `subscription_application` (
        `subscription_app_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `subscription_id` INT(11) UNSIGNED NOT NULL,
        `app_id` INT(11) UNSIGNED NOT NULL,
        `payment_method` VARCHAR(15) NULL DEFAULT NULL,
        `is_active` TINYINT(1) NOT NULL DEFAULT 0,
        `expire_at` datetime NULL DEFAULT NULL,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        INDEX `IDX_SUBSCRIPTION_ID` (`subscription_id`),
        UNIQUE `UNIQUE_APP_ID` (`app_id`),
        PRIMARY KEY (`subscription_app_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `subscription_application_detail` (
        `detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `subscription_app_id` int(11) unsigned NOT NULL,
        `code` varchar(50) NOT NULL,
        `value` varchar(255) NOT NULL,
        PRIMARY KEY (`detail_id`),
        INDEX IDX_SUBSCRIPTION_APP_ID (`subscription_app_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$this->query("
    CREATE TABLE `subscription_acl_resource` (
        `subscription_resource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `subscription_id` int(11) unsigned NOT NULL,
        `resource_id` int(11) unsigned NOT NULL,
        PRIMARY KEY(subscription_resource_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");


$this->query("
    ALTER TABLE `subscription_application`
        ADD CONSTRAINT `FK_SUBSCRIPTION_SUBSCRIPTION_ID` FOREIGN KEY (subscription_id) references `subscription` (`subscription_id`) ON UPDATE CASCADE ON DELETE CASCADE;
");

$this->query("
    ALTER TABLE `subscription_application`
        ADD CONSTRAINT `FK_APPLICATION_APP_ID` FOREIGN KEY (app_id) references `application` (`app_id`) ON UPDATE CASCADE ON DELETE CASCADE;
");

$this->query("
    ALTER TABLE `sales_order`
        ADD CONSTRAINT `FK_SALES_ORDER_SUBSCRIPTION_ID` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`subscription_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `sales_invoice`
      ADD CONSTRAINT `FK_SALES_INVOICE_SUBSCRIPTION_ID` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`subscription_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `subscription_application_detail`
        ADD CONSTRAINT FK_SUBSCRIPTION_APPLICATION_DETAIL_SUBSCRIPTION_APP_ID
            FOREIGN KEY (`subscription_app_id`) REFERENCES `subscription_application` (`subscription_app_id`) ON DELETE CASCADE ON UPDATE CASCADE
    ;
");
