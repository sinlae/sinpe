<?php
require_once '2checkout-php/lib/Twocheckout.php';
require_once('Stripe/init.php');
class Subscription_ApplicationController extends Application_Controller_Default
{

    protected $_order;

    public function createAction() {

        //$admin_can_publish = $this->getSession()->getAdmin()->canPublishThemself();

        //if($errors = $this->getApplication()->isAvailableForPublishing($admin_can_publish)) {
          //  $message = $this->_('In order to publish your application, we need:<br />-');
           // $message .= join('<br />- ', $errors);
           // $this->getSession()->addError($message);
           // $this->redirect("application/customization_publication_infos");
           // return $this;
        //} //else if($this->getApplication()->getSubscription()->isActive()) {
            //$this->getSession()->addWarning($this->_("You already have a subscription for this application"));
            //$this->redirect("application/customization_publication_infos");
            //return $this;
        //}

        if($order_id = $this->getSession()->order_id) {
            $order = new Sales_Model_Order();
            $order->find($order_id);
            $order->cancel();
            $this->getSession()->order_id = null;
        }

        $this->loadPartials();

    }

    public function createplanAction() {
         $this->loadPartials();
    }

    public function saveaddressAction() {

        $html = array();

        if($data = $this->getRequest()->getPost()) {

            try {

                $admin = $this->getSession()->getAdmin();

                if(empty($data["company"])) {
                    throw new Exception($this->_("Please, enter a company"));
                }
                if(empty($data["address"])) {
                    throw new Exception($this->_("Please, enter an address"));
                }
                if(empty($data["country_code"])) {
                    throw new Exception($this->_("Please, enter a country"));
                }
                if(!empty($data["vat_number"])) {

                    if(!$this->__checkVatNumber($data["vat_number"], $data["country_code"])) {
                        throw new Exception($this->_("Your VAT Number is not valid"));
                    }
                }
                $regions = Siberian_Locale::getRegions();
                if(!empty($data["region_code"])) {
                    if($regions[$data["country_code"]]) {
                        $region = $regions[$data["country_code"]][$data["region_code"]];
                        $region_code = $data["region_code"];
                    }else{
                        $region = null;
                        $region_code = null;
                    }
                }else{
                    $region = null;
                    $region_code = null;
                }
                $admin->setCompany($data["company"])
                    ->setFirstname($data["firstname"])
                    ->setLastname($data["lastname"])
                    ->setAddress($data["address"])
                    ->setAddress2($data["address2"])
                    ->setZipCode($data["zip_code"])
                    ->setCity($data["city"])
                    ->setRegionCode($region_code)
                    ->setRegion($region)
                    ->setCountryCode($data["country_code"])
                    ->setVatNumber($data["vat_number"])
                    ->setPhone($data["phone"])
                    ->save()
                ;

                $html = array("success" => 1);

                if($this->getSession()->subscription_id) {
                    $subscription = new Subscription_Model_Subscription();
                    $subscription->find($this->getSession()->subscription_id);

                    $order_details = $this->_getOrderDetailsHtml($subscription);

                    $html["order_details_html"] = $order_details;

                }

            } catch (Exception $e) {
                $html = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }

            $this->_sendHtml($html);
        }
    }

    public function setAction() {

        if($data = $this->getRequest()->getPost()) {

            try {

                if(empty($data["subscription_id"])) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $subscription = new Subscription_Model_Subscription();
                $subscription->find($data["subscription_id"]);

                $subscription_id = $data["subscription_id"];

                if(!isset($data["website_".$subscription_id])) {
                    $name = $subscription->getName();
                    $name = str_replace("Website", "", $name);
                    $subscriptionWithoutWebsite = new Subscription_Model_Subscription();
                    $subscriptionWithoutWebsite = $subscriptionWithoutWebsite->find(array("name"=>$name));
                    $subscription = $subscriptionWithoutWebsite;
                }

                if(!$subscription->getId()) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                

                $this->getSession()->subscription_id = $subscription->getId();

                if($this->getSession()->getAdmin()->getCustomerStripeId()){
                    $sub= new Subscription_Model_Subscription_Application();
                    $sub = $sub->find(array("app_id"=>$this->getApplication()->getAppId()));
                    if($sub->getSubscriptionId()){
                        $now = Zend_Date::now();
                        $date = new Siberian_Date();
                        $diff1 = $date->getDiff($now->get('y-MM-dd HH:mm:ss'),$sub->getExpireAt());
                        $diff2 = $date->getDiff($sub->getCreatedAt(), $now->get('y-MM-dd HH:mm:ss'));
                        $diff2 = $diff2-1;
                        $sub = new Subscription_Model_Subscription();
                        $proratePrice = $sub->getProratePrice($subscription->getId(), $this->getApplication()->getSubscription()->getSubscriptionId(),$diff1,$diff2);
                        $proratePrice = round($proratePrice,2);
                        $order_details = $this->_getOrderDetailsHtml($subscription,$proratePrice);
                        $html = array(
                        "success" => 1,
                        "order_details_html" => $order_details,
                        "has_to_be_paid" => $this->_subscriptionToOrder($subscription, $proratePrice)->hasToBePaid()
                    );
                    }
                }    else  {
                    $order_details = $this->_getOrderDetailsHtml($subscription);
                    $html = array(
                        "success" => 1,
                        "order_details_html" => $order_details,
                        "has_to_be_paid" => $this->_subscriptionToOrder($subscription)->hasToBePaid()
                    );
                }

            } catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($html);

        }

    }

    public function processAction() {

        if($subscription_id = $this->getSession()->subscription_id) {

            try {

                $subscription = new Subscription_Model_Subscription();
                $subscription->find($subscription_id);

                if (!$subscription->getId()) {
                    throw new Exception($this->_("An error occurred while saving. The selected subscription is not valid."));
                }

                $admin = $this->getAdmin();
                $subscription->setCurrentApplication($this->getApplication())
                    ->setAdmin($admin)
                ;
                $proratePrice = false;
                if($this->getSession()->getAdmin()->getCustomerStripeId()){
                    $sub= new Subscription_Model_Subscription_Application();
                    $sub = $sub->find(array("app_id"=>$this->getApplication()->getAppId()));
                    if($sub->getSubscriptionId()){
                        $now = Zend_Date::now();
                        $date = new Siberian_Date();
                        $diff1 = $date->getDiff($now->get('y-MM-dd HH:mm:ss'),$sub->getExpireAt());
                        $diff2 = $date->getDiff($sub->getCreatedAt(), $now->get('y-MM-dd HH:mm:ss'));
                        $sub = new Subscription_Model_Subscription();
                        $proratePrice = $sub->getProratePrice($subscription->getId(), $this->getApplication()->getSubscription()->getSubscriptionId(),$diff1,$diff2);
                        $proratePrice = round($proratePrice,2);
                    }
                } 
                if($proratePrice){
                    $order = $subscription->toOrder($proratePrice);
                }  else  {
                    $order = $subscription->toOrder();
                }
                $order->setStatus(Sales_Model_Order::DEFAULT_STATUS)
                    ->setAppId($this->getApplication()->getId())
                    ->setAppName($this->getApplication()->getName())
                    ->setSubscriptionId($subscription->getId())
                    ->setAdminId($admin->getId())
                    ->setAdminCompany($admin->getCompany())
                    ->setAdminName($admin->getFirstname() . " " . $admin->getLastname())
                    ->setAdminEmail($admin->getEmail())
                    ->setAdminAddress($admin->getAddress())
                    ->setAdminAddress2($admin->getAddress2())
                    ->setAdminCity($admin->getCity())
                    ->setAdminZipCode($admin->getZipCode())
                    ->setAdminRegion($admin->getRegion())
                    ->setAdminCountry($admin->getCountry())
                    ->setAdminPhone($admin->getPhone())
                    ->setPaymentMethod($this->getRequest()->getPost("payment_method"))
                    ->save()
                ;

                $this->getSession()->order_id = $order->getId();

                if($order->hasToBePaid()) {
                    $payment = new Payment_Model_Payment();
                    $params = array(
                        'payment_method' => $this->getRequest()->getPost("payment_method"),
                        'order' => $order
                    );
                    if($this->getRequest()->getPost("payment_method")=="stripe"){
                        $html = array("url" => $this->getUrl("subscription/application/stripe?subscription=".$subscription_id));
                    } else {
                        $html = $payment->getPaymentData($params);
                    }
                } else {
                    $html = array("url" => $this->getUrl("subscription/application/success"));
                }

            } catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($html);

        }

    }

    public function stripeAction() {
        $this->loadPartials();
    }

    public function successAction() {

        $admin = $this->getSession()->getAdmin();
        $upgrade = false;
        if(isset($_POST['stripeToken'])){
            $api_key = Api_Model_Key::findKeysFor('stripe');
            \Stripe\Stripe::setApiKey($api_key->getSecretKey()); 
            if($admin->getCustomerStripeId()){
                $old_subscription = new Subscription_Model_Subscription_Application();
                $old_subscription = $old_subscription->find(array("customer_id" => $admin->getCustomerStripeId()));

                if($old_subscription->getSubscriptionId()){
                    $subscription = \Stripe\Subscription::retrieve($old_subscription->getStripeId());
                    $subscription->plan = $_POST['subscription'];
                    $subscription->save();

                    $upgrade = \Stripe\Invoice::create(array(
                        "customer" => $admin->getCustomerStripeId()
                    ));

                    $invoice = \Stripe\Invoice::retrieve($upgrade->id);
                    $invoice->pay();
                }

                $customer = \Stripe\Customer::retrieve($admin->getCustomerStripeId());
                $admin->setCustomerStripeId($customer->id)->save();
                $upgrade = true;
            } else {
                $customer = \Stripe\Customer::create(array(
                    'email' => $_POST['stripeEmail'],
                    'source'  => $_POST['stripeToken'],
                    'plan' => $_POST['subscription']
                ));
                $admin->setCustomerStripeId($customer->id)->save();
            }
        }

        if($order_id = $this->getSession()->order_id) {
            $order = new Sales_Model_Order();
            $order->find($order_id);

            $payment = new Payment_Model_Payment();

            if($order->hasToBePaid()) {
                $params = array(
                    'payment_method' => $this->getRequest()->getParam("payment_method"),
                    'token' => $this->getRequest()->getParam('token'),
                    'key' => $this->getRequest()->getParam('key'),
                    'order_number' => $this->getRequest()->getParam('order_number')
                );

                $success = $payment->setData($params)->setOrder($order)->success();
            } else {
                $success = array();
            }

            if(!is_array($success)) {

                $this->getSession()->addWarning($this->_("An error occurred while processing the payment. For more information, please feel free to contact us."));
                $this->redirect("subscription/application/create");

            } else {
                    $order->pay();

                $old_subscription = new Subscription_Model_Subscription_Application();
                $old_subscription = $old_subscription->find(array("app_id" => $order->getAppId()));

                if($old_subscription)
                {    
                    $details = new Subscription_Model_Subscription_Application_Detail();
                    $details = $details->find(array("subscription_app_id" => $old_subscription->getSubscriptionAppId(), "code" => "order_number"));


                    Twocheckout::username('myappsin');
                    Twocheckout::password('Sinsc3n3s33n');
                    Twocheckout::sandbox(true); 
                    $args = array(
                        'sale_id' => $details->getValue()
                    );
                    try {
                        $result = Twocheckout_Sale::stop($args);
                    } catch (Twocheckout_Error $e) {
                        $e->getMessage();
                    }
                }

                if(isset($_POST['stripeEmail'])){
                    $success["payment_data"]["is_recurrent"] = 1;
                    $success["payment_data"]["order_number"] = $order->getNumber();
                    $stripe_sub = $customer->subscriptions->data[0]->id;
                    $subscription = new Subscription_Model_Subscription_Application();
                    if($upgrade == true){
                         $old_subscription->setSubscriptionId($order->getSubscriptionId())
                                    ->save();
                    } else {
                        $subscription->create($order->getAppId(), $order->getSubscriptionId())
                            ->setStripeId($stripe_sub)
                            ->setCustomerId($customer->id)
                            ->setPaymentData(isset($success["payment_data"]) ? $success["payment_data"] : null)
                            ->setPaymentMethod("stripe")
                            ->save(); 
                    } 
                } else {
                    $subscription = new Subscription_Model_Subscription_Application();
                    $subscription->create($order->getAppId(), $order->getSubscriptionId(), $upgrade)
                        ->setPaymentData(isset($success["payment_data"]) ? $success["payment_data"] : null)
                        ->setPaymentMethod($payment->getCode())
                        ->save();
                }



                if($order->getSubscriptionId() == 1 || $order->getSubscriptionId() == 2)
                {
                    $option = new Application_Model_Option();
                    $option = $option->find(array("code" => "padlock"));

                    $option_value = new Application_Model_Option_Value();
                    $option_value = $option_value->find(array("app_id"=>$order->getAppId(),"option_id" => $option->getOptionId()));

                    $padlock = new Padlock_Model_Padlock();
                    $padlock = $padlock->find(array("value_id" => $option_value->getValueId()));

                    $padlock->deleteFeature();
                }    

                $this->getSession()->subscription_id = null;
                $this->getSession()->order_id = null;

                if ($this->getApplication()->getIsLocked()) 
                {
                    $this->getApplication()->setIsLocked(false)->save();
                }

                $this->loadPartials();
            }
        }
    }

    public function cancelAction() {

        $payment = new Payment_Model_Payment();
        $params = array(
            'payment_method' => $this->getRequest()->getParam("payment_method")
        );
        $cancel = $payment->setData($params)->cancel();
        $this->redirect("subscription/application/create");

    }

    public function checknotificationsAction() {

        //$data = $this->getRequest()->getPost();
         if ($this->getRequest()->getParam("message")) {

            // $insMessage = array();
            // foreach ($data as $k => $v) {
             //    $insMessage[$k] = $v;
             //}

         // Validate the Hash
        //     $hashSecretWord = "sinsc3n3s33n"; // Input your secret word
        //     $hashSid = 901318971; //Input your seller ID (2Checkout account number)
        //     $hashOrder = $insMessage['sale_id'];
        //     $hashInvoice = $insMessage['invoice_id'];
        //     $StringToHash = strtoupper(md5($hashOrder . $hashSid . $hashInvoice . $hashSecretWord));

        //     if ($StringToHash != $insMessage['md5_hash']) {
        //         die('Hash Incorrect');
        //     }
            
             $app = new Application_Model_Application();
             $app = $app->find(array("app_id" => 2));
             $app->setName("testreussi3");
             $app->save();
        }
            
            //$app = new Application_Model_Application();
            //$app = $app->find(array("app_id" => 5));
            //$app->setName("testreussi2");
            //$app->save();
            $this->redirect("subscription/application/create");
    }

    protected function _getOrderDetailsHtml($subscription, $price=null) {

        $order = $this->_subscriptionToOrder($subscription,$price);
        return $this->getLayout()->addPartial("order_details", "admin_view_default", "subscription/application/create/order_details.phtml")
            ->setTmpOrder($order)
            ->toHtml()
        ;

    }

    private function __checkVatNumber($vatNumber, $countryCode) {

        // Serialize the VAT Number
        $vatNumber = str_replace(array(' ', '.', '-', ',', ', '), '', $vatNumber);
        // Retrieve the country code
        $countryCodeInVat = substr($vatNumber, 0, 2);
        // Retrieve the VAT Number
        $vatNumber = substr($vatNumber, 2);

        // Check the VAT Number syntax
        if (strlen($countryCode) != 2 || is_numeric(substr($countryCode, 0, 1)) || is_numeric(substr($countryCode, 1, 2))) {
            return false;
        }

        // Check if the country code in the VAT Number is the same than the parameter
        if ($countryCodeInVat != $countryCode) {
            return false;
        }

        // Call the webservice
        $client = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
        $params = array('countryCode' => $countryCode, 'vatNumber' => $vatNumber);
        $result = $client->checkVat($params);

        // Return the result
        return $result->valid;
    }

    protected function _subscriptionToOrder($subscription, $price) {

        if(!$this->_order) {
            $subscription->setCurrentApplication($this->getApplication())
                ->setAdmin($this->getSession()->getAdmin());

            $this->_order = $subscription->toOrder($price);
        }

        return $this->_order;
    }
}
