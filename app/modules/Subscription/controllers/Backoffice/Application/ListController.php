<?php

class Subscription_Backoffice_Application_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Subscriptions"),
            "icon" => "fa-list-ul",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $subscription = new Subscription_Model_Subscription_Application();
        $subscriptions = $subscription->getSubscriptionsList();
        $data = array();

        $key = 0;
        foreach($subscriptions as $subscription) {

            $data[$key] = $subscription->getData();
            $data[$key]['is_active'] = ($data[$key]['is_active'] == 1)?'Yes':'No';

            $date = new Zend_Date($data[$key]['expire_at'], Zend_Date::ISO_8601);
            $data[$key]['expire_at'] = $date->toString($this->_('MM/dd/yyyy'));

            $date = new Zend_Date($data[$key]['created_at'], Zend_Date::ISO_8601);
            $data[$key]['created_at'] = $date->toString($this->_('MM/dd/yyyy'));

            $key++;
        }

        $this->_sendHtml($data);
    }

}
