<?php

class Subscription_Backoffice_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Plans"),
            "icon" => "fa-credit-card",
            "delete_confirmation_message" => $this->_("Are you sure you want to delete this plan?"),
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $subscription = new Subscription_Model_Subscription();
        $subscriptions = $subscription->findAll();
        $data = array();

        foreach($subscriptions as $subscription) {

            if(!$subscription->isActive()) continue;

            if($subscription->getSetupFee() OR $subscription->getSetupFee() == 0) {
                $subscription->setSetupFee($subscription->getFormattedSetupFee());
            }

            if($subscription->getRegularPayment()) {
                $subscription->setRegularPayment($subscription->getFormattedRegularPayment());
            }
//            if($subscription->getDescription()) {
//                $subscription->setDescription(substr($subscription->getDescription(), 0, 30));
//            }
            $data[] = $subscription->getData();
        }

        $this->_sendHtml($data);

    }

    public function deleteAction() {


        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                if(empty($data["id"])) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $subscription = new Subscription_Model_Subscription();
                $subscription->find($data["id"]);

                if ($subscription->getId()) {
                    $subscription->setIsActive(0)
                        ->save()
                    ;
                }

                $data = array(
                    "success" => 1,
                    "id" => $data["id"]
                );

            } catch (Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
