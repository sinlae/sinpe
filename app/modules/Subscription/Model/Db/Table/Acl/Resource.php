<?php

class Subscription_Model_Db_Table_Acl_Resource extends Core_Model_Db_Table {

    protected $_name = "subscription_acl_resource";
    protected $_primary = "subscription_resource_id";

    public function findAllIdBySubscriptionId($subscription_id) {
        $select = $this->select()
            ->from(array('ars' => $this->_name),array("resource_id"))
            ->where('subscription_id = ?', $subscription_id);
        return $this->fetchAll($select);
    }

    public function findBySubscriptionId($subscription_id) {
        $select = $this->select()->setIntegrityCheck(false)
            ->from(array('ars' => $this->_name))
            ->joinLeft(array("ar" => "acl_resource"),"ars.resource_id = ar.resource_id")
            ->where('ars.subscription_id = ?', $subscription_id);
        return $this->fetchAll($select);
    }

    public function saveResources($subscription_id, $resources) {

        $this->beginTransaction();

        try {

            $this->_db->delete($this->_name, array('subscription_id = ?' => $subscription_id));

            foreach($resources as $resource) {
                if(!$resource["is_allowed"]) {
                    $data = array('resource_id' => $resource["id"], 'subscription_id' => $subscription_id);
                    $this->_db->insert($this->_name, $data);
                }
            }

            $this->commit();
            return true;
        } catch(Exception $e) {
            $this->rollback();
            return false;
        }
    }
}