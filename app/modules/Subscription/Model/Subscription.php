<?php

class Subscription_Model_Subscription extends Core_Model_Default {

    const MONTHLY_FRENQUENCY = "Monthly";
    const YEARLY_FRENQUENCY = "Yearly";

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Subscription';
        return $this;
    }

    public function isActive() {
        return $this->getData("is_active");
    }

    public function toOrder($price) {

        $order = new Sales_Model_Order();
        $admin = $this->getAdmin();
        $tax = new Tax_Model_Tax();
        $tax_rate = null;
        $countries = Siberian_Locale::getEuroZoneTerritoryDatas();
        $store_country = System_Model_Config::getValueFor("company_country");
        $admin_country = $admin->getCountryCode();

        // If the store is European, the client is European but doesn't have a VAT Number, he pays the store tax
        // if(array_key_exists($store_country, $countries) AND array_key_exists($admin_country, $countries)) {
        //     if(!$admin->getVatNumber()) {
        //         $tax->find($store_country, "country_code");
        //     }
        // } else {
            $tax->find($admin_country, "country_code");
        // }

        $tax_rate = $tax->getId() ? $tax->getRate() : 0;

        $order->setTaxRate($tax_rate);

        if($this->getSetupFee()) {
            $qty = 1;
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($this->_("Setup Fee"))
                ->setPriceExclTax($this->getSetupFee())
                ->setQty($qty)
                ->setTotalPriceExclTax($this->getSetupFee() * $qty)
            ;

            $order->addLine($line);
        }
        if($price){
            $price = $price;
        } else {
            $price = $this->getRegularPayment();
        }

        if($this->getRegularPayment()) {
            $qty = 1;
            $name = $this->_("%s Subscription for %s", $this->getPaymentFrequency(), $this->getCurrentApplication()->getName());
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($name)
                ->setPriceExclTax($price)
                ->setQty($qty)
                ->setTotalPriceExclTax($price * $qty)
                ->setIsRecurrent(1)
            ;

            $order->addLine($line);
        }

        $order->calcTotals();

        return $order;

    }

    public function getPriceHtml() {

        $html = array();
        $frequency = "%s";
        $has_setup_fees = is_numeric($this->getSetupFee());

        if ($has_setup_fees) {
            $html[] = $this->getFormattedSetupFee();
        }

        if ($this->getRegularPayment()) {

            if($has_setup_fees) {
                $html[] = "+";
            }

            $html[] = $this->getFormattedRegularPayment();

            if($this->getPaymentFrequency() == self::MONTHLY_FRENQUENCY) {
                $frequency = "%s / Month";
            } else if($this->getPaymentFrequency() == self::YEARLY_FRENQUENCY) {
                $frequency = "%s / Year";
            }
        }

        if(!$has_setup_fees AND ! $this->getRegularPayment()) {
            $frequency = "Free";
        }

        return $this->_($frequency, implode($has_setup_fees ? " " : "", $html));

    }

    public function getSetupFee() {
        $setup_fee = $this->getData("setup_fee");
        return $setup_fee > 0 ? $setup_fee : null;
    }

    public function getRegularPayment() {
        $regular_payment = $this->getData("regular_payment");
        return $regular_payment > 0 ? $regular_payment : null;
    }

    public function isFree() {
        return $this->getSetupFee() + $this->getRegularPayment() <= 0;
    }

    public function getResources()
    {
        $acl_subscription = new Subscription_Model_Acl_Resource();
        $acl_subscriptions = $acl_subscription->findAllIdBySubscriptionId($this->getId());
        $resources = array();
        foreach ($acl_subscriptions as $key => $acl) {
            $acl_resource = new Acl_Model_Resource();
            $acl_resource = $acl_resource->find(array("resource_id" => $acl->getResourceId()));
            $resources[$acl_resource->getResourceId()] = $acl_resource->getCode();
        }

        return $resources;
    }

    public function getProratePrice($subid1, $subid2, $diff1, $diff2){

        $subscription = $this->find(array("subscription_id"=>$subid1));
        $price = $subscription->getRegularPayment();
        $subscription2 = $this->find(array("subscription_id"=>$subid2));
        $price2 = $subscription2->getRegularPayment();

        $pricebysec = $price/31536000;
        $pricebysec2 = $price2/31536000;

        $proratePrice = ($pricebysec*$diff1)-($price2-($pricebysec2*$diff2));
        return $proratePrice;
    }

}
