<?php

class Subscription_Model_Acl_Resource extends Core_Model_Default {

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Acl_Resource';
        return $this;
    }

    public function findAllIdBySubscriptionId($subscription_id) {
        return $this->getTable()->findAllIdBySubscriptionId($subscription_id);
    }

    public function findBySubscriptionId($subscription_id) {
        return $this->getTable()->findBySubscriptionId($subscription_id);
    }

    public function saveResources($subscription_id, $resources) {
        return $this->getTable()->saveResources($subscription_id, $resources);
    }
}