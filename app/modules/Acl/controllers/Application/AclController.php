<?php 
class Acl_Application_AclController extends Application_Controller_Default
{
	public function findbysubscriptionAction() {
		$post = $_POST['subscription_id'];
		$sub_acl_resource = new Subscription_Model_Acl_Resource();
		$resources = $sub_acl_resource->findAllIdBySubscriptionId($post);
		$data = array();
		foreach ($resources as $key => $resource) {
			$acl_resource = new Acl_Model_Resource();
			$acl_resource = $acl_resource->find(array("resource_id" => $resource->getResourceId()));
			$data[] = $acl_resource->getOptionId();
		}
		$sub = new Subscription_Model_Subscription();
        $sub = $sub->find(array("subscription_id"=>$_POST['subscription_id']));
        $nb_subscribers = $sub->getNbSubscribers();
		$html = array('success' => 1, "resources" => $data, "nb_subscribers"=>$nb_subscribers, "subscription_name" => $sub->getName());
		$this->_sendHtml(json_encode($html));
	}

}
?>