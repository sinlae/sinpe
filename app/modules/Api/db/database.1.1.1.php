<?php

$keys = array(
    'username',
    'password',
    'signature'
);

$provider_name = "Paypal";
$provider_code = "paypal";
$provider = new Api_Model_Provider();
$provider->setData(array(
    'code' => $provider_code,
    'name' => $provider_name,
    'icon' => 'fa-paypal'
))->save();
foreach($keys as $key) {
    $data = array(
        'provider_id' => $provider->getId(),
        'key' => $key
    );
    $key = new Api_Model_Key();
    $key->setData($data)->save();
}
