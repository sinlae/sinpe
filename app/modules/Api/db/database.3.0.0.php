<?php

$twocheckout_api_keys = array(
    'sid',
    'secret',
    'username',
    'password'
);

$provider = new Api_Model_Provider();
$provider->find('2checkout', 'code');
if(!$provider->getId()) {
    $provider->setData(array(
        'code' => '2checkout',
        'name' => '2Checkout',
        'icon' => 'fa-credit-card'
    ))->save();
    foreach ($twocheckout_api_keys as $key) {
        $datas = array(
            'provider_id' => $provider->getId(),
            'key' => $key
        );
        $key = new Api_Model_Key();
        $key->setData($datas)->save();
    }
}
