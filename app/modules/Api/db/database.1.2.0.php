<?php

$soundcloud_api_keys = array(
    'client_id',
    'secret_id'
);

$provider = new Api_Model_Provider();
$provider->find('soundcloud', 'code');
if(!$provider->getId()) {
    $provider->setData(array(
        'code' => 'soundcloud',
        'name' => 'Soundcloud',
        'icon' => 'fa-soundcloud'
    ))->save();
    foreach ($soundcloud_api_keys as $key) {
        $datas = array(
            'provider_id' => $provider->getId(),
            'key' => $key
        );
        $key = new Api_Model_Key();
        $key->setData($datas)->save();
    }
}
