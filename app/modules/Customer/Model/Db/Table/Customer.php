<?php

class Customer_Model_Db_Table_Customer extends Core_Model_Db_Table
{
    protected $_name = "customer";
    protected $_primary = "customer_id";

    public function findSocialDatas($customer_id) {

        $social_datas = array();

        $select = $this->_db->select()
            ->from("customer_social", array("type", "social_id", "datas"))
            ->where("customer_id = ?", $customer_id)
        ;

        $datas = $this->_db->fetchAll($select);
        foreach($datas as $data) {
            $social_datas[$data["type"]] = array("social_id" => $data["social_id"], "datas" => $data["datas"]);
        }

        return $social_datas;
    }

    public function insertSocialDatas($customer_id, $datas) {

        $this->_db->beginTransaction();
        $table = 'customer_social';
        try {
//            $this->_db->delete($table, array('customer_id = ?' => $customer_id));
            foreach($datas as $data) {
                $data['customer_id'] = $customer_id;

                if($this->findBySocialId($data['social_id'], $data['type'])) {
                    $this->_db->update($table, array('datas' => $data['datas']), array('social_id = ?' => $data['social_id'], 'type = ? ' => $data['type']));
                }
                else {
                    $r = $this->_db->insert($table, $data);
                }
            }
            $this->_db->commit();
        }
        catch(Exception $e) {
            $this->_db->rollBack();
        }

        return $this;

    }

    public function findBySocialId($id, $type, $app_id) {

        $select = $this->_db->select()
            ->from($this->_name)
            ->join('customer_social', "customer_social.customer_id = {$this->_name}.customer_id", array())
            ->where('customer_social.social_id = ?', $id)
            ->where('customer_social.type = ?', $type)
            ->where('customer.app_id = ?', $app_id)
        ;

        return $this->_db->fetchRow($select);
    }

    public function findByCreatedAt($year, $month, $day, $app_id) { 
       $select = $this->_db->select()
                      ->from('customer')
                      ->where('customer.created_at > ?', $year."-".$month."-".$day." 00:00:00")
                      ->where('customer.created_at < ?', $year."-".$month."-".$day." 23:59:59")
                      ->where('customer.app_id = ?', $app_id);
        return $this->_db->fetchAll($select);
    }

    public function findByMonth($year, $month, $app_id) { 
        $month30 = array('04', '06', '09', '11');
        $month31 = array('01', '03', '05', '07', '08', '10', '12');
        if(in_array($month,$month30))
        {
            $select = $this->_db->select()
                          ->from('customer')
                          ->where('customer.created_at > ?', $year."-".$month."-"."01"." 00:00:00")
                          ->where('customer.created_at < ?', $year."-".$month."-"."30"." 23:59:59")
                          ->where('customer.app_id = ?', $app_id);
        }   elseif (in_array($month,$month31)) {
                    $select = $this->_db->select()
                      ->from('customer')
                      ->where('customer.created_at > ?', $year."-".$month."-"."01"." 00:00:00")
                      ->where('customer.created_at < ?', $year."-".$month."-"."31"." 23:59:59")
                      ->where('customer.app_id = ?', $app_id); 
        }   elseif ($month == "02") {
            $bisextil = $year/4;
            if(is_float($bisextil))
            {
                $select = $this->_db->select()
                          ->from('customer')
                          ->where('customer.created_at > ?', $year."-".$month."-"."01"." 00:00:00")
                          ->where('customer.created_at < ?', $year."-".$month."-"."28"." 23:59:59")
                          ->where('customer.app_id = ?', $app_id);  
            } else {
                $select = $this->_db->select()
                          ->from('customer')
                          ->where('customer.created_at > ?', $year."-".$month."-"."01"." 00:00:00")
                          ->where('customer.created_at < ?', $year."-".$month."-"."29"." 23:59:59")
                          ->where('customer.app_id = ?', $app_id);  
            }   
        }
        return $this->_db->fetchAll($select);
    }

    public function addSocialPost($customer_id, $customer_message, $message_type, $points) {

        $table = 'customer_social_post';

        $datas = array(
            'customer_id' => $customer_id,
            'customer_message' => $customer_message,
            'message_type' => $message_type
        );

        if(!empty($points)) {

            $datas['points'] = $points;

            $where = $this->_db->quoteInto('customer_id = ?', $customer_id);
            $current_points = $this->_db->fetchOne($this->_db->select()->from($table, array('points'))->where($where));

            if($current_points AND empty($customer_message)) {
                $datas['points'] += $current_points;
                $this->_db->update($table, array('points' => $datas['points']), array('customer_id = ?' => $customer_id));
            }
            else {
                $this->_db->insert($table, $datas);
            }

        }
        else {
            $this->_db->insert($table, $datas);
        }


        return $this;
    }

    public function deleteSocialPost($customer_id, $post_id) {
        $this->_db->delete('customer_social_post', array('customer_id = ?' => $customer_id, 'id = ?' => $post_id));
        return $this;
    }

    public function findAllPosts() {
        return $this->_db->fetchAll($this->_db->select()->from('customer_social_post'));
    }

}