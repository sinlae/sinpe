<?php
require_once 'Mailin/Mailin.php';
class Customer_ApplicationController extends Application_Controller_Default {

    public function listAction() {
        $this->loadPartials();
    }

    public function newAction() {
        $this->_forward("edit");
    }

    public function editAction() {

        $customer = new Customer_Model_Customer();
        if($customer_id = $this->getRequest()->getParam('customer_id')) {
            $customer->find($customer_id);
            if(!$customer->getId()) {
                $this->getRequest()->addError($this->_("This user does not exist."));
            }
        }

        $this->loadPartials();
        $this->getLayout()->getPartial('content')->setCurrentCustomer($customer);

    }

    public function saveAction() {

        if($data = $this->getRequest()->getPost()) {

            try {
                $customer = new Customer_Model_Customer();
                if(!empty($data['customer_id'])) {
                    $customer->find($data['customer_id']);
                    if(!$customer->getId() || $customer->getAppId() != $this->getApplication()->getId()) {
                        throw new Exception($this->_("An error occurred while saving. Please try again later."));
                    }
                }

                $isNew = !$customer->getId();
                $errors = array();

                if(empty($data['civility'])) $errors[] = $this->_("the gender");
                //if(empty($data['firstname'])) $errors[] = $this->_("the first name");
                //if(empty($data['lastname'])) $errors[] = $this->_("the last name");
                if(empty($data['email'])) $errors[] = $this->_("the email address");
                if($isNew AND empty($data['password'])) $errors[] = $this->_("the password");

                if(!empty($errors)) {
                    $message = array($this->_("Please fill in the following fields:"));
                    foreach($errors as $error) {
                        $message[] = $error;
                    }
                    $message = join('<br />- ', $message);
                    throw new Exception($message);
                }

                if(!empty($data['email']) AND !Zend_Validate::is($data['email'], 'emailAddress')) throw new Exception($this->_("Please enter a valid email address"));

                $data['show_in_social_gaming'] = (int) !empty($data['show_in_social_gaming']);
                $data['can_access_locked_features'] = (int) !empty($data['can_access_locked_features']);

                if($isNew) {
                    $data['app_id'] = $this->getApplication()->getId();
                }

                if(isset($data['password']) AND empty($data['password'])) {
                    unset($data['password']);
                }

                $customer->setData($data);
                if(!empty($data['password'])) {
                    $customer->setPassword($data['password']);
                }
                $customer->setPseudo($data['pseudo'], $this->getApplication());
                $customer->save();

                 
                $mailin = new Mailin('laellou@hotmail.fr','Es2PXOy7G4VntkfA');
                $mailin->addTo($data['email'], ' ')
                -> setFrom('laetitiaferdinand@sincube.com', 'sincube.com')
                -> setSubject('Entrer le sujet ici')
                ->setText('Bonjour')
                ->setHtml('<strong>Bonjour</strong>');
                $res = $mailin->send();

              /*  $config = array('ssl' => 'tls', 
                'port' => 587,
                'auth' => 'login',
                'username' => '6cbbf0bb58bb19f360de7cc6112b99df',
                'password' => '3c2a81da0de6a5b1a3e009a831101249');
 
                $transport = new Zend_Mail_Transport_Smtp('smtp.tipimail.com', $config);
                 
                $mail = new Zend_Mail();
                $mail->setBodyText('Bonjour');
                $mail->setBodyHtml('<strong>Bonjour</strong>');
                $mail->setFrom('laellou2@hotmail.fr', "Sincube l'app");
                $mail->addTo($customer->getEmail(), $customer->getPseudo());
                $mail->setSubject('Sujet de test');
                $mail->send($transport);*/

                  
                $this->getSession()->addSuccess($this->_("Info successfully saved"));
                $html = array("success" => 1);
            }
            catch(Exception $e) {
                $html = array(
                    "error" => 1,
                    "message" => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->getResponse()->setBody(Zend_Json::encode($html))->sendResponse();
            die;

        }

    }

    public function deleteAction() {

        if($customer_id = $this->getRequest()->getPost('customer_id')) {

            try {

                $customer = new Customer_Model_Customer();
                $customer->find($customer_id);
                if(!$customer->getId() || $customer->getAppId() != $this->getApplication()->getId()) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $customer_id = $customer->getId();
                $customer->delete();

                $html = array("success" => 1, "customer_id" => $customer_id);

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            //$this->getResponse()->setBody(Zend_Json::encode($html))->sendResponse();
            //die;
             $this->_sendHtml($html);

        }

    }

    public function getcustomerslistAction() {
        if($app_id = $this->getRequest()->getParam('app_id')) {
            $i = 0; 
            $application = new Application_Model_Application();
            $application = $application->find(array("app_id"=>$app_id));
            $customers = $application->getCustomers();
            $customers_array = array();
            foreach ($customers as $key => $customer) {
                if($customer->getCustomerId()){
                    $customers_array[$i]["mail"] = $customer->getEmail();
                    $customers_array[$i]["pseudo"] = $customer->getPseudo();
                    $i++;
                }
            }
            $this->_sendHtml(array("success"=>1,"customers"=>$customers_array));
        }

    }

    public function getcustomerslistoffsetAction() {
         if($offset = $this->getRequest()->getParam('offset')) {
            $offset = $offset - 1 ;
            $i = 0; 
            $application = $this->getApplication();
            $customers = $application->getCustomersOffset($offset);
            $customers_array = array();
            foreach ($customers as $key => $customer) {
                if($customer->getCustomerId()){
                    $customers_array[$i]["id"] = $customer->getCustomerId();
                    $customers_array[$i]["mail"] = $customer->getEmail();
                    $customers_array[$i]["pseudo"] = $customer->getPseudo();
                    $customers_array[$i]["name"] = $customer->getFirstname()." ".$customer->getLastname();
                    $customers_array[$i]["created_at"] = $customer->getCreatedAt();
                    $i++;
                }
            }
            $this->_sendHtml(array("success"=>1,"customers"=>$customers_array));
         }

    }
}


