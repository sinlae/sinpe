<?php
class Customer_Mobile_Account_EditController extends Application_Controller_Mobile_Default {

    public function findAction() {

        $customer = $this->getSession()->getCustomer();
        $data = array();
        if($customer->getId()) {
            $data = array(
                "id" => $customer->getId(),
                "civility" => $customer->getCivility(),
                "firstname" => $customer->getFirstname(),
                "lastname" => $customer->getLastname(),
                "email" => $customer->getEmail(),
                "picture" => $customer->getImage(),
                "pseudo" => $customer->getPseudo(),
                "show_in_social_gaming" => (bool) $customer->getShowInSocialGaming()
            );

        }

        echo $this->_sendHtml($data);

    }

    public function postAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {
            $customer = $this->getSession()->getCustomer();

            try {

                $clearCache = false;
                if($this->getSession()->getCustomer()->getAdminId())
                {    
                    throw new Exception($this->_("You are an admin, change have to be done on PC"));
                }

                if(!$customer->getId()) {
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                if(!Zend_Validate::is($data['email'], 'EmailAddress')) {
                    throw new Exception($this->_('Please enter a valid email address'));
                }

                $dummy = new Customer_Model_Customer();
                $dummy->find(array('email' => $data['email'], "app_id" => $this->getApplication()->getId()));

                if($dummy->getId() AND $dummy->getId() != $customer->getId()) {
                    throw new Exception($this->_('We are sorry but this address is already used.'));
                }

                if(empty($data['show_in_social_gaming'])) $data['show_in_social_gaming'] = 0;

                if($data['show_in_social_gaming'] != $customer->getShowInSocialGaming()) $clearCache = true;

                if(isset($data['id'])) unset($data['id']);
                if(isset($data['customer_id'])) unset($data['customer_id']);

                $password = "";
                if(!empty($data['password'])) {

                    if(empty($data['old_password']) OR (!empty($data['old_password']) AND !$customer->isSamePassword($data['old_password']))) {
                        throw new Exception($this->_("The old password does not match the entered password."));
                    }

                    $password = $data['password'];
                }

                $image = $data['image'];
                if ($image) {
                    $uploader = new Core_Model_Lib_Uploader();
                    $relativePath = "/tmp/";
                    $fullPath = Application_Model_Application::getBaseImagePath().$relativePath;
                    //throw new Exception($fullPath);
                    $file =  $uploader->saveImage($image, $fullPath);
                    $params = array();
                    $image_sizes = getimagesize($file);
                    $src_width = $image_sizes[0];
                    $src_height = $image_sizes[1];
                    $params['source_width'] = $src_width;
                    $params['source_height'] = $src_height;
                    $params['dest_folder'] = $fullPath;
                    $params['crop_width'] = $src_width;
                    $params['crop_height'] = $src_height;
                    $params['output_width'] = $src_width;
                    $params['output_height'] = $src_width;
                    $params['w'] = $src_width;
                    $params['h'] = $src_width;
                    $params['quality']=90;

                    $x1 = ($params["source_width"] / 2) - ($params["output_width"] / 2);
                    $y1 = ($params["source_height"] / 2) - ($params["output_height"] / 2);

                    $params['x1'] = $x1;
                    $params['y1'] = $y1;

                    $params['file-tmp'] = $file; 
                    $file_saved = $uploader->savecrop($params);
                    $file2 = $uploader->resizeImage($fullPath.$file_saved, 160, 160, $fullPath);
                    $url = $this->_saveImageContent($fullPath.'/'.$file2, $data['pseudo']);
                    unlink($file);
                    unlink($fullPath.'/'.$file_saved);
                    unlink($fullPath.'/'.$file2);
                    $data['image'] = $url;
                }

                $customer->setData($data);
                if(!empty($password)) $customer->setPassword($password);
                $pseudo = $data['pseudo'];
                $customer->setPseudo($data['pseudo'], $this->getApplication());

                $customer->save();

                $html = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved"),
                    "clearCache" => $clearCache
                );

            }
            catch(Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($html);

        }

    }


      // Reference parameter
    protected function _saveImageContent($image, $firstname) {
        $relativePath = "/tmp";
        $fullPath = Application_Model_Application::getBaseImagePath() . $relativePath;
        $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
        $config=parse_ini_file($ini);

        $awsAccessKey = $config["awsAccessKey"];
        $awsSecretKey = $config["awsSecretKey"];
         
        //create s3 client
        $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

         $uploader = new Core_Model_Lib_Uploader();
         $id_filename = $this->getApplication()->getAppId()."/account-".$firstname;
         $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client, false);
         return $url;
    }

}
