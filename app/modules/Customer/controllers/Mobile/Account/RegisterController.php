<?php
require_once 'Mailin/Mailin.php';
class Customer_Mobile_Account_RegisterController extends Application_Controller_Mobile_Default
{

    public function postAction() {
        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {


            $customer = new Customer_Model_Customer();

            try {

                if($this->getApplication()->getSubscription()) {

                    $subrcribtion = new Subscription_Model_Subscription();
                    $subrcribtion = $subrcribtion->find(array("subscription_id" => $this->getApplication()->getSubscription()->getSubscriptionId()));

                    $all_customers = new Customer_Model_Customer();
                    $all_customers = $all_customers->findAll(array("app_id" => $this->getApplication()->getAppId()));

                    $count_all_customers = count($all_customers);
            
                    if(($this->getApplication()->getNoLimitUser()==0) && ($count_all_customers > $subrcribtion->getNbSubscribers())) {
                        throw new Exception($this->_("The number of subrcribers on this application is already reached"));
                    }
                }

                if(!Zend_Validate::is($data['email'], 'EmailAddress')) {
                    throw new Exception($this->_("Please enter a valid email"));
                }

                $bannedDomains = array('@yopmail.com', '@yopmail.fr', '@brefmail.com');
 
                foreach($bannedDomains as $domain) {
                    if(strrpos($data['email'], $domain) == true) 
                    {
                        throw new Exception($this->_('This email is invalid'));
                    }
                }

                if(strrpos($data['email'], "gmail.com") == true)
                {
                    if(strrpos($data['email'], "+"))
                    {
                        throw new Exception($this->_('This email is invalid'));
                    } 
                }

                $dummy = new Customer_Model_Customer();
                $dummy->find(array('email' => $data['email'], "app_id" => $this->getApplication()->getId()));

                if($dummy->getId()) {
                    throw new Exception($this->_('We are sorry but this address is already used.'));
                }

                if(empty($data['show_in_social_gaming'])) {
                    $data['show_in_social_gaming'] = 0;
                }

                if(empty($data['password'])) {
                    throw new Exception($this->_('Please enter a password'));
                }

                $image = $data['image'];
                if ($image) {
                    $uploader = new Core_Model_Lib_Uploader();
                    $relativePath = "/tmp/";
                    $fullPath = Application_Model_Application::getBaseImagePath().$relativePath;
                    $file =  $uploader->saveImage($image, $fullPath);
                    $params = array();
                    $image_sizes = getimagesize($file);
                    $src_width = $image_sizes[0];
                    $src_height = $image_sizes[1];
                    $params['source_width'] = $src_width;
                    $params['source_height'] = $src_height;
                    $params['dest_folder'] = $fullPath;
                    $params['crop_width'] = $src_width;
                    $params['crop_height'] = $src_height;
                    $params['output_width'] = $src_width;
                    $params['output_height'] = $src_width;
                    $params['w'] = $src_width;
                    $params['h'] = $src_width;
                    $params['quality']=90;

                    $x1 = ($params["source_width"] / 2) - ($params["output_width"] / 2);
                    $y1 = ($params["source_height"] / 2) - ($params["output_height"] / 2);

                    $params['x1'] = $x1;
                    $params['y1'] = $y1;

                    $params['file-tmp'] = $file; 
                    $file_saved = $uploader->savecrop($params);
                    $file2 = $uploader->resizeImage($fullPath.$file_saved, 160, 160, $fullPath);
                    $url = $this->_saveImageContent($fullPath.'/'.$file2, $data['pseudo']);
                    unlink($file);
                    unlink($fullPath.'/'.$file_saved);
                    unlink($fullPath.'/'.$file2);
                    $data['image'] = $url;
                }

/*                $url = 'https://api.amplitude.com/httpapi';

                $event = '[{"user_id":"laetitiaferdinand@gmail.com", "event_type":"register", "user_properties":{"Cohort":"Test A"}, "country":"United States", "ip":"127.0.0.1", "time":1458831357 }]';
                $fields = array(
                    'api_key' => "111532fbbd81d3903aa0887322ce7823",
                    'event' => $event,
                    'time' => "1458831357"
                );

                //open connection
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($c, CURLOPT_HEADER, false);
                curl_setopt($c, CURLOPT_POST,true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);*/

                $customer->setData($data)
                    ->setAppId($this->getApplication()->getId())
                    ->setPassword($data['password'])
                    ->setPseudo($data['pseudo'], $this->getApplication())
                    ->save()
                ;

                $this->getSession()->setCustomer($customer);

                $this->_sendNewAccountEmail($customer, $data['password']);

                $html = array(
                    'success' => 1,
                    'customer_id' => $customer->getId(),
                    'can_access_locked_features' => $customer->canAccessLockedFeatures()
                );

            }
            catch(Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($html);

        }

    }

      // Reference parameter
    protected function _saveImageContent($image, $firstname) {
        $relativePath = "/tmp";
        $fullPath = Application_Model_Application::getBaseImagePath() . $relativePath;
        $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
        $config=parse_ini_file($ini);

        $awsAccessKey = $config["awsAccessKey"];
        $awsSecretKey = $config["awsSecretKey"];
         
         //create s3 client
         $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

         $uploader = new Core_Model_Lib_Uploader();
         $id_filename = $this->getApplication()->getAppId()."/account-".$firstname;
         $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client, false);
         return $url;
    }

    protected function _sendNewAccountEmail($customer, $password) {

        $admin_email = null;
        $contact = new Contact_Model_Contact();
        $contact_page = $this->getApplication()->getPage('contact');
        $sender = 'no-reply@'.Core_Model_Lib_String::format($this->getApplication()->getName(), true).'.com';

        if($contact_page->getId()) {
            $contact->find($contact_page->getId(), 'value_id');
            $admin_email = $contact->getEmail();
        }

        $layout = $this->getLayout()->loadEmail('customer', 'create_account');
        $layout->getPartial('content_email')->setCustomer($customer)->setPassword($password)->setAdminEmail($admin_email)->setApp($this->getApplication()->getName());
        $content = $layout->render();

        $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
        $config=parse_ini_file($ini);

        $mailin = new Mailin("https://api.sendinblue.com/v2.0",$config['sendinblue_key']);
        $data = array( "to" => array($customer->getEmail()=>$customer->getName()),
        "from" => array("no-reply@sincube.com",$this->getApplication()->getName()),
        "subject" => $this->_('%s - Account creation', $this->getApplication()->getName()),
        "html" => $content
        );
        $res = $mailin->send_email($data);    
    }

}
