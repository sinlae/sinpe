<?php

class Contact_ApplicationController extends Application_Controller_Default
{

    public function editpostAction() {

        if($data = $this->getRequest()->getPost()) {

            try {
                $application = $this->getApplication();

                // Test s'il y a un value_id
                if(empty($data['value_id'])) throw new Exception($this->_('An error occurred while saving. Please try again later.'));

                // Récupère l'option_value en cours
                $option_value = new Application_Model_Option_Value();
                $option_value->find($data['value_id']);

                $html = '';
                $contact = new Contact_Model_Contact();
                $contact->find($option_value->getId(), 'value_id');

                if(!empty($data['file'])) {

                    $file = pathinfo($data['file']);
                    $filename = $file['basename'];
                    $relative_path = $option_value->getImagePathTo();
                    $folder = Application_Model_Application::getBaseImagePath().$relative_path;
                    $img_dst = $folder.'/'.$filename;
                    $img_src = Core_Model_Directory::getTmpDirectory(true).'/'.$filename;

                    if(!is_dir($folder)) {
                        mkdir($folder, 0777, true);
                    }

                    if(!@copy($img_src, $img_dst)) {
                        throw new exception($this->_('An error occurred while saving your picture. Please try again later.'));
                    } else {
                        $url = $this->_saveImageContent($img_dst, $option_value, $img_src); 
                        $data['cover'] = $url;
                    }
                }
                else if(!empty($data['remove_cover'])) {
                    $data['cover'] = null;
                }

                $contact->setData($data);

                if($contact->getStreet() AND $contact->getPostcode() AND $contact->getCity()) {
                    $latlon = Siberian_Google_Geocoding::getLatLng(array(
                        "street" => $contact->getStreet(),
                        "postcode" => $contact->getPostcode(),
                        "city" => $contact->getCity()
                    ));

                    if(!empty($latlon[0]) && !empty($latlon[1])) {
                        $contact->setLatitude($latlon[0])
                            ->setLongitude($latlon[1])
                        ;
                    }
                } else {
                    $contact->setLatitude(null)
                        ->setLongitude(null)
                    ;
                }

                $contact->save();

                $html = array(
                    'success' => '1',
                    'success_message' => $this->_('Info successfully saved'),
                    'message_timeout' => 2,
                    'message_button' => 0,
                    'message_loader' => 0
                );

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($html));

        }

    }

    protected function _saveImageContent($image, $option_value, $img_src) {
          $relativePath = $this->getCurrentOptionValue()->getImagePathTo();
          $fullPath = Application_Model_Application::getBaseImagePath().$relativePath;
          $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
          $config=parse_ini_file($ini);
          $id_filename = $this->getApplication()->getAppId()."/contact";
          $awsAccessKey = $config["awsAccessKey"];
          $awsSecretKey = $config["awsSecretKey"];

           //create s3 client
          $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);
           
           $uploader = new Core_Model_Lib_Uploader();
           $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client, false);
           unlink($img_src);  
           unlink($image);
           return $url;
    }

    public function cropAction() {

        if($datas = $this->getRequest()->getPost()) {
            try {
                $uploader = new Core_Model_Lib_Uploader();
                $file = $uploader->savecrop($datas);
                $datas = array(
                    'success' => 1,
                    'file' => $file,
                    'message_success' => $this->_('Info successfully saved'),
                    'message_button' => 0,
                    'message_timeout' => 2,
                );
            } catch (Exception $e) {
                $datas = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }
            $this->getLayout()->setHtml(Zend_Json::encode($datas));
         }

    }

}