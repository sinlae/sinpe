<?php

class Social_FacebookController extends Core_Controller_Default
{
    public function getlastfacebookidAction(){
        $app_id     = Core_Model_Lib_Facebook::getAppId();
        $app_secret = Core_Model_Lib_Facebook::getSecretKey();
        $url = 'https://graph.facebook.com/v2.6/oauth/access_token';
        $url .= '?grant_type=client_credentials';
        $url .= "&client_id=$app_id";
        $url .= "&client_secret=$app_secret";

        $access_token = json_decode(file_get_contents($url),true);
        $access_token = $access_token['access_token'];

        $token = $access_token;
        $facebook_models = new Social_Model_Facebook();
        $facebook_models =  $facebook_models->findAll();
        foreach ($facebook_models as $facebook_key => $facebook_model) {
        $page_id = $facebook_model->getFbUser();
        $datas = file_get_contents("https://graph.facebook.com/v2.6/".$page_id."/posts?limit=1&access_token=".$token,true);
        $datas = json_decode($datas,true);
        $messages = array();
        $i=0;
        foreach ($datas['data'] as $key => $data) {
               //$messages[$i]["message"] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", $data["message"]), ENT_NOQUOTES, 'UTF-8');
               $messages[$i]["id"] = $data["id"];
               $messages[$i]['postid'] = explode('_',$data["id"])[1];
               echo $messages[$i]['postid']."/";
               if($facebook_model->getLastPostId() != $messages[$i]['postid']){
                    $value_id = $facebook_model->getValueId();
                    $option_value = new Application_Model_Option_Value();
                    $option_value = $option_value->find(array('value_id' => $value_id));
                    $application = new Application_Model_Application();
                    $application = $application->find(array('app_id'=>$option_value->getAppId()));
                    $device = new Application_Model_Device();
                    $ios = $device->find(array("app_id"=>$application->getAppId(), "type_id"=>1));
                    $android = $device->find(array("app_id"=>$application->getAppId(), "type_id"=>2));
                    $facebook_model->setLastPostId($messages[$i]['postid'])->save();
                    if(($ios->getStatusId()==3) && ($android->getStatusId()==3)){
                        $message = new Push_Model_Message();
                        $message->setTypeId(1);
                        $datas['title'] = $application->getName();
                        $datas['send_at'] = Zend_Date::now()->toString('y-MM-dd HH:mm:ss');
                        $datas['type_id'] = 1;
                        $datas['app_id'] = $application->getAppId();
                        $datas["send_to_all"] = 1;
                        $datas['action_value'] = $facebook_model->getValueId();
                        $datas["text"] = "Nouveau post social sur l'application ".$application->getName();
                        $message->setData($datas)->save();
                    
                    //     $auto = 'Authorization: Basic '.$application->getOnesignalAppKey();
                    //     $content = array(
                    //          "en" => "Nouveau post sur l'application ".$application->getName()
                    //     );
                    //     $fields = array(
                    //          'app_id' => $application->getOnesignalAppId(),
                    //          'included_segments' => array('All'),
                    //          'data' => array("foo" => "bar"),
                    //          "ios_badgeType"=>"Increase", 
                    //          "ios_badgeCount"=>1,
                    //          'contents' => $content
                    //      ); 
        
                    // $fields = json_encode($fields);
                    // print("\nJSON sent:\n");
                    // print($fields);
        
                    // $ch = curl_init();
                    // curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                    //                                            $auto));
                    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    // curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    // curl_setopt($ch, CURLOPT_POST, TRUE);
                    // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                    // $response = curl_exec($ch);
                    // var_dump($response);
                    // curl_close($ch);   

                    }
               }
               $i++;
            }
        }

        die('done');

    }
}