<?php

class Social_Mobile_Facebook_ListController extends Application_Controller_Mobile_Default {

    public function findAction() {

        if($value_id = $this->getRequest()->getParam("value_id")) {

            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

            if(in_array($value_id, $lock_values_id))
            {
               $locked = 1;
            } else {
               $locked = 2;
            }

            $facebook = $this->getCurrentOptionValue()->getObject();
            $data = array(
                "username" => $facebook->getFbUser(),
                "token" => $facebook->getAccessToken(),
                "page_title" => $this->getCurrentOptionValue()->getTabbarName(),
                "locked" => $locked
            );
            $this->_sendHtml($data);

        }

    }

    public function findallpostsAction(){
        if($page_id = $this->getRequest()->getParam("page_id")) {
        $app_id     = Core_Model_Lib_Facebook::getAppId();
        $app_secret = Core_Model_Lib_Facebook::getSecretKey();
        $url = 'https://graph.facebook.com/v2.6/oauth/access_token';
        $url .= '?grant_type=client_credentials';
        $url .= "&client_id=$app_id";
        $url .= "&client_secret=$app_secret";

        $access_token = json_decode(file_get_contents($url),true);
        $access_token = $access_token['access_token'];

        $token = $access_token;
        $datas = file_get_contents("https://graph.facebook.com/v2.6/".$page_id."/posts?fields=attachments,message,created_time,likes.limit(1).summary(true),comments.limit(1).summary(true)&access_token=".$token,true);
        $datas = json_decode($datas,true);
        $cover = file_get_contents("https://graph.facebook.com/v2.6/".$page_id."/?fields=cover&access_token=".$token);
        $cover = json_decode($cover,true);
        $cover = $cover['cover']['source'];
        $messages = array();
        $i=0;
        foreach ($datas['data'] as $key => $data) {
               $messages[$i]["message"] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", $data["message"]), ENT_NOQUOTES, 'UTF-8');
               $created_at =  new Zend_Date($data["created_time"], Zend_Date::ISO_8601, new Zend_Locale('en_US'));
               $messages[$i]["created_at"] = $created_at->toString($this->_("MM/dd/y hh:mm a"));
               $messages[$i]["id"] = $data["id"];
               $messages[$i]["author"] = $this->getApplication()->getName();
               $messages[$i]["icon"] = $this->getApplication()->getIcon(74);
               $messages[$i]["picture"] = $data['attachments']['data'][0]['media']['image']['src'];
               // $likes= file_get_contents("https://graph.facebook.com/v2.6/".$messages[$i]["id"]."/likes?summary=true&access_token=".$token);
               // $likes = json_decode($likes,true); 
               // $count_comments = file_get_contents("https://graph.facebook.com/v2.6/".$messages[$i]["id"]."/comments?summary=true&access_token=".$token);
               // $count_comments = json_decode($count_comments,true); 
               $messages[$i]["meta"]["area1"]["text"] =  $created_at->toString($this->_("MM/dd/y hh:mm a"));
               $messages[$i]["meta"]["area3"]["text"] =  ($data['likes']['summary']['total_count']) < 25 ? $data['likes']['summary']['total_count']: ">25";
               $messages[$i]["meta"]["area2"]["text"] =  ($data['comments']['summary']['total_count']) < 25 ? $data['comments']['summary']['total_count'] : ">25";
               $messages[$i]["facebook"] = true;
               $messages[$i]['from']['id'] = $page_id;
               $messages[$i]['postid'] = explode('_',$data["id"])[1];

               $i++;
            }
            $data = array(
                "collection" => $messages,
                "cover" => $cover
            );
            $this->_sendHtml($data);

        }

    }
}