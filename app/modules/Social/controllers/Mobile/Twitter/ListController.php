<?php

class Social_Mobile_Twitter_ListController extends Application_Controller_Mobile_Default {

    public function findAction() {

        if($value_id = $this->getRequest()->getParam("value_id")) {

            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

            if(in_array($value_id, $lock_values_id))
            {
               $locked = 1;
               $data['details']['locked'] = 1;
            } else {
                $locked = 2;
                $data['details']['locked'] = 2;
            }

            $twitter = $this->getCurrentOptionValue()->getObject();


    		$token = new Zend_Oauth_Token_Access();

		    $token->setParams(array(
		        'oauth_token'        => '398062316-pxZNlybV8Cjdha2smnl5dqSJtuDyOQnhuPdw4iE4',
		        'oauth_token_secret' => 'roCud5tg3tWsXtOQaFDnPQqhuQJ96SPmiHcrrb15mUbZc',
		    ));

			$twitter_service = new Zend_Service_Twitter(array(
			    'username'       => 'sincub3',
			    'oauthOptions' => array(
			    	'consumerKey'    => 'Fgvbzrmt9Np9Lm5DIMZN6CSOt',
			    	'consumerSecret' => 'USYJ7puijXpH3QUBNMG1KdarBQKSgjGbuFmN4hXO1r0uUBpvKl'
			    ),
			    'accessToken'    => $token
			));

			$tweets = $twitter_service->statusesUserTimeline(array("user_id"=>$twitter->getTwitterUser(), "count"=>20, "exclude_replies"=>1));
			$tweets = json_decode($tweets->getRawResponse(), true);
			$data = array();
			//$data['collection'] = $tweets;
			$application = $this->getApplication();
			foreach($tweets as $key=>$tweet) {
				$tweetvid = null;
				$picture = null;
				$youtube = null;
				$imgyoutube = null;
				$image = null;
				if(isset($tweet["extended_entities"]["media"][0]["video_info"]["variants"])) {
					foreach ($tweet["extended_entities"]["media"][0]["video_info"]["variants"] as $key => $video) {
						if($video["content_type"] == "video/mp4") {
							$tweetvid = $video["url"];
							$picture = $tweet["extended_entities"]["media"][0]['media_url'];
						}
					}

				}
				$url = $tweet["entities"]["media"][0]["url"];
				if(!$url){
					$url = $tweet["entities"]["urls"][0]["url"];
				}
                if(!(strpos($tweet["entities"]["urls"][0]["display_url"], ".be")===false)){
					$youtube_vid = explode('?', $tweet["entities"]["urls"][0]["display_url"]);
					if($youtube_vid) $youtube = $youtube_vid[0];
					$imgyoutube = explode(".be/", $youtube);
					$imgyoutube = "http://img.youtube.com/vi/".$imgyoutube[1]."/0.jpg";
				}	

				if(isset($tweet["entities"]['media'][0]['media_url'])) {
					$image = $tweet["entities"]['media'][0]['media_url']; 
				}
				$retweet_count = $tweet["retweet_count"];
				$favorite_count = $tweet["favorite_count"];
				if($tweet["favorite_count"] > 999) {
					$favorite_count = ceil($tweet["favorite_count"]/1000);
					$favorite_count = $favorite_count."k";
				}
				if($tweet["retweet_count"] > 999) {
					$retweet_count = ceil($tweet["retweet_count"]/1000);
					$retweet_count = $retweet_count."k";
				}

				$text = str_replace($url, '', $tweet["text"]);
				$date = explode("+",$tweet["created_at"]);
			 	$data['collection'][] = array(
                        "message" => strip_tags(html_entity_decode(strip_tags($text), ENT_NOQUOTES, "UTF-8")),
                        "author" => $application->getName(),
                        "icon" => $tweets[0]["user"]["profile_image_url"],
                        "meta" => array(
				 	        "area1" => array("text" => $date[0])
				 	    ),
				 	    "source" => $tweetvid,
				 	    "picture" => $picture,
				 	    "youtube" => array(
				 	    	"link" => $youtube,
				 	    	"img" => $imgyoutube
				 	    ),
				 	    "image" => $image,
				 	    "like" => $favorite_count,
				 	    "retweet" => $retweet_count,
				 	    "tweet_url" => $url,
				 	    "social_sharing_active" => 1
				);
			}
   //                      "meta" => array(
   //                          "area1" => array(
   //                              "picto" => 
   //                              "text" => $this->_durationSince($tweet["created_at"])
   //                          )
   //                      ),
   //                      "social_sharing_active" => $this->getCurrentOptionValue()->getSocialSharingIsActive()
   //                  );

			// }
			$followers_count = $tweets[0]["user"]["followers_count"];
			$friends_count = $tweets[0]["user"]["friends_count"];
			$tweets_count = $tweets[0]["user"]["statuses_count"];
			if($tweets[0]["user"]["followers_count"] > 9999) {
				$followers_count = ceil($tweets[0]["user"]["followers_count"]/1000);
				$followers_count = $followers_count."K";
			}
			if($tweets[0]["user"]["friends_count"] > 9999) {
				$friends_count = ceil($tweets[0]["user"]["friends_count"]/1000);
				$friends_count = $friends_count."K";
			}
			if($tweets[0]["user"]["statuses_count"] > 9999) {
				$tweets_count = ceil($tweets[0]["user"]["statuses_count"]/1000);
				$tweets_count = $tweets_count."K";
			}
			$data["infos"] = array(
				"twitter_name" => $tweets[0]["user"]["screen_name"],
                "followers" => $followers_count,
                "following" => $friends_count,
                "tweets" => $tweets_count,
                "icon" => $tweets[0]["user"]["profile_image_url"]
			);

            $html = array(
                "username" => $twitter->getTwitterUser(),
                "page_title" => $this->getCurrentOptionValue()->getTabbarName(),
                "locked" => $locked,
                "collection" => $data["collection"],
                "details" => $data["details"],
                "cover_image_url" => $tweets[0]["user"]["profile_banner_url"],
                "infos" => $data["infos"]
            );

            $this->_sendHtml($html);
        }

    }
 }
 ?>