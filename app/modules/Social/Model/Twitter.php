<?php

class Social_Model_Twitter extends Core_Model_Default {

    const MAX_RESULTS = 5;

    protected $_is_cachable = false;

    protected $_twitter_user;
    protected $_list = array();
    protected $_page;
    protected $_next_url;
    private static $__access_token;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Social_Model_Db_Table_Twitter';
        return $this;
    }

}
