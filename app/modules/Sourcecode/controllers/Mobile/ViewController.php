<?php

class Sourcecode_Mobile_ViewController extends Application_Controller_Mobile_Default {

    public function _toJson($sourcecode){
        $json = array(
            "id" => $sourcecode->getId(),
            "htmlFilePath" => $sourcecode->getHtmlFilePath()
        );
        return $json;
    }

    public function findAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try {

                $option_value = $this->getCurrentOptionValue();
                $sourcecode = $option_value->getObject();

                $locker_option = $this->getApplication()->getOption("padlock");
                $locker = new Padlock_Model_Padlock();
                $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

                if(in_array($value_id, $lock_values_id))
                {
                    $locked = 1;
                } else {
               $locked = 2;
        }

                $data = array(
                    "sourcecode" => $this->_toJson($sourcecode),
                    "page_title" => $option_value->getTabbarName(),
                    "locked" => $locked
                );
            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        }else{
            $data = array('error' => 1, 'message' => 'An error occurred during process. Please try again later.');
        }

        $this->_sendHtml($data);

    }

}