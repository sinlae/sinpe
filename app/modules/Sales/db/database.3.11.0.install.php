<?php

$this->query("

    CREATE TABLE `sales_order` (
      `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `admin_id` int(11) unsigned NOT NULL,
      `number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `app_id` int(11) unsigned NOT NULL,
      `app_name` varchar(30) NOT NULL,
      `subscription_id` int(11) unsigned NOT NULL,
      `status_id` tinyint(1) NOT NULL DEFAULT '1',
      `payment_method` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
      `admin_company` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
      `admin_address` TEXT COLLATE utf8_unicode_ci NOT NULL,
      `admin_address2` VARCHAR(255) NULL,
      `admin_city` VARCHAR(255) NULL,
      `admin_zip_code` VARCHAR(255) NULL,
      `admin_region` VARCHAR(255) NULL,
      `admin_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
      `subtotal_excl_tax` decimal(8,4) NOT NULL,
      `total_excl_tax` decimal(8,4) NOT NULL,
      `tax_rate` decimal(5,2) NOT NULL DEFAULT 0.00,
      `total_tax` decimal(8,4) NOT NULL,
      `total` decimal(8,4) NOT NULL,
      `created_at` datetime NOT NULL,
      `paid_at` datetime DEFAULT NULL,
      PRIMARY KEY (`order_id`),
      KEY `KEY_ADMIN_ADMIN_ID` (`admin_id`),
      KEY `KEY_APPLICATION_APPLICATION_ID` (`app_id`),
      KEY `KEY_SUBSCRIPTION_SUBSCRIPTION_ID` (`subscription_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `sales_order_line` (
      `line_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `order_id` int(11) unsigned NOT NULL,
      `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `price_excl_tax` decimal(8,4) DEFAULT NULL,
      `qty` decimal(4,2) NOT NULL DEFAULT '1.00',
      `total_price_excl_tax` decimal(8,4) DEFAULT NULL,
      `is_recurrent` TINYINT(1) NOT NULL DEFAULT 0,
      PRIMARY KEY (`line_id`),
      KEY `KEY_SALES_ORDER_ORDER_ID` (`order_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

");

$this->query("

    CREATE TABLE `sales_invoice` (
      `invoice_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `admin_id` int(11) unsigned NOT NULL,
      `order_id` int(11) unsigned DEFAULT NULL,
      `number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `app_id` int(11) unsigned NOT NULL,
      `app_name` varchar(30) NOT NULL,
      `subscription_id` int(11) unsigned NOT NULL,
      `admin_company` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
      `admin_address` TEXT COLLATE utf8_unicode_ci NOT NULL,
      `admin_address2` VARCHAR(255) NULL,
      `admin_city` VARCHAR(255) NULL,
      `admin_region` VARCHAR(255) NULL,
      `payment_method` VARCHAR(255) NULL,
      `admin_zip_code` VARCHAR(255) NULL,
      `admin_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `admin_vat_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
      `subtotal_excl_tax` decimal(8,4) NOT NULL,
      `total_excl_tax` decimal(8,4) NOT NULL,
      `tax_rate` decimal(5,2) NOT NULL DEFAULT 0.00,
      `total_tax` decimal(8,4) NOT NULL,
      `total` decimal(8,4) NOT NULL,
      `created_at` datetime NOT NULL,
      PRIMARY KEY (`invoice_id`),
      KEY `KEY_ADMIN_ADMIN_ID` (`admin_id`),
      KEY `KEY_SALES_ORDER_ORDER_ID` (`order_id`),
      KEY `KEY_APPLICATION_APPLICATION_ID` (`app_id`),
      KEY `KEY_SUBSCRIPTION_SUBSCRIPTION_ID` (`subscription_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `sales_invoice_line` (
      `line_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `invoice_id` int(11) unsigned NOT NULL,
      `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `price_excl_tax` decimal(8,4) DEFAULT NULL,
      `qty` decimal(4,2) NOT NULL DEFAULT '1.00',
      `total_price_excl_tax` decimal(8,4) DEFAULT NULL,
      `is_recurrent` TINYINT(1) NOT NULL DEFAULT 0,
      PRIMARY KEY (`line_id`),
      KEY `KEY_INVOICE_INVOICE_ID` (`invoice_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

");

$this->query("
    ALTER TABLE `sales_order`
      ADD CONSTRAINT `FK_SALES_ORDER_ADMIN_ID` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
      ADD CONSTRAINT `FK_SALES_ORDER_APP_ID` FOREIGN KEY (`app_id`) REFERENCES `application` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE
    ;
");
$this->query("
    ALTER TABLE `sales_order_line`
      ADD CONSTRAINT `FK_SALES_ORDER_LINE_ORDER_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE
    ;
");


$this->query("
    ALTER TABLE `sales_invoice`
      ADD CONSTRAINT `FK_SALES_INVOICE_APP_ID` FOREIGN KEY (`app_id`) REFERENCES `application` (`app_id`) ON DELETE NO ACTION ON UPDATE CASCADE
    ;
");
$this->query("
    ALTER TABLE `sales_invoice_line`
      ADD CONSTRAINT `FK_SALES_INVOICE_LINE_INVOICE_ID` FOREIGN KEY (`invoice_id`) REFERENCES `sales_invoice` (`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE
    ;
");
