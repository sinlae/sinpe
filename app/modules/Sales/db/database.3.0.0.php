<?php

$this->query("
    ALTER TABLE `sales_order`
        ADD `admin_address2` VARCHAR(255) NULL AFTER `admin_address`,
        ADD `admin_city` VARCHAR(255) NULL AFTER `admin_address2`,
        ADD `admin_zip_code` VARCHAR(255) NULL AFTER `admin_city`,
        ADD `admin_region` VARCHAR(255) NULL AFTER `admin_zip_code`
    ;
");

$this->query("
    ALTER TABLE `sales_invoice`
        ADD `admin_address2` VARCHAR(255) NULL AFTER `admin_address`,
        ADD `admin_city` VARCHAR(255) NULL AFTER `admin_address2`,
        ADD `admin_zip_code` VARCHAR(255) NULL AFTER `admin_city`,
        ADD `admin_region` VARCHAR(255) NULL AFTER `admin_zip_code`,
        ADD `payment_method` VARCHAR(255) NULL AFTER `admin_region`
    ;
");

$this->query("ALTER TABLE `sales_invoice` DROP FOREIGN KEY `FK_SALES_INVOICE_ADMIN_ID`");
