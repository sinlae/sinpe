<?php

class Sales_Backoffice_Order_ViewController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Order"),
            "icon" => "fa-file-text-o",
        );

        $this->_sendHtml($html);

    }

    public function findAction() {

    }

}
