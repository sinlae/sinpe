<?php

class Sales_Model_Db_Table_Invoice extends Core_Model_Db_Table
{

    protected $_name = "sales_invoice";
    protected $_primary = "invoice_id";


    public function getNextIncrement()
    {

        $select = $this->_db->select()
            ->from('sales_invoice', array('last_increment' => new Zend_Db_Expr('SUBSTRING_INDEX(number, "-", -1)')))
            ->where('DATE(created_at) = ?', Siberian_Date::now()->toString('y-MM-dd'))
            ->order('invoice_id DESC')
            ->limit(1);

        return ((int)$this->_db->fetchOne($select)) + 1;

    }

    public function getStats()
    {


        $date = new Siberian_Date();
        $date->addDay(1);
        $endDate= $date->toString("yyyy-MM-dd HH:mm:ss");
        $date->setDay(1);
        $startDate = $date->toString("yyyy-MM-dd HH:mm:ss");


        $select = $this->select()
            ->from($this->_name, array("sum" => new Zend_Db_Expr("SUM(total)"),"count" => new Zend_Db_Expr("COUNT(total)"), "day" => new Zend_Db_Expr("DATE(created_at)")))
            ->where("created_at <= ?", $endDate)
            ->where("created_at > ?", $startDate)
            ->order("created_at")
            ->group("created_at");


        return $this->fetchAll($select);


    }
}
