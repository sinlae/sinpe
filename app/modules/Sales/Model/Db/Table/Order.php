<?php

class Sales_Model_Db_Table_Order extends Core_Model_Db_Table {

    protected $_name    = "sales_order";
    protected $_primary = "order_id";

    public function findByAppId($app_id) {

        $select = $this->select()
            ->from(array("so" => $this->_name))
            ->where("so.app_id = ?", $app_id)
            ->order("so.order_id DESC")
            ->limit(1)
        ;

        return $this->fetchRow($select);

    }

    public function getNextIncrement() {

        $select = $this->_db->select()
            ->from($this->_name, array('last_increment' => new Zend_Db_Expr('SUBSTRING_INDEX(number, "-", -1)')))
            ->where('DATE(created_at) = ?', Siberian_Date::now()->toString('y-MM-dd'))
            ->order('order_id DESC')
            ->limit(1)
        ;

        return ((int) $this->_db->fetchOne($select)) + 1;

    }

}
