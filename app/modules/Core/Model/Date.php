<?php

class Core_Model_Date
{
  public function getDateMinusOneDay($year, $month, $day)
  {
  	// //echo "hjhjh"; exit;
  	 $date = array();
  	 $month30 = array('04', '06', '09', '11');
  	 $month31 = array('01', '03', '05', '07', '08', '10', '12');
  	 if ($day != '01')
  	 {
  	   	$date["year"] = $year;
  	   	$date["month"] = $month;
  	 	  $date["day"] = $day-1;  
  	  }  else {
  	   	if ($month == '01')
  	   	{
  	   	    $date["year"] = $year-1;
  	   	    $date["month"] = '12';
  	   	    $date["day"] = '31';
  	   	} elseif($month == '03'){
           $bisextil = $year/4;
  	   		if(is_float($bisextil))
  	   		{
  	   		    $date["year"] = $year;
  	   	    	$date["month"] = '02';
  	   	    	$date["day"] = '28';	
  	   		} else {
    	   			$date["year"] = $year;
    	       	$date["month"] = '02';
    	  	    $date["day"] = '29';	
  	   		}	

  	 	}   else {	
	     		if(in_array(($month-1), $month30))
	     		{
	     			$date["year"] = $year;
	     			$date["month"] = $month-1;
	     			$date["day"] = '30';

	     		} elseif(in_array(($month-1), $month31)) {
	     			$date["year"] = $year;
	     			$date["month"] = $month-1;
	     			$date["day"] = '31';
	     		}
	     	}	
  	  }
  	return $date;	
  }
/*
	public function getNbDaysBetween($date1, $date2){
		$datetime1 = new DateTime($date1);
		$datetime2 = new DateTime($date2);
		$interval = $datetime1->diff($datetime2);
	    //return $interval->format('%R%a days');
	    return $interval;
	}*/

	public function getAllDaysBetween($year,$month,$day,$nbDays, $label=true)
	{
		$dates = array();
		$dates[0]["year"] = $year;
		$dates[0]["month"] = $month;
		$dates[0]["day"] = $day;
		$j = 0;
    $dates_label = array();
    $dates_label[0] =  $dates[0]["year"]."-".$dates[0]["month"]."-".$dates[0]["day"];
		for($i=$nbDays; $i>0; $i--) 
		{	
		    $dateminusone = Core_Model_Date::getDateMinusOneDay($dates[$j]["year"],$dates[$j]["month"], $dates[$j]["day"]);	
        $dates[$j+1]["year"] = $dateminusone["year"];
        $dates[$j+1]["month"] = $dateminusone["month"];
        $dates[$j+1]["day"] = $dateminusone["day"];
        $dates_label[$j+1] =  $dates[$j+1]["year"]."-".$dates[$j+1]["month"]."-".$dates[$j+1]["day"];
        $j++;
		}	

    if ($label)
    { 
	 	  return $dates_label;
    }  else {
      return $dates;
    } 
	}

  public function getLastTwelveMonth($year, $month, $label = false)
  {
    $allMonth = array();
    $allMonth[0]["month"] = $month;
    $allMonth[0]["year"] = $year;
    for($i=1; $i<12; $i++)
    {
      if($month=="01" || $allMonth[$i-1]["month"] == "01")
      {
        $allMonth[$i]["month"] = "12";
        $allMonth[$i]["year"] = $year-1;
      } else {
          $allMonth[$i]["month"] = ($allMonth[$i-1]["month"]-1);
          $allMonth[$i]["year"] = $allMonth[$i-1]["year"];
      }
    }
    if($label)
    {
       $dates = array();
       foreach ($allMonth as $date) {
          if($date['month'] < 10 && $date['month'] != $month)
          {  
            $date['month'] = "0".$date['month'];
          }
        $dates[] = $date['month']."-".$date['year'];
       }
       return $dates;
    } else {
        return  $allMonth;
    }
  }

}
?>