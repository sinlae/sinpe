<?php

class Application_Model_Website_BlockDetails extends Core_Model_Default {

    public function __construct($data = array()) {
    	parent::__construct($data);
    	$this->_db_table = "Application_Model_Db_Table_Website_BlockDetails";
	}

	public function getType() {
		$block = new Application_Model_Website_Block();
		$block = $block->find(array("block_id"=>$this->getBlockId()));
		return $block->getBlockType();
	}

	public function setParams($website_id,$block_id,$position,$name=null){
		$this->getTable()->setParams($website_id,$block_id,$position,$name);
	}

}	
