<?php

class Application_Model_Db_Table_Website_SocialContent extends Core_Model_Db_Table {

    protected $_name = "website_social_content";
    protected $_primary = "social_content_id";

}