<?php

class Application_Model_Db_Table_Website_BlockDetails extends Core_Model_Db_Table {

    protected $_name = "website_block_details";
    protected $_primary = "website_id";

    public function setParams($website_id,$block_id,$position,$name){
    	 $this->_db->insert('website_block_details', array("website_id"=>$website_id,"block_id"=>$block_id,"position"=>$position,"name"=>$name));
    }
}