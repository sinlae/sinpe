<?php

class Application_Model_Website extends Core_Model_Default {

    public function __construct($data = array()) {
    	parent::__construct($data);
    	$this->_db_table = "Application_Model_Db_Table_Website";
	}

	public function getBlocks() {
		$blocks = new Application_Model_Website_BlockDetails();
		$blocks = $blocks->findAll(array("website_id"=>$this->getWebsiteId()), "position ASC");
		return $blocks;
	}

	public function getBlockByPosition($positon) {
		$blocks = new Application_Model_Website_BlockDetails();
		$block_detail = $blocks->find(array("website_id"=>$this->getWebsiteId(), "position"=>0));
		$block = new Application_Model_Website_Block();
		$block = $block->find(array("block_id"=>$block_detail->getBlockId()));
		return $block->getBlockType();
	}

	public function getBlockDetailsByType($type) {
		$block = new Application_Model_Website_Block();
		$block = $block->find(array("block_type" => $type));

		$block_details = new Application_Model_Website_BlockDetails();
		$block_details = $block_details->find(array("website_id"=>$this->getWebsiteId(), "block_id"=>$block->getBlockId()));

		return $block_details;

	}

	public function getYoutubeUrl($embed=false){
		$video = new Application_Model_Website_VideoDescription();
		$video = $video->find(array("website_id"=>$this->getWebsiteId()));
		if($embed){
			$url = str_replace("watch?v=", "embed/", $video->getYoutubeUrl());
		} else {
			$url = $video->getYoutubeUrl();
		}
		
		return $url;
	}

	public function getSliderImages(){
		$slider = new Application_Model_Website_SliderImage();
		$slider = $slider->findAll(array("website_id"=>$this->getWebsiteId()));
		return $slider;
	}
}	