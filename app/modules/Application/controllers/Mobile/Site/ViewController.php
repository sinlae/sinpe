<?php

class Application_Mobile_Site_ViewController extends Application_Controller_Mobile_Default {

    public function indexAction() {
    	$this->loadPartials("front_index_index");
    }

    public function templateAction() {
        $layout_id = $this->getApplication()->getWebsite()->getLayoutId();
        $this->loadPartials($this->getFullActionName('_').'_l'.$layout_id, false);
    }

    public function subscribeAction() {
        if ($data = Zend_Json::decode($this->getRequest()->getRawBody())) {
            try {
                if($data["email"] != ""){
                    $email = $data["email"];
                    $firstname = $data["firstname"];
                    $name = $data["name"];
                    $newsletter = new Application_Model_Website_NewsletterContact();
                    $newsletter = $newsletter->find(array("app_id"=>$this->getApplication()->getAppId(),"email"=>$email));
                    if(!$newsletter->getContactId()){
                       $newsletter->setAppId($this->getApplication()->getAppId())
                                  ->setName($name)
                                  ->setFirstname($firstname)
                                  ->setEmail($email)
                                  ->save();
                    }
                }
                $data["success"] = 1;
            } catch(Exception $e)  {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }
        }
        $this->_sendHtml($data);
    }

    public function findsiteallowedAction()
    {
        $now = Zend_Date::now(); 
        if($now->get('YYYY-MM-dd HH:mm:ss') < $this->getApplication()->getFreeUntil() || $this->getApplication()->getFreeUntil() == NULL ) 
        {
            $data["website"] = true;
            $data["success"] = 1;

        } else 
        {
    	    try
    	    {
    		    $subscribtion = new Subscription_Model_Subscription_Application();
    		    $subscribtion = $subscribtion->find(array("app_id"=>$this->getApplication()->getAppId()));

    	        $acl = new Subscription_Model_Acl_Resource();
    		    $acl = $acl->findBySubscriptionId($subscribtion->getSubscriptionId());
    		    $aclcodes = array();
    		    $data = array();

    		    foreach($acl as $key => $acl_code)
    		    {
    			    $aclcodes[] = $acl_code->getCode();
    		    }

    		    if(in_array("feature_website", $aclcodes))
    		    {
    			    $data["website"] = false;
    		    } else {
    			    $data["website"] = true;
    		    }	

    		    $data["success"] = 1;

    	    } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }
        }

        $this->_sendHtml($data);
    }
}