<?php

class Application_Customization_Design_ColorsController extends Application_Controller_Default {

    public function editAction() {
        $this->loadPartials();

        if($this->getRequest()->isXmlHttpRequest()) {
            $html = array('html' => $this->getLayout()->getPartial('content_editor')->toHtml());
            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }
    }

    public function saveAction() {

        if($datas = $this->getRequest()->getPost()) {

            try {

                    $application = $this->getApplication();
                    $background_block_id = $application->getBlock("background")->getBlockId();
                    $news_block_id = $application->getBlock("news")->getBlockId();
                    $button_block_id = $application->getBlock("button")->getBlockId();
                    $connect_button_block_id = $application->getBlock("connect_button")->getBlockId();
                    $discount_block_id = $application->getBlock("discount")->getBlockId();
                    $subheader_block_id = $application->getBlock("subheader")->getBlockId();
                    $header_block_id = $application->getBlock("header")->getBlockId();
                    $tabbar_block_id = $application->getBlock("tabbar")->getBlockId();
                    
                    //set block background colors
                    $block = new Template_Model_Block();
                    $block->find($datas['background_id']);
                    if(!empty($datas['background_color'])) {
                    $block->setData('color', $datas['background_color']);
                    }
                    if(!empty($datas['background_background_color'])) {
                    $block->setData('background_color', $datas['background_background_color']);
                    }
                    $block->setBlockId($datas['background_id'])
                        ->setAppId($application->getId())
                        ->save();

                    //set connect button colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($connect_button_block_id);
                    if(!empty($datas['header_background_color'])) {
                      $blockc->setData('color', $datas['header_background_color']);
                    }
                    if(!empty($datas['header_color'])) {
                     $blockc->setData('background_color', $datas['header_color']);
                    }
                    $blockc->setBlockId($connect_button_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    //set news colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($news_block_id);
                    if(!empty($datas['background_color'])) {
                      $blockc->setData('color', $datas['background_color']);
                    }
                    if(!empty($datas['background_background_color'])) {
                     $blockc->setData('background_color',$datas['background_background_color']);
                    }
                    $blockc->setBlockId($news_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    //set tabbar colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($tabbar_block_id);
                    if(!empty($datas['header_color'])) {
                      $blockc->setData('color',$datas['header_color']);
                      $blockc->setData('image_color', $datas['header_color']); 
                    }
                    if(!empty($datas['tabbar_transparent']))
                    {
                      $blockc->setData('background_color',"transparent");
                    } elseif(empty($datas['tabbar_transparent']) && empty($datas['header_background_color']))
                    {
                      $blockc->setData('background_color',$application->getBlock('header')->getBackgroundColor());
                    } elseif(!empty($datas['header_background_color'])) {
                     $blockc->setData('background_color',$datas['header_background_color']);
                    }
                    $blockc->setBlockId($tabbar_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    //set discount colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($discount_block_id);
                    if(!empty($datas['background_color'])) {
                      $blockc->setData('color',$datas['color']);
                    }
                    if(!empty($datas['background_background_color'])) {
                     $blockc->setData('background_color',$datas['background_background_color']);
                    }
                    $blockc->setBlockId($discount_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    //set header colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($button_block_id);
                    if(!empty($datas['header_background_color'])) {
                      $blockc->setData('color', $datas['header_background_color']);
                    }
                    if(!empty($datas['header_color'])) {
                     $blockc->setData('background_color', $datas['header_color']);
                    }
                    $blockc->setBlockId($button_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    // Set header colors
                    $block = new Template_Model_Block();
                    $block->find($datas['header_id']);
                    if(!empty($datas['header_color'])) {
                    $block->setData('color', $datas['header_color']);
                    }
                    if(!empty($datas['header_background_color'])) {
                    $block->setData('background_color', $datas['header_background_color']);
                    }
                    $block->setBlockId($datas['header_id'])
                    ->setAppId($application->getId())
                    ->save();

                    // Set subheader colors
                    $blockc = new Template_Model_Block();
                    $blockc->find($subheader_block_id);
                    if(!empty($datas['header_color'])) {
                          $blockc->setData('color', $datas['header_color']);
                    }
                    if(!empty($datas['header_background_color'])) {
                     $blockc->setData('background_color', $datas['header_background_color']);
                    }
                    $blockc->setBlockId($subheader_block_id)
                    ->setAppId($application->getId())
                    ->save();

                    // Set comment colors
                    $block = new Template_Model_Block();
                    $block->find($datas['comments_id']);
                    if(!empty($datas['comments_color'])) {
                    $block->setData('color', $datas['comments_color']);
                    }
                    if(!empty($datas['comments_background_color'])) {
                    $block->setData('background_color', $datas['comments_background_color']);
                    }
                    $block->setBlockId($datas['comments_id'])
                    ->setAppId($application->getId())
                    ->save();

                $html = array(
                    'success' => '1',
                    "tabbar_is_transparent" => $block->getBackgroundColor() == "transparent",
                );

                $this->redirect("application/customization_design_colors/edit");
            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage()
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }

    }
}
