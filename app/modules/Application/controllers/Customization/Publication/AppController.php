<?php

class Application_Customization_Publication_AppController extends Application_Controller_Default {

    public function indexAction() {
        $this->loadPartials();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $html = array('html' => $this->getLayout()->getPartial('content_editor')->toHtml());
            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }
    }

    public function iconAction() {
        $this->getLayout()->setBaseRender('content', 'application/customization/publication/app/icon.phtml', 'admin_view_default');
        $html = array('html' => $this->getLayout()->render());
        $this->getLayout()->setHtml(Zend_Json::encode($html));
    }

    public function startupAction() {
        $this->getLayout()->setBaseRender('content', 'application/customization/publication/app/startup.phtml', 'admin_view_default');
        $html = array('html' => $this->getLayout()->render());
        $this->getLayout()->setHtml(Zend_Json::encode($html));
    }

public function saveiconAction() {

        if ($datas = $this->getRequest()->getPost()) {

            $html = '';

            try {

                if(!empty($datas['file'])) {

                    $icon_relative_path = '/'.$this->getApplication()->getId().'/icon/';

                    $folder = Application_Model_Application::getBaseImagePath().$icon_relative_path;

                    $datas['dest_folder'] = $folder;

                    $datas['new_name'] = $datas['file'];

                    $uploader = new Core_Model_Lib_Uploader();

                    $file = $uploader->savecrop($datas);

                    $format = pathinfo($file, PATHINFO_EXTENSION);
                    //Icon must be forced to png

                    if($format != "png") {

                        switch ($format) {

                            case 'jpg':

                            case 'jpeg':

                                $image = imagecreatefromjpeg($folder . $file);

                                break;

                            case 'gif':

                                $image = imagecreatefromgif($folder . $file);

                                break;

                        }

                        $new_name = uniqid().".png";

                        imagepng($image, $folder.$new_name);
                        if($this->getApplication()->getIcon() && (strpos($this->getApplication()->getIcon(), "placeholder") !== false))
                        {
                            unlink($folder.$file);
                            unlink($this->getApplication()->getIcon(null, null, false,true));
                            unlink($this->getApplication()->getIcon(null,null,true));
                        }    
                        $this->getApplication()->setIcon($icon_relative_path.$new_name)->save();
                        $customer = new Customer_Model_Customer();
                        $customer_admin = $customer->getAdmin($this->getApplication()->getId(), $this->getApplication()->getName());
                        $customer_admin->setImage($this->getApplication()->getIcon());
                        $customer_admin->save();

                    } else {
                        if($this->getApplication()->getIcon() && (strpos($this->getApplication()->getIcon(), "placeholder") !== false))
                        {
                            unlink($this->getApplication()->getIcon(null, null, false,true));
                            unlink($this->getApplication()->getIcon(null,null,true));
                        } 
                       // throw new exception($this->_(Application_Model_Application::getBaseImagePath().$this->getApplication()->getIcon()));
                        $this->getApplication()->setIcon($icon_relative_path.$file)->save();
                        $customer = new Customer_Model_Customer();
                        $customer_admin = $customer->getAdmin($this->getApplication()->getId(), $this->getApplication()->getName());
                        $customer_admin->setImage($this->getApplication()->getIcon());
                        $customer_admin->save();
                    }
                    $html = array(

                        'success' => 1,

                        'file' => $this->getApplication()->getIcon(128)
                    );
                }
                else {

                    $this->getApplication()->setIcon(null)->save();
                }
            } catch (Exception $e) {

                $html = array(
                    'message' => $e->getMessage()

                );

            }
            $this->getLayout()->setHtml(Zend_Json::encode($html));
        }
    }

    public function getfiledataAction(){
        if ($datas = $this->getRequest()->getPost()) {

            $html = '';
            if(!empty($datas['file'])) {
                $watermark_relative_path = '/'.$this->getApplication()->getId().'/watermark/';
                $folder = Application_Model_Application::getBaseImagePath().$watermark_relative_path;
                $datas['dest_folder'] = $folder;
                $datas['new_name'] = $file;
                $uploader = new Core_Model_Lib_Uploader();
                $file = $uploader->savecrop($datas);
                 $html = array(

                    'success' => 1,
                    'file' => $file
                );
            }
        }
        $this->getLayout()->setHtml(Zend_Json::encode($html)); 
    }

    public function savewatermarkAction() {
        if($file = $this->getRequest()->getParam('file')) {
            $html = '';

            try {

                if(!empty($file)) {

                    $watermark_relative_path = '/'.$this->getApplication()->getId().'/watermark/';

                    $folder = Application_Model_Application::getBaseImagePath().$watermark_relative_path;

                    $datas['dest_folder'] = $folder;

                    $datas['new_name'] = $file;

                    $uploader = new Core_Model_Lib_Uploader();

                    //$file = $uploader->savecrop($datas);

                    $format = pathinfo($file, PATHINFO_EXTENSION);
                     //Icon must be forced to png
                    $color = $this->getRequest()->getParam('color');

                    $logo = imagecreatefrompng($folder.$datas['new_name']);
                    $logo_resize = $uploader->resizeImage($folder.$datas['new_name'], imagesx($logo)*2, imagesy($logo)*2, $folder);
                     
                    $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
                    $config=parse_ini_file($ini);
                    $awsAccessKey = $config["awsAccessKey"];
                    $awsSecretKey = $config["awsSecretKey"];
         
                    //create s3 client
                    $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

                    $uploader = new Core_Model_Lib_Uploader();

                    $id_filename = $this->getApplication()->getAppId()."/640x1136";
                    //Background Images
                    if($this->getRequest()->getParam('splashscreenonly') == 0){
                        $this->saveAllImages($color, 640,1136, $folder, $datas['new_name']);
                        $file_url = $uploader->saveImageContent($folder."640x1136.png", $id_filename, $config, $watermark_relative_path, $folder, $client, false);
                        $this->getApplication()->setBackgroundImage($file_url);
                        unlink($folder."640x1136.png");

                        $id_filename = $this->getApplication()->getAppId()."/1242x2208";
                        $this->saveAllImages($color, 1242,2208, $folder, $logo_resize);
                        $file_url = $uploader->saveImageContent($folder."1242x2208.png",$id_filename, $config, $watermark_relative_path, $folder, $client, false);
                        $this->getApplication()->setBackgroundImageHd($file_url);
                        unlink($folder."1242x2208.png");

                        $id_filename = $this->getApplication()->getAppId()."/1536x2048";
                        $this->saveAllImages($color, 1536,2048, $folder, $logo_resize);
                        $file_url = $uploader->saveImageContent($folder."1536x2048.png", $id_filename, $config, $watermark_relative_path, $folder, $client, false);
                        $this->getApplication()->setBackgroundImageTablet($file_url);
                        unlink($folder."1536x2048.png");
                    }

                    //startup images
                    $relative_path = '/'.$this->getApplication()->getId().'/startup_image/';
                    $startup_image_folder = Application_Model_Application::getBaseImagePath().$relative_path;
                    if (!is_dir($startup_image_folder)) mkdir($startup_image_folder, 0777, true);
                    $this->saveAllImages($color, 640,960, $startup_image_folder, $datas['new_name']);
                    $this->getApplication()->setStartupImage($relative_path."640x960.png")->save();

                    $this->saveAllImages($color, 640,1136, $startup_image_folder, $datas['new_name']);
                    $this->getApplication()->setStartupImageRetina($relative_path."640x1136.png")->save();

                    $this->saveAllImages($color, 750,1334, $startup_image_folder, $datas['new_name']);
                    $this->getApplication()->setStartupImageIphone6($relative_path."750x1334.png")->save();

                    $this->saveAllImages($color, 1242,2208, $startup_image_folder, $logo_resize);
                    $this->getApplication()->setStartupImageIphone6Plus($relative_path."1242x2208.png")->save();

                    $this->saveAllImages($color, 1536,2048, $startup_image_folder, $logo_resize);
                    $this->getApplication()->setStartupImageIpadRetina($relative_path."1536x2048.png")->save();

                    unlink($folder.$datas['new_name']);
                    unlink($folder.$logo_resize);
                    unlink($folder."background.jpg");
                    imagedestroy($logo);

                    $files = glob(Application_Model_Application::getBaseImagePath().'/'.$this->getApplication()->getAppId().'/watermark/*.png'); // get all file names
                    foreach($files as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }

                    $html = array(

                        'success' => 1,
                        'color' => $color

                    );
                }
            } catch (Exception $e) {

                $html = array(
                     'message' => $e->getMessage()
                );

            }  
            $this->getLayout()->setHtml(Zend_Json::encode($html)); 
        }
    }
    // public function savewatermarkAction() {
    //     if ($datas = $this->getRequest()->getPost()) {

    //         $html = '';

    //         try {

    //             if(!empty($datas['file'])) {

    //                 $watermark_relative_path = '/'.$this->getApplication()->getId().'/watermark/';

    //                 $folder = Application_Model_Application::getBaseImagePath().$watermark_relative_path;

    //                 $datas['dest_folder'] = $folder;

    //                 $datas['new_name'] = $datas['file'];

    //                 $uploader = new Core_Model_Lib_Uploader();

    //                 $file = $uploader->savecrop($datas);

    //                 $format = pathinfo($file, PATHINFO_EXTENSION);
    //                  //Icon must be forced to png
    //                 $color = $this->getRequest()->getParam('color');

    //                 $logo = imagecreatefrompng($folder.$datas['new_name']);
    //                 $logo_resize = $uploader->resizeImage($folder.$datas['new_name'], imagesx($logo)*2, imagesy($logo)*2, $folder);
                     
    //                 $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
    //                 $config=parse_ini_file($ini);
    //                 $awsAccessKey = $config["awsAccessKey"];
    //                 $awsSecretKey = $config["awsSecretKey"];
         
    //                 //create s3 client
    //                 $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

    //                 $uploader = new Core_Model_Lib_Uploader();

    //                 $id_filename = $this->getApplication()->getAppId()."/640x1136";
    //                 //Background Images
    //                 $this->saveAllImages($color, 640,1136, $folder, $datas['new_name']);
    //                 $file_url = $uploader->saveImageContent($folder."640x1136.png", $id_filename, $config, $watermark_relative_path, $folder, $client, false);
    //                 $this->getApplication()->setBackgroundImage($file_url);
    //                 unlink($folder."640x1136.png");

    //                 $id_filename = $this->getApplication()->getAppId()."/1242x2208";
    //                 $this->saveAllImages($color, 1242,2208, $folder, $logo_resize);
    //                 $file_url = $uploader->saveImageContent($folder."1242x2208.png",$id_filename, $config, $watermark_relative_path, $folder, $client, false);
    //                 $this->getApplication()->setBackgroundImageHd($file_url);
    //                 unlink($folder."1242x2208.png");

    //                 $id_filename = $this->getApplication()->getAppId()."/1536x2048";
    //                 $this->saveAllImages($color, 1536,2048, $folder, $logo_resize);
    //                 $file_url = $uploader->saveImageContent($folder."1536x2048.png", $id_filename, $config, $watermark_relative_path, $folder, $client, false);
    //                 $this->getApplication()->setBackgroundImageTablet($file_url);
    //                 unlink($folder."1536x2048.png");

    //                 //startup images
    //                 $relative_path = '/'.$this->getApplication()->getId().'/startup_image/';
    //                 $startup_image_folder = Application_Model_Application::getBaseImagePath().$relative_path;
    //                 if (!is_dir($startup_image_folder)) mkdir($startup_image_folder, 0777, true);
    //                 $this->saveAllImages($color, 640,960, $startup_image_folder, $datas['new_name']);
    //                 $this->getApplication()->setStartupImage($relative_path."640x960.png")->save();

    //                 $this->saveAllImages($color, 640,1136, $startup_image_folder, $datas['new_name']);
    //                 $this->getApplication()->setStartupImageRetina($relative_path."640x1136.png")->save();

    //                 $this->saveAllImages($color, 750,1134, $startup_image_folder, $datas['new_name']);
    //                 $this->getApplication()->setStartupImageIphone6($relative_path."750x1134.png")->save();

    //                 $this->saveAllImages($color, 1242,2208, $startup_image_folder, $logo_resize);
    //                 $this->getApplication()->setStartupImageIphone6Plus($relative_path."1242x2208.png")->save();

    //                 $this->saveAllImages($color, 1536,2048, $startup_image_folder, $logo_resize);
    //                 $this->getApplication()->setStartupImageIpadRetina($relative_path."1536x2048.png")->save();

    //                 unlink($folder.$datas['new_name']);
    //                 unlink($folder.$logo_resize);
    //                 unlink($folder."background.jpg");
    //                 imagedestroy($logo);

    //                 $html = array(

    //                     'success' => 1,
    //                     'color' => $color

    //                 );
    //             }
    //         } catch (Exception $e) {

    //             $html = array(
    //                  'message' => $e->getMessage()
    //             );

    //         }  
    //     }
    //     $this->getLayout()->setHtml(Zend_Json::encode($html)); 
    // }   

    public function saveAllImages($color, $width, $height, $folder, $name) {
        $watermark_relative_path = '/'.$this->getApplication()->getId().'/watermark/';
        $watermark_folder = Application_Model_Application::getBaseImagePath().$watermark_relative_path;

        $rgbArray = $this->_hex2RGB($color);

        $image = imagecreate($width,$height);

        $background = imagecolorallocate($image, $rgbArray['red'], $rgbArray['green'], $rgbArray['blue']);

        $destination = imagejpeg($image, $watermark_folder."background.jpg"); 

        $im = imagecreatefromjpeg($watermark_folder."background.jpg");

        $stamp = imagecreatefrompng($watermark_folder.$name);

        $largeur_source = imagesx($stamp);
        $hauteur_source = imagesy($stamp);
        $largeur_destination = imagesx($im);
        $hauteur_destination = imagesy($im);

        // // On veut placer le logo en bas à droite, on calcule les coordonnées où on doit placer le logo sur la photo
        $destination_x = ($largeur_destination - $largeur_source)/2;
        $destination_y =  ($hauteur_destination - $hauteur_source)/2;

        // Définit les marges pour le cachet et récupère la hauteur et la largeur de celui-ci
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);

        // Copie le cachet sur la photo en utilisant les marges et la largeur de la
        // photo originale  afin de calculer la position du cachet 
        imagecopy($im, $stamp, $destination_x, $destination_y, 0, 0, imagesx($stamp), imagesy($stamp));

        // Affichage et libération de la mémoire
        imagepng($im, $folder.$width."x".$height.".png");
        imagedestroy($im);
        imagedestroy($stamp);
        //unlink($folder.$name);
    }

    public function savestartupAction() {

        if($datas = $this->getRequest()->getPost()) {

            try {
                $application = $this->getApplication();
                $relative_path = '/'.$application->getId().'/startup_image/';
                $filetype = $this->getRequest()->getParam('filetype');
                $folder = Application_Model_Application::getBaseImagePath().$relative_path;
                $datas['dest_folder'] = $folder;
                $datas['new_name'] = $datas['file'];

                $uploader = new Core_Model_Lib_Uploader();
                $file = $uploader->savecrop($datas);
                $url = "";

                $format = pathinfo($file, PATHINFO_EXTENSION);

                //Startup images must be forced to png
                if($format != "png") {
                    switch ($format) {
                        case 'jpg':
                        case 'jpeg':
                            $image = imagecreatefromjpeg($folder . $file);
                            break;
                        case 'gif':
                            $image = imagecreatefromgif($folder . $file);
                            break;
                    }
                    $new_name = uniqid().".png";
                    imagepng($image, $folder.$new_name);
                    $file = $new_name;
                }

                if($filetype == "standard") $application->setData("startup_image", $relative_path.$file);
                else $application->setData("startup_image_".$filetype, $relative_path.$file);

                $application->save();

                $datas = array(
                    'success' => 1,
                    'file' => $application->getStartupImageUrl($filetype)
                );

            } catch (Exception $e) {
                $datas = array(
                    'error' => 1,
                    'message' => $e->getMessage()
                );
            }

            $this->getLayout()->setHtml(Zend_Json::encode($datas));
        }
    }

    protected function _createIcon($datas) {

        // Créé l'icône
        $image = imagecreatetruecolor(256, 256);

        // Rempli la couleur de fond
        $rgb = $this->_hex2RGB(000000);
        $background_color = imagecolorallocate($image, $rgb['red'], $rgb['green'], $rgb['blue']);
        imagefill($image, 0, 0, $background_color);
        $targ_w = $targ_h = 256;
        if(!empty($datas['icon']['file'])) {
            //Applique l'image
            $logo_relative_path = '/logo/';
            $folder = Application_Model_Application::getBaseImagePath().$logo_relative_path;
            if (!is_dir($folder))
                mkdir($folder, 0777, true);

            $src = Core_Model_Directory::getTmpDirectory(true).'/'.$datas['icon']['file'];
            $source = imagecreatefromstring(file_get_contents($src));
        }
        $dest = ImageCreateTrueColor($targ_w, $targ_h);
        imagecopyresampled($dest,$image,0,0,0,0,$targ_w,$targ_h,$targ_w,$targ_h);
        if($datas['icon']['file'] != '') {
            imagecopyresampled($dest,$source,0,0,$datas['icon']['x1'],$datas['icon']['y1'],$targ_w,$targ_h,$datas['icon']['w'],$datas['icon']['h']);
        }

        return $dest;
    }

    protected function _hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {

        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr);
        $rgbArray = array();

        if (strlen($hexStr) == 6) {
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) {
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false;
        }

        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray;
    }

}
