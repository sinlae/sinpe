<?php
require_once "aws/aws-autoloader.php";
use Aws\S3\S3Client;
class Application_WebsiteController extends Application_Controller_Default {

    public function indexAction() {
        $this->loadPartials();
    }

    public function saveAction() {

        if($datas = $this->getRequest()->getPost()) {

            try 
            {

                $application = $this->getApplication();
                $website = new Application_Model_Website();
                $website = $website->find(array("app_id" => $application->getId()));
                $blocks = $website->getBlocks();
                foreach ($blocks as $key => $block) {
                    $block->delete();
                }

                if(isset($datas["order"]))
                {
                    $order = str_replace("&block[]=", ",", $datas["order"]);
                    $order = explode(",", $order);
                    $order = array_flip($order);
                } else {
                    $allblocks = new Application_Model_Website_Block();
                    $allblocks = $allblocks->findAll();
                    $order = array();
                    foreach ($allblocks as $key => $block) {
                       $order[] = $block->getBlockType();
                    }
                    $order = array_flip($order);
                }
                if(isset($datas["name"]))
                {
                    $website->setName($datas["name"]);
                }
                if(isset($datas["design"]))
                {
                    $website->setLayoutId($datas["design"]);
                }
                if(isset($datas["biography"]))
                {
                    $website->setBiography($datas["biography"]);

                }
                if(isset($datas["contact_mail"]))
                {
                    $website->setContactEmail($datas["contact_mail"]);
                }   
                if(isset($datas["shop_url"]))
                {
                    $website->setShopUrl($datas["shop_url"]);
                }
                if(isset($datas["facebook_id"]))
                {
                    $website->setFacebookId($datas["facebook_id"]);
                }
                if(isset($datas["image"]))
                {
                    $website->setImage(1);
                    $image_block = new Application_Model_Website_BlockDetails();
                    $image_block->setParams($website->getWebsiteId(),2,$order['image'],$name);
                } else {
                    $website->setImage(0);
                }
                if(isset($datas["bio"]))
                {
                    $website->setBio(1);
                    $bio_block = new Application_Model_Website_BlockDetails();
                    $name = isset($datas["bio_name"]) ? $datas["bio_name"] : null;
                    $bio_block->setParams($website->getWebsiteId(),1,$order['bio'],$name);
                } else {
                    $website->setBio(0);
                }
                if(isset($datas["video"]))
                {
                    $website->setVideo(1);
                    $video_block = new Application_Model_Website_BlockDetails();
                    $name = isset($datas["video_name"]) ? $datas["video_name"] :null;
                    $video_block->setParams($website->getWebsiteId(),3,$order['video'],$name);
                } else {
                    $website->setVideo(0);
                }
                if(isset($datas["music"]))
                {
                    $website->setMusic(1);
                    $music_block = new Application_Model_Website_BlockDetails();
                    $name = isset($datas["music_name"]) ? $datas["music_name"] :null;
                    $music_block->setParams($website->getWebsiteId(),5,$order['music'],$name);
                } else {
                    $website->setMusic(0);
                }
                if(isset($datas["event"]))
                {
                    $website->setEvent(1);
                    $event_block = new Application_Model_Website_BlockDetails();
                    $name = isset($datas["event_name"]) ? $datas["event_name"] :null;
                    $event_block->setParams($website->getWebsiteId(),4,$order['event'],$name);
                } else {
                    $website->setEvent(0);
                }
                if(isset($datas["shop"]))
                {
                    $website->setShop(1);
                    $shop_block = new Application_Model_Website_BlockDetails();
                    $name = isset($datas["shop_name"]) ? $datas["shop_name"] : null;
                    $shop_block->setParams($website->getWebsiteId(),6,$order['shop'],$name);
                } else {
                    $website->setShop(0);
                }
                if(isset($datas["general_conditions"]))
                {
                    $website->setGeneralConditions($datas["general_conditions"]);
                }
                if(isset($datas["custom_css"]))
                {
                    $website->setCustomCss($datas["custom_css"]);
                }
                if(isset($datas["social_name"]) && isset($datas["social_url"])){
                    foreach ($datas["social_name"] as $key => $social) {
                        if($datas["social_url"][$key] != "") {
                           $social_content = new Application_Model_Website_SocialContent();
                           $social_content->setWebsiteId($website->getWebsiteId());
                           $social_content->setSocialId($datas["social_name"][$key]);
                           $social_content->setSocialUrl($datas["social_url"][$key]);
                           $social_content->save();
                        }
                    }
                }

                if(isset($datas["promote"])){
                    if($datas["promote"] == "promote"){
                        $website->setPromoteApp(1);
                        $website->setVideoDescription(0);
                        $website->setPhotoSlider(0);
                    }
                    if($datas["promote"] == "photo_slider"){
                        $website->setPromoteApp(0);
                        $website->setVideoDescription(0);
                        $website->setPhotoSlider(1);
                    }
                    if($datas["promote"] == "video_description"){
                        $website->setPromoteApp(0);
                        $website->setVideoDescription(1);
                        $website->setPhotoSlider(0);

                        if(isset($datas["video_url"])){
                            $video = new Application_Model_Website_VideoDescription();
                            $video = $video->find(array("website_id"=>$website->getWebsiteId()));
                            $video->setWebsiteId($website->getWebsiteId())
                                  ->setYoutubeUrl($datas["video_url"])
                                  ->save();
                        }
                    }
                    if($datas["promote"] == "none"){
                        $website->setPromoteApp(0);
                        $website->setVideoDescription(0);
                        $website->setPhotoSlider(0);
                    }
                    if(isset($datas["newsletter"])){
                        $website->setNewsletterSubscription(1);
                    } else {
                        $website->setNewsletterSubscription(0);
                    }
                }

                $website->save();
                $html = array(
                    'success' => '1',
                );

                $this->redirect("application/website/index");

            }  catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage()
                );
            }
        }
    }

    public function savesliderAction(){
        if ($datas = $this->getRequest()->getPost()) {

            $html = '';

            try {

                if(!empty($datas['file'])) {

                    $image = new Application_Model_Website_SliderImage();
                    $image = $image->findAll(array("website_id"=>$this->getRequest()->getParam('website_id')));
                    if(count($image) == 3){
                        $this->getSession()->addError($this->_('You already have 3 images on your animation'));
                    } else {

                    $slider_relative_path = '/'.$this->getApplication()->getId().'/slider/';

                    $folder = Application_Model_Application::getBaseImagePath().$slider_relative_path;

                    $datas['dest_folder'] = $folder;

                    $datas['new_name'] = $datas['file'];

                    $uploader = new Core_Model_Lib_Uploader();

                    $file = $uploader->savecrop($datas);

                    $format = pathinfo($file, PATHINFO_EXTENSION);
                     //Icon must be forced to png
                     
                    $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
                    $config=parse_ini_file($ini);
                    $awsAccessKey = $config["awsAccessKey"];
                    $awsSecretKey = $config["awsSecretKey"];
                        
                    //create s3 client
                    $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);

                    $uploader = new Core_Model_Lib_Uploader();
                    $image = $folder.'/'.$file;
                    $id_filename = $this->getApplication()->getAppId()."/slider";
                    $dest = Application_Model_Application::getBaseImagePath().$slider_relative_path;
                    $file_url = $uploader->saveImageContent($image, $id_filename, $config, $slider_relative_path, $dest, $client, false);
                    unlink($image);
                    $image = new Application_Model_Website_SliderImage();
                    $image->setWebsiteId($this->getRequest()->getParam('website_id'))
                          ->setImageUrl($file_url)
                          ->save();
                    }

                    $html = array(

                        'success' => 1,

                    );
                }
            } catch (Exception $e) {

                $html = array(
                     'message' => $e->getMessage()
                );

            }  
        }
        $this->getLayout()->setHtml(Zend_Json::encode($html)); 
    }

    public function deletesliderimageAction(){
        $image_id = $_POST["image_id"];
        $image = new Application_Model_Website_SliderImage();
        $image = $image->find(array("image_id"=>$_POST["image_id"]));
        $image->delete();
        $html = array('success' => 1);
        $this->_sendHtml(json_encode($html));
    }

    public function deletesocialAction(){
        $social_content_id = $_POST["social_content_id"];
        $social_network = new Application_Model_Website_SocialContent();
        $social_network = $social_network->find(array("social_content_id"=>$_POST["social_content_id"]));
        $social_network->delete();
        $html = array('success' => 1);
        $this->_sendHtml(json_encode($html));
    }

    public function savedomainAction(){
        $website = new Application_Model_Website();
        $website = $website->find(array("app_id" => $this->getApplication()->getId()));
        $website->setDomain($_POST["domain"]);
        $website->save();
        $config = array();
        $config['aws_access_key_id'] = 'AKIAIPXG7WXXV35HVQRQ';
        $config['aws_secret_access_key'] = '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';

        $config['region'] = 'us-east-1';
        $ec2Client = S3Client::factory(array(
         'credentials' => [
             'key' => "AKIAIPXG7WXXV35HVQRQ",
             'secret' => "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",
         ],
         'region'  => 'us-east-1',
         'version' => 'latest'
        ));
        $domain = $website->getDomain();
        $bio = str_replace("\n", ' ',strip_tags($website->getBiography()));
        $bio = str_replace("\r\n", " ", $bio); 
        $bio = str_replace("\r", " ", $bio);
        $website_name = $website->getName() ? $website->getName() : $this->getApplication()->getName();
        $image_url = $this->getUrl().$this->getApplication()->getIcon(128);
        $favicon_url = substr($this->getUrl(), 0, -1).$this->getApplication()->getIcon(32);
        $website_url = $this->getApplication()->getUrl()."website";
        $bio = trim($bio);
        try{
            $ec2Client->createBucket(array('ACL' => 'public-read', 'Bucket' => $domain));
            $result = $ec2Client->waitUntil('BucketExists', array('Bucket' => $domain));

            $result = $ec2Client->putObject(array(
             'ACL' => 'public-read',
             'Bucket' => $domain,
             'Key'    => 'index.html',
             'Body'   => "<html><head><title>Bienvenue sur le site  ".$website_name." : ".$domain."</title>
             <meta name='description' content='".substr($bio,0,110)."'/>
             <meta name='keywords' content='".$website_name."'>
             <meta property='og:title' content='".$website_name."'/>
             <meta property='og:type' content='website'/><meta property='og:url' content='".$domain."'/>
             <meta property='og:image' content='".$image_url."'/><meta name='twitter:card' content='summary' />
             <meta name='twitter:site' content='@".$website_name."'/>
             <meta name='twitter:title' content='".$website_name."' />
             <meta name='twitter:description' content='".$bio."' />
             <meta name='twitter:image' content='".$image_url."' />
             <link rel='shortcut icon' type='image/png' href=".$favicon_url."'/>
             <link rel='shortcut icon' id='favicon' type='image/x-icon' href='".$favicon_url."'>
             </head>
             <body style='margin:0px;padding:0px;overflow:hidden'>
             <div style='height:0px'>
             <a  style='color:white' href='/presentation.html'>pres</a>
             <a style='color:white' href='/images.html'>images</a>
             <a style='color:white' href='/video.html'>images</a>
             <h1 style='color:white'>".$website_name."</h1>
             <h2 style='color:white'>".$website_name."</h2>
             </div>
             <div style='overflow:auto;-webkit-overflow-scrolling:touch'>
             <iframe src='".$website_url."' width='100%'' height='100%'>
             <p>Your browser does not support iframes.</p></iframe></div></body></html>",
             'ContentType' => 'text/html'
            ));
            $result = $ec2Client->putObject(array(
                         'ACL' => 'public-read',
                         'Bucket' => $domain,
                         'Key'    => 'error.html',
                         'Body'   => "<html><head><title>".$website_name."</title>
                         <meta name='description' content='une erreur est survenue'/>
                         <meta charset='UTF-8'></head>
                         <body>Une erreur est survenue sur le site ".$website_name.", nous tentons de résoudre ce problème.
                         </body></html>",
                         'ContentType' => 'text/html'
                        ));

            $result = $ec2Client->putObject(array(
                         'ACL' => 'public-read',
                         'Bucket' => $domain,
                         'Key'    => 'sitemap.xml',
                         'Body'   => '<?xml version="1.0" encoding="UTF-8"?>
                            <urlset
                                  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                                        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
                            <!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->

                            <url>
                              <loc>http://'.$website->getDomain().'</loc>
                              <lastmod>2017-07-23T16:29:26+00:00</lastmod>
                            </url>
                            <url>
                              <loc>http://'.$website->getDomain().'/presentation.html</loc>
                              <lastmod>2017-07-23T16:29:27+00:00</lastmod>
                            </url>
                            <url>
                              <loc>http://'.$website->getDomain().'/index.html</loc>
                              <lastmod>2017-07-23T16:29:26+00:00</lastmod>
                            </url>
                            <url>
                              <loc>http://'.$website->getDomain().'/image.html</loc>
                              <lastmod>2017-07-23T16:29:26+00:00</lastmod>
                            </url>
                            </urlset>',
                         'ContentType' => 'text/xml'
                        ));

             $result = $ec2Client->putObject(array(
             'ACL' => 'public-read',
             'Bucket' => $domain,
             'Key'    => 'presentation.html',
             'Body'   => "<html><head><title>".$website_name."</title>
             <meta name='description' content='Présentation du site / bio / description, pour retrouver l'intégralité du site cliquez sur index: ".$website_name."'/>
             <meta name='keywords' content='".$website_name."'>
             <meta property='og:title' content='".$website_name."'/>
             <meta property='og:type' content='website'/><meta property='og:url' content='".$domain."'/>
             <meta property='og:image' content='".$image_url."'/><meta name='twitter:card' content='summary' />
             <meta name='twitter:site' content='@".$website_name."'/>
             <meta name='twitter:title' content='".$website_name."' />
             <meta name='twitter:description' content='".$bio."' />
             <meta name='twitter:image' content='".$image_url."'/>
             </head>
             <body style='margin:0px;padding:0px;overflow:hidden'>
             <h1>".$website_name."</h1>
             <h2>".$website_name."</h2>
             <a href='/index.html'>home</a>
             <p>".$bio."</p></body></html>",
             'ContentType' => 'text/html'
            ));

             $result = $ec2Client->putObject(array(
             'ACL' => 'public-read',
             'Bucket' => $domain,
             'Key'    => 'images.html',
             'Body'   => "<html><head><title>".$website_name."</title>
             <meta name='description' content='Retrouvez l intégratlité des images de ce site en cliquant sur index : ".$website_name."'/>
             <meta name='keywords' content='".$website_name."'>
             <meta property='og:title' content='".$website_name."'/>
             <meta property='og:type' content='website'/><meta property='og:url' content='".$domain."'/>
             <meta property='og:image' content='".$image_url."'/><meta name='twitter:card' content='summary' />
             <meta name='twitter:site' content='@".$website_name."'/>
             <meta name='twitter:title' content='".$website_name."' />
             <meta name='twitter:description' content='Retrouvez l'intégratlité des images de ce site en cliquant sur index : ".$website_name."' />
             <meta name='twitter:image' content='".$image_url."'/>
             </head>
             <body style='margin:0px;padding:0px;overflow:hidden'>
             <h1>".$website_name."</h1>
             <h2>".$website_name."</h2>
             <a href='/index.html'>home</a>
             <img src=".$image_url." /></body></html>",
             'ContentType' => 'text/html'
            ));

                $result = $ec2Client->putObject(array(
             'ACL' => 'public-read',
             'Bucket' => $domain,
             'Key'    => 'videos.html',
             'Body'   => "<html><head><title>".$website_name."</title>
             <meta name='description' content='Retrouvez toutes les video  de ce site en cliquant sur index : ".$website_name."'/>
             <meta name='keywords' content='".$website_name."'>
             <meta property='og:title' content='".$website_name."'/>
             <meta property='og:type' content='website'/><meta property='og:url' content='".$domain."'/>
             <meta property='og:image' content='".$image_url."'/><meta name='twitter:card' content='summary' />
             <meta name='twitter:site' content='@".$website_name."'/>
             <meta name='twitter:title' content='".$website_name."' />
             <meta name='twitter:description' content='Retrouvez l'intégratlité des images de ce site en cliquant sur index : ".$website_name."' />
             <meta name='twitter:image' content='".$image_url."'/>
             </head>
             <body style='margin:0px;padding:0px;overflow:hidden'>
             <h1>".$website_name.' : toutes les videos'."</h1>
             <h2>".$website_name."</h2>
             <a href='/index.html'>home</a>
             <img src=".$image_url." /></body></html>",
             'ContentType' => 'text/html'
            ));

            try {
            $ec2Client->putBucketWebsite(array(
                'Bucket'        => $domain,  
                'IndexDocument' => array('Suffix' => 'index.html'),
                'ErrorDocument' => array('Key' => 'error.html'),
            ));
            } catch(Exception $e){
                $html = array('success' => "error");
            }
            $html = array('success' => 1);
        } catch (Exception $e){
            $html = array('success' => "error");
        }
        $this->_sendHtml(json_encode($html));
    }

    public function getnewslettermembersAction() {
        if($offset = $this->getRequest()->getParam('offset')) {
            $offset = $offset - 1 ;
            $i = 0; 
            $application = $this->getApplication();
            $members = $application->getNewsletterMembers($offset);
            $members_array = array();
            foreach ($members as $key => $member) {
                if($member->getContactId()){
                    $members_array[$i]["id"] = $member->getContactId();
                    $members_array[$i]["mail"] = $member->getEmail();
                    $members_array[$i]["name"] = $member->getFirstname()." ".$member->getName();
                    $i++;
                }
            }
            $this->_sendHtml(array("success"=>1,"members"=>$members_array));
       }
    }

}
?>