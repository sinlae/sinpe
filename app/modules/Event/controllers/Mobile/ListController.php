<?php
class Event_Mobile_ListController extends Application_Controller_Mobile_Default {

    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {
            try {

                $option = $this->getCurrentOptionValue();

                $start_at = new Zend_Date();
                $end_at = new Zend_Date();
                $format = 'y-MM-dd HH:mm:ss';
                $offset = $this->getRequest()->getParam('offset', 0);
                $events = $option->getObject()->getEvents($offset);
                $data = array('collection' => array());

                $locker_option = $this->getApplication()->getOption("padlock");
                $locker = new Padlock_Model_Padlock();
                $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

                if(in_array($value_id, $lock_values_id))
                {
                   $data['details']['locked'] = 1;
                } else {
                   $data['details']['locked'] = 2;
                }

                $data['details']['length'] = count($events);

                $month = array();
                $i = 0;
                foreach($events as $key => $event) {
                    $start_at->set($event->getStartAt(), $format);
                    $end_at->set($event->getEndAt(), $format);
                    $formatted_start_at = $start_at->toString($this->_("MM.dd.y"));

                    $subtitle2 = $this->_("Entrance: %s", $event->getStartTimeAt());
                    if($event->getLocationLabel()) {
                        $subtitle2 .= " | ".$this->_("Location: %s", $event->getLocationLabel());
                    }

                    if ($i == 0)
                    {
                        $month[$i]['print'] = $start_at->toString(Zend_Date::MONTH_NAME_SHORT);
                        $month[$i]['real'] = $start_at->toString(Zend_Date::MONTH_NAME_SHORT);
                    } elseif($start_at->toString(Zend_Date::MONTH_NAME_SHORT) != $month[$i-1]['real'])
                    {
                        $month[$i]['print'] = $start_at->toString(Zend_Date::MONTH_NAME_SHORT);
                        $month[$i]['real'] = $start_at->toString(Zend_Date::MONTH_NAME_SHORT);
                    } else {
                       $month[$i]['print'] = NULL;
                       $month[$i]['real'] = $start_at->toString(Zend_Date::MONTH_NAME_SHORT);
                    }  

                    $data['collection'][] = array(
                        "id" => $key,
                        "title" => $event->getName(),
                        "title2" => "{$start_at->toString(Zend_Date::WEEKDAY_NAME)} {$formatted_start_at}",
                        "name" => $event->getName(),
                        "subtitle" => $event->getSubtitle(),
                        "subtitle2" => $subtitle2,
                        "description" => $event->getDescription(),
                        "month_name_short" => $start_at->toString(Zend_Date::MONTH_NAME_SHORT),
                        "month" => $month[$i]['print'],
                        "day" => $start_at->toString('dd'),
                        "year" => $start_at->toString('yyyy'),
                        "time" => $start_at->toString('HH:mm'),
                        "weekday_name" => $start_at->toString(Zend_Date::WEEKDAY_NAME),
                        "start_time_at" => $event->getStartTimeAt(),
                        "address" => $event->getAddress(),
                        "location" => $event->getLocation(),
                        "picture" => $event->getPicture(),
                        "shop_url" => $event->getTicketShopUrl(),
                        "rsvp_url" => $event->getRsvp(),
                        "url" => $this->getPath("event/mobile_view/index", array('value_id' => $option->getId(), "event_id" => $key))
                    );
                    $i++;
                }

                $data['details']['nopost'] = $this->_("No event for the moment");
                $data['page_title'] = $option->getTabbarName();

            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($data);

        }

    }


    public function findeventsoptionsvaluesAction() 
    {
        try  
        {
            $option = new Application_Model_Option();
            $option->find('calendar', 'code');
            $option_value = new Application_Model_Option_Value();
            $option_value = $option_value->findFolderValues($this->getApplication()->getId(),$option->getId());
            $data = array();


            $locker_option = $this->getApplication()->getOption("padlock");
            $locker = new Padlock_Model_Padlock();
            $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

            foreach ($option_value as $key => $value) {
                if(!in_array($value->getId(), $lock_values_id))
                {
                    $data[] = array(
                    "id" => $value->getId(),
                    "name" => $value->getTabbarName() ? $value->getTabbarName() : $this->_('Events')
                    );
                }
            }

        }  catch (Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage()); 
        }

        $this->_sendHtml($data);
    }

}