<?php

class Event_Model_Event_Custom extends Core_Model_Default {

    protected $_agenda;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Event_Model_Db_Table_Event_Custom';
        return $this;
    }

    public function getAgenda() {

        if(!$this->_agenda) {
            $this->_agenda = new Event_Model_Event();
            $this->_agenda->find($this->getAgendaId());
        }

        return $this->_agenda;
    }

    public function getPictureUrl() {
        if($this->getData('picture')) {
            $path_picture = Application_Model_Application::getImagePath().$this->getData('picture');
            $base_path_picture = Application_Model_Application::getBaseImagePath().$this->getData('picture');
            if (strpos($this->getData('picture'), "cloudfront") !== false)
            {
                return $this->getPicture();
            } elseif(file_exists($base_path_picture)) {
                return $path_picture;
            }
        }
        return null;
    }

    public function getWebsites() {
        $websites = array();
        if($this->getData("websites")) {
            try {
                $websites = Zend_Json::decode($this->getData("websites"));
            } catch(Exception $e) {
                $websites = array();
            }
        }
        return $websites;
    }

    public function getPrices(){
        $price = new Event_Model_Event_Price();
        $prices = $price->findAll(array("event_id"=>$this->getEventId()));
        return $prices;
    }

     public function getPricesArray(){
        $price = new Event_Model_Event_Price();
        $prices = $price->findAll(array("event_id"=>$this->getEventId()));
        $prices_array = array();
        $i=0;
        foreach ($prices as $key => $price) {
            $prices_array[$i]['price'] = $price->getPrice();
            $prices_array[$i]['title'] = $price->getTitle();
            $i++;
        }
        return $prices_array;
    }

}
