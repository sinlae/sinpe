<?php

class Weblink_Mobile_MultiController extends Application_Controller_Mobile_Default {

    public function indexAction() {
        $this->forward('index', 'index', 'Front', $this->getRequest()->getParams());
    }

    public function templateAction() {
        $this->loadPartials($this->getFullActionName('_').'_l'.$this->_layout_id, false);
    }

    public function findAction() {

        $option = $this->getCurrentOptionValue();
        $weblink = $option->getObject();
        $data = array();

        $data["weblink"] = array(
            "cover_url" => $weblink->getCover() ? $weblink->getCoverUrl() : null,
            "alphabetic_order" => $weblink->getAlphabeticOrder(),
            "links" => array()
        );
        $value_id = $this->getRequest()->getParam('value_id');
        $locker_option = $this->getApplication()->getOption("padlock");
        $locker = new Padlock_Model_Padlock();
        $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

        if(in_array($value_id, $lock_values_id))
        {
            $data["weblink"]["locked"] = 1;
        } else {
               $data["weblink"]["locked"] = 2;
        }


        $links = new Weblink_Model_Weblink_Link();
        if($weblink->getAlphabeticOrder() == 1) {
            $links = $links->findAll(array("weblink_id" => $weblink->getWeblinkId()),"title ASC");
        } else {
            $links = $links->findAll(array("weblink_id" => $weblink->getWeblinkId()));
        }
        $index = array();
        $i = 0;
        $first_index = false; 
        foreach($links as $link) {
            $target = "_blank";
            if(strpos($link->getUrl(),$this->getApplication()->getUrl()) === false)
            {
                $target = true;
                $url = $link->getUrl();
            } else{
               $target = false;
               $url = explode($this->getApplication()->getKey(), $link->getUrl());
               $url = "/".$this->getApplication()->getKey().$url[1];
            } 
            $index[$i] = strtolower($link->getTitle()[0]);
            if($i > 0) {
                if($index[$i] != $index[$i-1]){
                   $first_index = true; 
               } else {
                    $first_index = false; 
               }

            } else {
                $first_index = true;
            }
            $data["weblink"]["links"][] = array(
                "id" => $link->getId(),
                "title" => $link->getTitle(),
                "picto_url" => $link->getPictoUrl() ? $link->getPictoUrl() : Media_Model_Library_Image::getImagePathTo('weblink/default_link.jpg'),
                "url" => $url,
                "target" => $target,
                "first_index" => $first_index,
                "index" => $index[$i]
            );
            $i++;
        }

        $letter = 'A';
        $data["letters"] = array();
        for($i=0;$i<26;$i++){
            $data["letters"][$i]["value"] = $letter;
            $data["letters"][$i]["index"] = strtolower($letter);
            $letter++;
        }

        $data['page_title'] = $option->getTabbarName();

        $this->_sendHtml($data);

    }

}