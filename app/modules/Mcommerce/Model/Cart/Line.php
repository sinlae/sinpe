<?php

class Mcommerce_Model_Cart_Line extends Core_Model_Default {

    /**
     * @var Catalog_Model_Product Produit du catalogue
     */
    protected $_product;

    /**
     * @var array Options de la ligne
     */
    protected $_options;

    /**
     * @var array Format de la ligne
     */
    protected $_format;

    /**
     * @var Mcommerce_Model_Cart Panier
     */
    protected $_cart;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Mcommerce_Model_Db_Table_Cart_Line';
        return $this;
    }

    public function getQty() {
        $qty = 0;
        if($this->getData('qty')) {
            $qty = round($this->getData('qty'));
        }

        return $qty;
    }

    public function isRecurrent() {
        return false;
    }

//    public function setQty($qty) {
//        $this->setData('qty', $qty);
//        $total_price = $this->getPrice() * $qty;
//        foreach($this->getOptions() as $option) {
//
//        }
//    }

    public function calcTotal() {

        $price = $this->getBasePrice();

        foreach($this->getOptions() as $option) {
            $price += $option->getPrice();
        }

        $price = round($price, 2);
        $priceInclTax = round($price * (1 + $this->getTaxRate() / 100), 2);
        
        $total = round($price, 2);
        $totalInclTax = round($total * (1 + $this->getTaxRate() / 100), 2);
        
        $this->setPrice($price)
            ->setPriceInclTax($priceInclTax)
            ->setTotal($total)
            ->setTotalInclTax($totalInclTax)
        ;
        
        return $this;
    }

    public function getProduct() {

        if(!$this->_product) {

            $this->_product = new Catalog_Model_Product();
            if($this->getProductId()) {
                $this->_product->find($this->getProductId());
            }
        }

        return $this->_product;
    }

    public function getCart() {

        if(!$this->_cart) {

            $this->_cart = new Mcommerce_Model_Cart();
            if($this->getCartId()) {
                $this->_cart->find($this->getCartId());
            }
        }

        return $this->_cart;
    }

    public function getOptions() {

        if(!$this->_options) {
            $this->_options = array();
            if($this->getData('options')) {
                $options = @unserialize($this->getData('options'));
                if(is_array($options)) {
                    foreach($options as $option_datas) {
                        $this->_options[] = new Core_Model_Default($option_datas);
                    }
                }
            }
        }

        return $this->_options;
    }

    public function getFormat() {

        if(!$this->_format AND $this->getData('format')) {
            $format = @unserialize($this->getData('format'));
            $this->_format = new Core_Model_Default($format);
        }

        return $this->_format;
    }
}
