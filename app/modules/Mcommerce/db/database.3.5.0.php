<?php

$this->query("ALTER TABLE `mcommerce` ADD `show_search` TINYINT(1) NOT NULL DEFAULT '0' AFTER `description`;");
$this->query("ALTER TABLE `catalog_product` ADD `library_id` INT(11) UNSIGNED NOT NULL AFTER `tax_id`, ADD INDEX (`library_id`) ;");
$this->query("ALTER TABLE `mcommerce_cart_line` ADD `format` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `options`;");

$product = new Catalog_Model_Product();
$products = $product->findAll(array("mcommerce_only" => new Zend_Db_Expr("mcommerce_id IS NOT NULL"), "library_id" => 0));

foreach($products as $product) {

    if(!$product->getPicture()) continue;

    $library = new Media_Model_Library();
    $library_name = "product_".$product->getProductId();
    $library->setName($library_name)->save();
    
    $data = array('library_id' => $library->getId(), 'link' => $product->getPicture(), 'can_be_colorized' => 0);
    $image = new Media_Model_Library_Image();
    $image->setData($data)->save();
    
    $product->setPicture(null)
        ->setLibraryId($library->getId())
        ->save()
    ;

}
