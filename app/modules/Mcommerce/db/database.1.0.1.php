<?php

$this->query("
    ALTER TABLE `mcommerce_cart`
        CHANGE `subtotal_excl_tax` `subtotal_excl_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `delivery_cost` `delivery_cost` DECIMAL(12,4) unsigned NOT NULL,
        CHANGE `total_excl_tax` `total_excl_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `total_tax` `total_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `total` `total` DECIMAL(12,4) NOT NULL
    ;
");

$this->query("
    ALTER TABLE `mcommerce_cart_line`
        CHANGE `base_price` `base_price` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `base_price_incl_tax` `base_price_incl_tax` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `price` `price` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `price_incl_tax` `price_incl_tax` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `total` `total` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `total_incl_tax` `total_incl_tax` DECIMAL(12,4) DEFAULT NULL
    ;
");

$this->query("
    ALTER TABLE `mcommerce_order`
        CHANGE `subtotal_excl_tax` `subtotal_excl_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `total_excl_tax` `total_excl_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `total_tax` `total_tax` DECIMAL(12,4) NOT NULL,
        CHANGE `delivery_cost` `delivery_cost` DECIMAL(12,4) unsigned NOT NULL,
        CHANGE `total` `total` DECIMAL(12,4) NOT NULL
    ;
");

$this->query("
    ALTER TABLE `mcommerce_order_line`
        CHANGE `base_price` `base_price` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `base_price_incl_tax` `base_price_incl_tax` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `price` `price` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `price_incl_tax` `price_incl_tax` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `total` `total` DECIMAL(12,4) DEFAULT NULL,
        CHANGE `total_incl_tax` `total_incl_tax` DECIMAL(12,4) DEFAULT NULL
    ;
");
