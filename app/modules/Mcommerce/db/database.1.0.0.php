<?php

$now = Zend_Date::now()->toString('y-MM-dd HH:mm:ss');

$this->query("
    CREATE TABLE `mcommerce` (
        `mcommerce_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `value_id` int(11) unsigned NOT NULL,
        `catalog_value_id` int(11) unsigned NOT NULL,
        `root_category_id` int(11) unsigned NOT NULL,
        `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`mcommerce_id`),
        KEY `value_id` (`value_id`),
        KEY `KEY_VALUE_ID` (`catalog_value_id`),
        KEY `KEY_CATEGORY_ID` (`root_category_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_cart` (
        `cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `mcommerce_id` int(11) unsigned NOT NULL,
        `delivery_method_id` int(11) unsigned DEFAULT NULL,
        `payment_method_id` int(11) unsigned DEFAULT NULL,
        `customer_id` int(11) DEFAULT NULL,
        `customer_firstname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_lastname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_birthday` date DEFAULT NULL,
        `customer_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_postcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_latitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_longitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        `store_id` int(11) unsigned NOT NULL,
        `subtotal_excl_tax` decimal(8,4) NOT NULL,
        `delivery_cost` decimal(8,4) unsigned NOT NULL,
        `delivery_tax_rate` decimal(5,2) DEFAULT NULL,
        `total_excl_tax` decimal(8,4) NOT NULL,
        `total_tax` decimal(8,4) NOT NULL,
        `total` decimal(8,4) NOT NULL,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`cart_id`),
        KEY `KEY_MCOMMERCE_ID` (`mcommerce_id`),
        KEY `KEY_STORE_ID` (`store_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_cart_line` (
        `line_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `cart_id` int(11) unsigned NOT NULL,
        `product_id` int(11) NOT NULL,
        `ref` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `base_price` decimal(8,4) DEFAULT NULL,
        `base_price_incl_tax` decimal(8,4) DEFAULT NULL,
        `price` decimal(8,4) DEFAULT NULL,
        `price_incl_tax` decimal(8,4) DEFAULT NULL,
        `qty` decimal(4,2) NOT NULL DEFAULT '1.00',
        `total` decimal(8,4) DEFAULT NULL,
        `total_incl_tax` decimal(8,4) DEFAULT NULL,
        `options` text COLLATE utf8_unicode_ci,
        `tax_id` int(11) unsigned NOT NULL,
        `tax_rate` decimal(5,2) NOT NULL,
        PRIMARY KEY (`line_id`),
        KEY `KEY_CART_ID` (`cart_id`),
        KEY `KEY_TAX_ID` (`tax_id`),
        KEY `KEY_PRODUCT_ID` (`product_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_delivery_method` (
        `method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
        `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0',
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`method_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_order` (
        `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `mcommerce_id` int(11) unsigned NOT NULL,
        `cart_id` int(11) unsigned NOT NULL,
        `store_id` int(11) unsigned NOT NULL,
        `customer_id` int(11) DEFAULT NULL,
        `number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
        `status_id` tinyint(1) NOT NULL DEFAULT '1',
        `payment_method_id` int(11) unsigned NOT NULL,
        `payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `delivery_method_id` int(11) unsigned NOT NULL,
        `delivery_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `customer_firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
        `customer_lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
        `customer_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_postcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `customer_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
        `subtotal_excl_tax` decimal(8,4) NOT NULL,
        `total_excl_tax` decimal(8,4) NOT NULL,
        `total_tax` decimal(8,4) NOT NULL,
        `delivery_cost` decimal(8,4) unsigned NOT NULL,
        `total` decimal(8,4) NOT NULL,
        `created_at` datetime NOT NULL,
        `paid_at` datetime DEFAULT NULL,
        PRIMARY KEY (`order_id`),
        KEY `KEY_MCOMMERCE_ID` (`mcommerce_id`),
        KEY `KEY_STORE_ID` (`store_id`),
        KEY `KEY_CART_ID` (`cart_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_order_line` (
        `line_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `order_id` int(11) unsigned NOT NULL,
        `cart_line_id` int(11) unsigned NOT NULL,
        `ref` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
        `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `base_price` decimal(8,4) DEFAULT NULL,
        `base_price_incl_tax` decimal(8,4) DEFAULT NULL,
        `price` decimal(8,4) DEFAULT NULL,
        `price_incl_tax` decimal(8,4) DEFAULT NULL,
        `qty` decimal(4,2) NOT NULL DEFAULT '1.00',
        `total` decimal(8,4) DEFAULT NULL,
        `total_incl_tax` decimal(8,4) DEFAULT NULL,
        `options` text COLLATE utf8_unicode_ci,
        `tax_id` int(11) unsigned NOT NULL,
        `tax_rate` decimal(5,2) NOT NULL,
        PRIMARY KEY (`line_id`),
        KEY `KEY_ORDER_ID` (`order_id`),
        KEY `KEY_TAX_ID` (`tax_id`),
        KEY `KEY_LINE_ID` (`cart_line_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_payment_method` (
        `method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
        `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `online_payment` tinyint(1) NOT NULL DEFAULT '0',
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`method_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_store` (
        `store_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `mcommerce_id` int(11) unsigned NOT NULL,
        `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
        `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `postcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
        `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
        `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
        `latitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        `longitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
        `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
        `delivery_fees` decimal(12,4) NOT NULL DEFAULT '0.0000',
        `min_amount` decimal(12,4) NOT NULL DEFAULT '0.0000',
        `min_amount_free_delivery` decimal(12,4) NOT NULL DEFAULT '0.0000',
        `delivery_area` decimal(8,4) DEFAULT NULL,
        `delivery_time` decimal(4,2) DEFAULT NULL,
        `opening_hours` text COLLATE utf8_unicode_ci,
        `is_visible` tinyint(1) NOT NULL DEFAULT '1',
        `currency_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`store_id`),
        KEY `KEY_MCOMMERCE_ID` (`mcommerce_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_store_delivery_method` (
        `store_delivery_method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `store_id` int(11) unsigned NOT NULL,
        `method_id` int(11) unsigned NOT NULL,
        `tax_id` int(11) unsigned DEFAULT NULL,
        `price` decimal(12,4) DEFAULT NULL,
        `min_amount_for_free_delivery` decimal(12,4) DEFAULT NULL,
        PRIMARY KEY (`store_delivery_method_id`),
        KEY `KEY_STORE_ID` (`store_id`),
        KEY `KEY_METHOD_ID` (`method_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_store_payment_method` (
        `store_payment_method_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `store_id` int(11) unsigned NOT NULL,
        `method_id` int(11) unsigned NOT NULL,
        PRIMARY KEY (`store_payment_method_id`),
        KEY `KEY_STORE_ID` (`store_id`),
        KEY `KEY_METHOD_ID` (`method_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_store_payment_method_paypal` (
        `paypal_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `store_id` int(11) unsigned NOT NULL,
        `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `signature` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`paypal_id`),
        KEY `KEY_STORE_ID` (`store_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_store_printer` (
        `printer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `store_id` int(11) unsigned NOT NULL,
        `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`printer_id`),
        KEY `KEY_STORE_ID` (`store_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `mcommerce_store_tax` (
        `store_tax_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `store_id` int(11) unsigned NOT NULL,
        `tax_id` int(11) unsigned NOT NULL,
        `rate` decimal(5,2) NOT NULL,
        PRIMARY KEY (`store_tax_id`),
        KEY `KEY_STORE_ID` (`store_id`),
        KEY `KEY_TAX_ID` (`tax_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

    CREATE TABLE `mcommerce_tax` (
        `tax_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `mcommerce_id` int(11) unsigned NOT NULL,
        `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
        `rate` decimal(5,2) NOT NULL,
        `type` tinyint(1) NOT NULL DEFAULT '1',
        `created_at` datetime NOT NULL,
        `updated_at` datetime NOT NULL,
        PRIMARY KEY (`tax_id`),
        KEY `KEY_MCOMMERCE_ID` (`mcommerce_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `catalog_product_group` (
      `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `is_required` tinyint(1) NOT NULL DEFAULT '0',
      `created_at` datetime NOT NULL,
      `updated_at` datetime NOT NULL,
      PRIMARY KEY (`group_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

    CREATE TABLE `catalog_product_group_option` (
      `option_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `group_id` int(11) unsigned NOT NULL,
      `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(2550) COLLATE utf8_unicode_ci DEFAULT NULL,
      `created_at` datetime NOT NULL,
      `updated_at` datetime NOT NULL,
      PRIMARY KEY (`option_id`),
      KEY `FK_CATALOG_PRODUCT_GROUP_GROUP_ID` (`group_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;
");

$this->query("
    CREATE TABLE `catalog_product_group_option_value` (
      `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `group_value_id` int(11) unsigned NOT NULL,
      `option_id` int(11) unsigned NOT NULL,
      `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
      `qty_min` smallint(5) NOT NULL DEFAULT '0',
      `qty_max` smallint(5) DEFAULT NULL,
      PRIMARY KEY (`value_id`),
      KEY `FK_CATALOG_PRODUCT_GROUP_OPTION_OPTION_ID` (`option_id`),
      KEY `FK_CATALOG_PRODUCT_GROUP_GROUP_ID` (`group_value_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

    CREATE TABLE `catalog_product_group_value` (
      `value_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `product_id` int(11) NOT NULL,
      `group_id` int(11) unsigned NOT NULL,
      PRIMARY KEY (`value_id`),
      KEY `FK_CATALOG_PRODUCT_PRODUCT_ID` (`product_id`),
      KEY `FK_CATALOG_PRODUCT_GROUP_GROUP_ID` (`group_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;
");

$this->query("
    CREATE TABLE `catalog_product_folder_category` (
      `product_id` int(11) NOT NULL,
      `category_id` int(11) unsigned NOT NULL,
      PRIMARY KEY (`category_id`,`product_id`),
      KEY `FK_CATALOG_PRODUCT_PRODUCT_ID` (`product_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");


$this->query("
    ALTER TABLE `catalog_product`
        ADD `mcommerce_id` int(11) unsigned DEFAULT NULL AFTER `value_id`;
");
$this->query("
    ALTER TABLE `catalog_product_folder_category`
        ADD FOREIGN KEY `FK_CATEGORY_ID` (`category_id`) REFERENCES `folder_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `catalog_product_group_option`
        ADD FOREIGN KEY `FK_MCOMMERCE_ID` (`group_id`) REFERENCES `catalog_product_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `catalog_product_group_option_value`
        ADD FOREIGN KEY `FK_OPTION_ID` (`option_id`) REFERENCES `catalog_product_group_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_GROUP_OPTION_ID` (`group_value_id`) REFERENCES `catalog_product_group_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `catalog_product_group_value`
        ADD FOREIGN KEY `FK_PRODUCT_ID` (`product_id`) REFERENCES `catalog_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_GROUP_ID` (`group_id`) REFERENCES `catalog_product_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `catalog_product_folder_category`
        ADD FOREIGN KEY `FK_PRODUCT_ID` (`product_id`) REFERENCES `catalog_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `catalog_product`
        ADD KEY `KEY_MCOMMERCE_ID` (`mcommerce_id`);
");

$this->query("
    ALTER TABLE `mcommerce`
        ADD FOREIGN KEY `FK_CATALOG_VALUE_ID` (`catalog_value_id`) REFERENCES `application_option_value` (`value_id`) ON UPDATE CASCADE ON DELETE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_cart`
        ADD FOREIGN KEY `FK_MCOMMERCE_ID` (`mcommerce_id`) REFERENCES `mcommerce` (`mcommerce_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `mcommerce_cart_line`
        ADD FOREIGN KEY `FK_CART_ID` (`cart_id`) REFERENCES `mcommerce_cart` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_TAX_ID` (`tax_id`) REFERENCES `mcommerce_tax` (`tax_id`) ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_PRODUCT_ID` (`product_id`) REFERENCES `catalog_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_order`
        ADD FOREIGN KEY `FK_MCOMMERCE_ID` (`mcommerce_id`) REFERENCES `mcommerce` (`mcommerce_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_CART_ID` (`cart_id`) REFERENCES `mcommerce_cart` (`cart_id`) ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `mcommerce_order_line`
        ADD FOREIGN KEY `FK_ORDER_ID` (`order_id`) REFERENCES `mcommerce_order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_TAX_ID` (`tax_id`) REFERENCES `mcommerce_tax` (`tax_id`) ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_CART_LINE_ID` (`cart_line_id`) REFERENCES `mcommerce_cart_line` (`line_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_store`
        ADD FOREIGN KEY `FK_MCOMMERCE_ID` (`mcommerce_id`) REFERENCES `mcommerce` (`mcommerce_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `mcommerce_store_delivery_method`
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_METHOD_ID` (`method_id`) REFERENCES `mcommerce_delivery_method` (`method_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_store_payment_method`
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_METHOD_ID` (`method_id`) REFERENCES `mcommerce_payment_method` (`method_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `mcommerce_store_payment_method_paypal`
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_store_printer`
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `mcommerce_store_tax`
        ADD FOREIGN KEY `FK_STORE_ID` (`store_id`) REFERENCES `mcommerce_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_TAX_ID` (`tax_id`) REFERENCES `mcommerce_tax` (`tax_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
$this->query("
    ALTER TABLE `mcommerce_tax`
        ADD FOREIGN KEY `FK_MCOMMERCE_ID` (`mcommerce_id`) REFERENCES `mcommerce` (`mcommerce_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");



$catalog_option = new Application_Model_Option();
$catalog_option->find('catalog', 'code');

$datas = array(
    'library_id' => $catalog_option->getLibraryId(),
    'icon_id' => $catalog_option->getIconId(),
    'code' => 'm_commerce',
    'name' => 'Commerce',
    'model' => 'Mcommerce_Model_Mcommerce',
    'desktop_uri' => 'mcommerce/application/',
    'mobile_uri' => 'mcommerce/mobile_category/',
    'only_once' => 1,
    'is_ajax' => 1,
    'position' => 220
);
$option = new Application_Model_Option();
$option->setData($datas)->save();


$datas = array(
    array('code' => 'in_store', 'name' => 'In store', 'is_free' => 1),
    array('code' => 'carry_out', 'name' => 'Carry Out', 'is_free' => 1),
    array('code' => 'home_delivery', 'name' => 'Delivery', 'is_free' => 0)
);
foreach($datas as $data) {
    $method = new Mcommerce_Model_Delivery_Method();
    $method->setData($data)->save();
}

$datas = array(
    array('code' => 'paypal', 'name' => 'Paypal', 'online_payment' => 1),
    array('code' => 'cash', 'name' => 'Cash', 'online_payment' => 0),
    array('code' => 'check', 'name' => 'Check', 'online_payment' => 0),
    array('code' => 'meal_voucher', 'name' => 'Meal Voucher', 'online_payment' => 0),
    array('code' => 'cc_upon_delivery', 'name' => 'Credit card (pay upon pickup or delivery)', 'online_payment' => 0),
);
foreach($datas as $data) {
    $method = new Mcommerce_Model_Payment_Method();
    $method->setData($data)->save();
}
