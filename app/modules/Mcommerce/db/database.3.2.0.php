<?php

$this->query("
    ALTER TABLE `mcommerce_tax` CHANGE `rate` `rate` DECIMAL(5,3) NOT NULL;
");