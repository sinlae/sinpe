<?php

$this->query("ALTER TABLE `mcommerce_store` ADD `clients_calculate_change` TINYINT(1) NULL DEFAULT NULL AFTER `min_amount_free_delivery`;");
$this->query("ALTER TABLE `mcommerce_cart` ADD `paid_amount` DECIMAL(12,4) NULL DEFAULT NULL AFTER `total`;");
$this->query("ALTER TABLE `mcommerce_order` ADD `paid_amount` DECIMAL(12,4) NULL DEFAULT NULL AFTER `total`;");