<?php

class Mcommerce_Mobile_CategoryController extends Mcommerce_Controller_Mobile_Default {
  
    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try {

                $category_id = $this->getRequest()->getParam('category_id');
                $current_category = new Folder_Model_Category();

                $offset = $this->getRequest()->getParam('offset', 0);

                if($category_id) {
                    $current_category->find($category_id, 'category_id');
                }

                $object = $this->getCurrentOptionValue()->getObject();

                if(!$object->getId() OR ($current_category->getId() AND $current_category->getRootCategoryId() != $object->getRootCategoryId())) {
                    throw new Exception($this->_('An error occurred during process. Please try again later.'));
                }

                if(!$current_category->getId()) {
                    $current_category = $object->getRootCategory();
                }

                $data = array("collection" => array());
                $locker_option = $this->getApplication()->getOption("padlock");
                $locker = new Padlock_Model_Padlock();
                $lock_values_id = $locker->getValueIds($this->getApplication()->getId());

                if(in_array($value_id, $lock_values_id))
                {
                    $data['collection']['locked'] = 1;
                } else {
                    $data['collection']['locked'] = 2;
                }
                
                $subcategories = $current_category->getChildren($offset);

                foreach($subcategories as $subcategory) {
                    $data["collection"][] = array(
                        "title" => $subcategory->getTitle(),
                        "subtitle" => $subcategory->getSubtitle(),
                        "picture" => $subcategory->getPictureUrl() ? $subcategory->getPictureUrl() : $this->getApplication()->getImagePath().'/placeholder/no-image.png',
                        "url" => $this->getPath("mcommerce/mobile_category", array("value_id" => $value_id, "category_id" => $subcategory->getId()))
                    );
                }

                $products = $current_category->getProducts($offset);

                if ($this->getRequest()->getParam("category_id"))
                {
                  $data['collection']['length'] = count($products);
                } else {
                    $data['collection']['length'] = count($subcategories);
                }

                $color = $this->getApplication()->getBlock('background')->getImageColor();
                
                $current_store = $this->getStore();

                foreach($products as $product) {
                    
                    $taxRate = $current_store->getTax($product->getTaxId())->getRate();

                    $picture = null;
                    if($image = $product->getLibraryPictures(false)) {
                        $picture = $image["url"];
                    }
                    $data["collection"][] = array(
                        "title" => $product->getName(),
                        "subtitle" => $product->getPrice() > 0 ? $product->formatPrice($product->getPrice() * (1 + $taxRate / 100)) : strip_tags($product->getDescription()),
                        "picture" => $picture,
                        "url" => $product->getPath("mcommerce/mobile_product", array("value_id" => $value_id, "product_id" => $product->getId()))
                    );
                }

                $mcommerce = new Mcommerce_Model_Mcommerce();
                $mcommerce->find(array("value_id" => $value_id));
                if($mcommerce->getId()) {
                    if($mcommerce->getShowSearch() == 1 ) {
                        $data["show_search"] = 1;
                    }
                }

                $data["cover"] = array(
                    "title" => $current_category->getTitle(),
                    "subtitle" => $current_category->getSubtitle(),
                    "picture" => $current_category->getPictureUrl()
                );

                $data["page_title"] = $current_category->getTitle();
                $data["displayed_per_page"] = Folder_Model_Category::DISPLAYED_PER_PAGE;

            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($data);

        }

    }

}