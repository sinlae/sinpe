<?php
class Push_Mobile_EditController extends Application_Controller_Mobile_Default {
	public function createAction() {
        if ($datas = Zend_Json::decode($this->getRequest()->getRawBody())) {
            try {
                if(($this->getApplication()->countByMonth() > 1) && ($this->getApplication()->getSubscriptionName() == "Free")){
                    $message = $this->_("You have already used your 2 notifications included in free plan by month");
                }  else  {
                    $message = new Push_Model_Message();
                    $message->setMessageTypeByOptionValue($this->getCurrentOptionValue()->getOptionId());
                    $sendNow = false;
                    $inputs = array('send_at', 'send_until');

                    foreach($inputs as $input) {
                        if(empty($datas[$input.'_a_specific_datetime'])) {
                            $datas[$input] = null;
                        }
                        else if(empty($datas[$input])) {
                            throw new Exception($this->_('Please, enter a valid date'));
                        }
                        else {
                            $date = new Zend_Date($datas[$input]);
                            $datas[$input] = $date->toString('y-MM-dd HH:mm:ss');
                        }
                    }

                    if(empty($datas['send_at'])) {
                        $sendNow = true;
                        $datas['send_at'] = Zend_Date::now()->toString('y-MM-dd HH:mm:ss');
                    }

                    // Récupère l'option_value en cours
                    $option_value = new Application_Model_Option_Value();

                    $image = $datas['image'];
                    if ($image) {
                        $url = $this->_saveImageContent($image);
                        $message->setImage($url);
                    }else {
                        $datas['cover'] = null;
                    }

                    if(empty($datas['action_value'])) {
                        $datas['action_value'] = null;
                    } else if(!preg_match('/^[0-9]*$/', $datas['action_value'])) {
                        $url = "http://".$datas['action_value'];
                        if(stripos($datas['action_value'], "http://") !== false || stripos($datas['action_value'], "https://") !== false) {
                            $url = $datas['action_value'];
                        }

                        $datas['action_value'] = file_get_contents("http://tinyurl.com/api-create.php?url=".urlencode($url));
                    }

                    $datas['type_id'] = $message->getMessageType();
                    $datas['app_id'] = $this->getApplication()->getId();
                    $datas["send_to_all"] = $datas["topic_receiver"]?0:1;
                    $message->setData($datas)->save();
                    $message->setCover($url)->save();

     				if($message->getMessageType()==1) {
                        if ($sendNow) {
                            $c = curl_init();
                            curl_setopt($c, CURLOPT_URL, $this->getUrl('push/message/send', array('message_id' => $message->getId())));
                            curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);  // Follow the redirects (needed for mod_rewrite)
                            curl_setopt($c, CURLOPT_HEADER, false);         // Don't retrieve headers
                            curl_setopt($c, CURLOPT_NOBODY, true);          // Don't retrieve the body
                            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);  // Return from curl_exec rather than echoing
                            curl_setopt($c, CURLOPT_FRESH_CONNECT, true);   // Always ensure the connection is fresh

                            // Timeout super fast once connected, so it goes into async.
                            curl_setopt($c, CURLOPT_TIMEOUT, 10);
                            curl_exec($c);
                            curl_close($c);

                        }
                    } else {
                        $message->updateStatus('delivered');
                    }
                    $message = $this->_('Your push was successfully added');
                }
                $html = array('success' => 1, 'message' => $message);

            } catch (Exception $e) {
                $html = array('error' => 1, 'message' => $e->getMessage());
            }
            $this->_sendHtml($html);
        }
    }

    // Reference parameter
    protected function _saveImageContent($image) {

        $relativePath = $this->getCurrentOptionValue()->getImagePathTo();
        $fullPath = Application_Model_Application::getBaseImagePath() . $relativePath;
        $ini = file_exists(APPLICATION_PATH . DS . 'configs' . DS . 'app.ini') ? APPLICATION_PATH.DS.'configs'.DS.'app.ini' : APPLICATION_PATH.DS.'configs'.DS.'app.sample.ini';
        $config=parse_ini_file($ini);

        $awsAccessKey = $config["awsAccessKey"];
        $awsSecretKey = $config["awsSecretKey"];
         
         //create s3 client
         $client = new Zend_Service_Amazon_S3($awsAccessKey, $awsSecretKey);
         
         $id_filename = $this->getApplication()->getAppId()."/push-mobile";
         $uploader = new Core_Model_Lib_Uploader();
         $url = $uploader->saveImageContent($image, $id_filename, $config, $relativePath, $fullPath, $client);
         return $url;
    }
}