<?php

class Push_Model_Message extends Core_Model_Default {

    const DISPLAYED_PER_PAGE = 10;

    protected $_is_cachable = false;

    protected $_types = array(
        'iphone' => 'Push_Model_Iphone_Message',
        'android' => 'Push_Model_Android_Message'
    );

    protected $_instances;

    protected $_messageType;

    public function __construct($datas = array()) {
        parent::__construct($datas);
        $this->_db_table = 'Push_Model_Db_Table_Message';

        $this->_initInstances();

        $this->_initMessageType();
    }

    public function delete() {
        $message_id = $this->getId();

        parent::delete();
        $this->getTable()->deleteLog($message_id);
    }

    public function deleteFeature($option_value) {
        $app = $this->getApplication();

        $this->setMessageTypeByOptionValue($option_value);
        $this->getTable()->deleteAllLogs($app->getId(),$this->getMessageType());
        $this->getTable()->deleteAllMessages($app->getId(),$this->getMessageType());

    }

    public function getInstance($type = null) {
        if(!empty($this->_instances[$type])) return $this->_instances[$type];
        else return null;
    }

    public function getInstances() {
        return $this->_instances;
    }

    public function getMessageType() {
        return $this->_messageType;
    }

    public function getMessages() {
        return $this->getTable()->getMessages($this->_messageType);
    }

    public function getTitle() {
        return mb_convert_encoding($this->getData('title'), 'UTF-8', 'UTF-8');
    }

    public function getText() {
        return mb_convert_encoding($this->getData('text'), 'UTF-8', 'UTF-8');
    }

    public function markAsRead($device_uid,$message_id = null) {
        return $this->getTable()->markAsRead($device_uid,$message_id);
    }

    public function markAsDisplayed($device_id, $message_id) {
        $push_delivered_message = new Push_Model_DeliveredMessage();
        $push_delivered_message = $push_delivered_message->find(array("device_uid" => $device_id, "is_read"=> 0, "is_displayed" => 0));
        $push_delivered_message->setIsDisplayed(1);
        $push_delivered_message->save();
        return true;
    }

    public function findByDeviceId($device_id,$offset=0) {
        $push_delivered_message = new Push_Model_DeliveredMessage();
        $push_delivered_message = $push_delivered_message->findAll(array("device_uid" => $device_id));
        $push_delivered_message_id = array();
        foreach ($push_delivered_message as $key => $push_delivered) {
            $push_delivered_message_id[] = $push_delivered->getMessageId();
        }
        $messages = new Push_Model_Message;
        $messages = $messages->findAll(array("message_id" => $push_delivered_message_id),"message_id DESC");
        return $messages;
    }

    public function findByDeviceIdAndAppId($device_id,$app_id,$offset=0) {
        $push_delivered_message = new Push_Model_DeliveredMessage();
        $push_delivered_message = $push_delivered_message->findAll(array("device_uid" => $device_id));
        $push_delivered_message_id = array();
        foreach ($push_delivered_message as $key => $push_delivered) {
            //test message
            $test_message = new Push_Model_Message;
            $test_message = $test_message->findAll(array("app_id"=>$app_id,"message_id" => $push_delivered->getMessageId()));
            if(isset($test_message[0])){
                $push_delivered_message_id[] = $push_delivered->getMessageId();
            }
    }

    $messages = new Push_Model_Message;
        if(isset($push_delivered_message_id[0])){
                $messages = $messages->findAll(array("message_id" => $push_delivered_message_id),"message_id DESC");
        }
        return $messages;
    }


    public function countByDeviceId($device_id) {
        $push_delivered_message = new Push_Model_DeliveredMessage();
        $push_delivered_message = $push_delivered_message->findAll(array("device_uid" => $device_id, "status" => 1, "is_displayed" => 0, "is_read" => 0));
        return count($push_delivered_message);
    }

    public function findLastPushMessage($device_id) {
        $row = $this->getTable()->findLastPushMessage($device_id);
        $this->_prepareDatas($row);
        return $this;
    }

    public function findLastInAppMessage($app_id,$device_id) {
        $subscription = new Topic_Model_Subscription();
        $allowed_categories = $subscription->findAllowedCategories($device_id);

        $row = $this->getTable()->findLastInAppMessage($app_id,$device_id,$allowed_categories);
        $this->_prepareDatas($row);
        return $this;
    }

    public function markInAppAsRead($app_id, $device_id, $device_type) {
        return $this->getTable()->markInAppAsRead($app_id, $device_id, $device_type);
    }
     public function sendOneSignalMessage(){
         $application = new Application_Model_Application();
         $application = $application->find(array("app_id"=>$this->getAppId()));
         $auto = 'Authorization: Basic '.$application->getOnesignalAppKey();
         $content = array(
                            "en" => $this->getText()
                            );
         if($this->getRadius() == null) {
            $fields = array(
                            'app_id' => $application->getOnesignalAppId(),
                            'included_segments' => array('All'),
                            'data' => array("foo" => "bar"),
                            "ios_badgeType"=>"Increase", 
                            "ios_badgeCount"=>1,
                            'contents' => $content
                        );   
            
         } else {
            $radius = $this->getRadius()*1000;
            $fields = array(
                'app_id' => $application->getOnesignalAppId(),
                'included_segments' => array('All'),
                "ios_badgeType"=>"Increase", 
                "ios_badgeCount"=>1,
                'filters'=> array(array("field" => "location", "radius" => $radius, "lat" => $this->getLatitude(), "long" => $this->getLongitude())),
                'data' => array("foo" => "bar"),
                'contents' => $content
            );                
         }

         $fields = json_encode($fields);
                        //var_dump($fields);exit;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                                   $auto));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


                                    // curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                    //                                            $auto));
                    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    // curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    // curl_setopt($ch, CURLOPT_POST, TRUE);
                    // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


                //curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"8d365908-d099-4664-883b-6c0617d278a8\",\n\"contents\": {\"en\": \"English Message\"},\n\"isIos\": true,\n\"included_segments\": [\"All\"]}");
                //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                $response = curl_exec($ch);
                curl_close($ch);
     }
    public function push() {
        $errors = array();
        $this->updateStatus('sending');
        foreach($this->_instances as $type => $instance) {
            $instance->setMessage($this)
                ->push()
            ;
            if($instance->getErrors()) {
                $errors[$instance->getId()] = $instance->getErrors();
            }
        }
        $this->updateStatus('delivered');

        if(!empty($errors)) {
            Zend_Registry::get("logger")->sendException(print_r($errors, true), "push_", false);
        }

        $this->setErrors($errors);

    }

    public function createLog($device, $status, $id = null) {

        if(!$id) $id = $device->getDeviceUid();
        $is_displayed = 0;
//        $is_displayed = $device->getTypeId() == 1 ? 1 : 0;
//        if(!$is_displayed) {
//            $is_displayed = !$this->getLatitude() && !$this->getLongitude();
//        }
        $datas = array(
            'device_id' => $device->getId(),
            'device_uid' => $id,
            'device_type' => $device->getTypeId(),
            'is_displayed' => $is_displayed,
            'message_id' => $this->getId(),
            'status' => $status,
            'delivered_at' => $this->formatDate(null, 'y-MM-dd HH:mm:ss')
        );

        $this->getTable()->createLog($datas);

        return $this;
    }

    public function updateStatus($status) {

        $this->setStatus($status);
        if($status == 'delivered') {
            $this->setDeliveredAt($this->formatDate(null, 'y-MM-dd HH:mm:ss'));
        }

        $this->save();

    }

    public function setMessageType($message_type) {
        $this->_messageType = $message_type;
        return $this;
    }

    public function setMessageTypeByOptionValue($optionValue) {
        $inapp_option_id = $this->getTable()->getInAppCode();
        switch($optionValue) {
            case $inapp_option_id:
                $this->_messageType = 2;
                break;
            default:
                $this->_messageType = 1;
        }
    }

    public function getCoverUrl() {
        $cover_path = Application_Model_Application::getImagePath().$this->getCover();
        $base_cover_path = Application_Model_Application::getBaseImagePath().$this->getCover();
        if($this->getCover() AND file_exists($base_cover_path)) {
            return $cover_path;
        }
        return '';
    }

    public function getInAppCode() {
        return $this->getTable()->getInAppCode();
    }

    protected function _initInstances() {

        if(is_null($this->_instances)) {

            $this->_instances = array();
            foreach($this->_types as $device => $type) {
                $this->_instances[$device] = new $type();
            }
        }

        return $this->_instances;
    }

    public function _initMessageType() {
            if (is_null($this->_messageType)) {
                $this->_messageType = 1;
            }
    }
}