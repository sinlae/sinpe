<?php

class Push_Model_DeliveredMessage extends Core_Model_Default {

	public function __construct($datas = array()) {
        parent::__construct($datas);
        $this->_db_table = 'Push_Model_Db_Table_DeliveredMessage';
    }
}