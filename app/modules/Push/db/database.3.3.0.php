<?php

$this->query("
    ALTER TABLE `push_messages` ADD `type_id` INT(2) NOT NULL DEFAULT '1' AFTER `app_id`;
");

$this->query("
    ALTER TABLE `push_messages` ADD `cover` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `text`;
");

$library = new Media_Model_Library();
$library->setName('Messages')->save();

$icon_paths = array(
    '/inapp_messages/inapp1.png'
);

$icon_id = 0;
foreach($icon_paths as $key => $icon_path) {
    $datas = array('library_id' => $library->getId(), 'link' => $icon_path, 'can_be_colorized' => 1);
    $image = new Media_Model_Library_Image();
    $image->setData($datas)->save();

    if($key == 0) $icon_id = $image->getId();
}

$datas = array(
    'library_id' => $library->getId(),
    'icon_id' => $icon_id,
    'code' => 'inapp_messages',
    'name' => 'In App Messages',
    'model' => '',
    'desktop_uri' => 'push/application/',
    'mobile_uri' => 'push/mobile_list/',
    'only_once' => 1,
    'is_ajax' => 1,
    'position' => 130
);
$option = new Application_Model_Option();
$option->setData($datas)->save();