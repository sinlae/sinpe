<?php
require_once('Stripe/init.php');
class Admin_ApplicationController extends Admin_Controller_Default
{
    public function indexAction() {
        $this->_forward('list');
        return $this;
    }

    public function listAction() {
        if(count($this->getSession()->getAdmin()->getApplications())==1 && $this->getSession()->getAdmin()->getRoleId()!= 1){
            $admin = $this->getSession()->getAdmin();
            $id = $admin->getApplication()->getId();
            $this->getSession()->setAppId($id);
            $this->_redirect('application/customization');
        } else {
            $this->loadPartials();
        }
    }

    public function helpAction() {
        $this->loadPartials();
    }

    public function createpostAction() {

        if($datas = $this->getRequest()->getPost()) {

            try {
                if(empty($datas['name'])) {
                    throw new Exception($this->_('Please, enter an application name'));
                }

                $application = new Application_Model_Application();
                $application->setName($datas['name'])
                    ->setAdminId($this->getSession()->getAdminId())
                    ->addAdmin($this->getSession()->getAdmin())
                ;


                $free_trial_period = System_Model_Config::getValueFor("application_free_trial");
                
                if(is_numeric($free_trial_period)) {
                    $date = new Zend_Date();
                    $date = $date->addDay($free_trial_period);
                    $application->setFreeUntil($date->toString("y-MM-dd HH:mm:ss"));
                }

                $application->save();

                $application_account_option = new Application_Model_Accountoption();
                $application_account_option->setAppId($application->getAppId());
                $application_account_option->save();

                $application_website = new Application_Model_Website();
                $application_website->setAppId($application->getAppId());
                $application_website->save();

                $this->getSession()->showTemplates = true;

                $admin =$this->getSession()->getAdmin();
                if($admin->getRoleId()==2){
                    $admin->setRoleId(3);
                    $admin->save();
                }

                $customer = new Customer_Model_Customer();
                $customer->setAppId($application->getId())
                         ->setPseudo($application->getName(),$application)
                         ->setEmail($admin->getEmail())
                         ->setAdminId($admin->getId())
                         ->setPassword($admin->getPassword(), true);
                $customer->save();         

                $html = array(
                    'url' => $this->getUrl('admin/application/set', array('id' => $application->getId(), 'redirect_to' => 'edit'))
                );

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->_sendHtml($html);

        }

        return $this;

    }

    public function deleteAction() {

        if($id = $this->getRequest()->getParam('id')) {
            try {
                $application = new Application_Model_Application();
                $application->find($id)->setIsActive(0)->save();
                $html = array('success' => '1');
            }
            catch(Exception $e) {
                $html = array(
                    'message' => "Une erreur s'est produite lors de la suppression de votre application",
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->_sendHtml($html);

        }
    }

    public function setAction() {

        if($id = $this->getRequest()->getParam('id')) {

            $application = new Application_Model_Application();
            $application->find($id);
            $admin_ids = $application->getTable()->getAdminIds($application->getId());
            if(!$application->getId() OR !in_array($this->getSession()->getAdmin()->getId(), $admin_ids)) {
                $this->_redirect('admin/application/list');
            }
            else {
                // Passe l'id de l'appli en session
                $this->getSession()->setAppId($id);
                $this->_redirect('application/customization');
            }

            return $this;
        }

    }

    public function upgradeAction() {
        if($id = $this->getRequest()->getParam('id')) {

            $application = new Application_Model_Application();
            $application->find($id);
            $admin_ids = $application->getTable()->getAdminIds($application->getId());
            if(!$application->getId() OR !in_array($this->getSession()->getAdmin()->getId(), $admin_ids)) {
                $this->_redirect('admin/application/list');
            }
            else {
                // Passe l'id de l'appli en session
                $this->getSession()->setAppId($id);
                $this->_redirect('subscription/application/create');
            }

            return $this;
        }
    }

    public function stopsubscriptionAction() {
        if($id = $this->getRequest()->getParam('id')) {

            $application = new Application_Model_Application();
            $application->find($id);
            $admin_ids = $application->getTable()->getAdminIds($application->getId());
            if(!$application->getId() OR !in_array($this->getSession()->getAdmin()->getId(), $admin_ids)) {
                $this->_redirect('admin/application/list');
            }
            else {
                $is_reccurent = $application->getSubscription()->getIsRecurrent();
                $is_reccurent->setValue(0);
                $is_reccurent->save();

                if($application->getSubscription()->getPaymentMethod()=="stripe"){
                    $admin = $this->getSession()->getAdmin();
                    $api_key = Api_Model_Key::findKeysFor('stripe');
                     \Stripe\Stripe::setApiKey($api_key->getSecretKey()); 
                    if($admin->getCustomerStripeId()){
                        $cu = \Stripe\Customer::retrieve($admin->getCustomerStripeId());
                        $cu->delete();
                    }
                    $admin->setCustomerStripeId(NULL)->save();
                }
                $this->_redirect('admin/application/list');
            }
            return $this;
        }
    }

    public function duplicateAction() {

        if($app_id = $this->getRequest()->getParam('id')) {
            $application = new Application_Model_Application();
            $application->find($app_id);
            $admin_ids = array();

            foreach($application->getAdmins() as $admin) $admin_ids[] = $admin;

            if(in_array($this->getSession()->getAdminId(), $admin_ids)) {

                try {
                    $result = $application->duplicate();
                    $html = array(
                        'url' => $this->getUrl('admin/application/list')
                    );
                    $this->_sendHtml($html);
                } catch(Exception $e) {
                    $html = array(
                        'message' => $this->_("Sorry but an error occurred whiled duplicating your application."),
                        'message_button' => 1,
                        'message_loader' => 1
                    );
                    $this->_sendHtml($html);
                }
            }

        }

    }

}
