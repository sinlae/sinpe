<?php

$this->query("
    ALTER TABLE `admin` 
        ADD `address2` VARCHAR(255) NULL AFTER `address`,
        ADD `city` VARCHAR(255) NULL AFTER `address2`,
        ADD `zip_code` VARCHAR(255) NULL AFTER `city`,
        ADD `region_code` VARCHAR(255) NULL AFTER `zip_code`,
        ADD `region` VARCHAR(255) NULL AFTER `region_code`,
        ADD `publication_access_type` ENUM('sources', 'info') NULL DEFAULT NULL AFTER `password`
    ;
");

