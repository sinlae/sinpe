<?php

$config = new System_Model_Config();
$config->find("application_free_trial", "code");

if(!$config->getId()) {

    $data = array(
        "code" => "application_free_trial",
        "label" => "Free trial period",
        "value" => null
    );

    $config->setData($data)
        ->save()
    ;

}