<?php

$configs = array(
    array(
        "code" => "sales_email",
        "label" => "Sales Email Address"
    ),

    array(
        "code" => "sales_name",
        "label" => "Name"
    ),

    array(
        "code" => "sales_tc",
        "label" => "Terms & Conditions"
    )
);

foreach($configs as $data) {

    $config = new System_Model_Config();
    $config->find($data["code"], "code");

    if(!$config->getId()) {
        $config->setData($data)->save();
    }
}

