<?php

$config = new System_Model_Config();
$config->find("sales_tc", "code");

if(!$config->getId()) {

    $data = array(
        "code" => "sales_tc",
        "label" => "Terms & Conditions",
        "value" => null
    );

    $config->setData($data)
        ->save()
    ;

}