<?php

class System_Backoffice_Config_TcController extends System_Controller_Backoffice_Default {

    protected $_codes = array("sales_tc");

    public function loadAction() {

        $html = array(
            "title" => $this->_("Terms & Conditions"),
            "icon" => "fa-file-text",
        );

        $this->_sendHtml($html);

    }


    public function findallAction() {

       // $data = $this->_findconfig();
		$data = array();
        $data["sales_tc"]["label"] = "Terms & Conditions";

 
        $system = new System_Model_Config();
        $termsandconditions = $system->find(array("code" => "sales_tc"));
        $data["sales_tc"]["value"] = $termsandconditions->getValue(); 

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {
                $this->_save($data);

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved")
                );
            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);

        }

    }

}