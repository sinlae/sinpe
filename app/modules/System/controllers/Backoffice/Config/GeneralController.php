<?php

class System_Backoffice_Config_GeneralController extends System_Controller_Backoffice_Default {

    protected $_codes = array("platform_name", "company_name", "company_phone", "company_address", "company_country", "company_vat_number", "system_timezone", "system_currency", "system_default_language", "system_publication_access_type", "application_free_trial","company_siret");

    public function loadAction() {

        $html = array(
            "title" => $this->_("General"),
            "icon" => "fa-home",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

       // $data = $this->_findconfig();
$data = array();
        $timezones = DateTimeZone::listIdentifiers();
        if(empty($timezones)) {
            $locale = Zend_Registry::get("Zend_Locale");
            $timezones = $locale->getTranslationList('TimezoneToTerritory');
        }
        foreach($timezones as $timezone) {
            $data["territories"][$timezone] = $timezone;
        }

        foreach(Core_Model_Language::getCountriesList() as $country) {
            $data["currencies"][$country->getCode()] = $country->getName() . " ({$country->getSymbol()})";
        }

        $countries = $countries = Zend_Registry::get('Zend_Locale')->getTranslationList('Territory', null, 2);;
        asort($countries, SORT_LOCALE_STRING);
        $data["countries"] = $countries;

        $data["platform_name"]["label"] = "Plateform Name";

        $data["company_name"]["label"] = "Company";
        $data["company_siret"]["label"] = "Siret";
        $data["company_phone"]["label"] = "Phone";
        $data["company_address"]["label"] = "Address";
        $data["company_phone"]["label"] = "Phone";
        $data["company_country"]["label"] = "Country";
        $data["company_vat_number"]["label"] = "Vat Number";

        $data["system_timezone"]["label"] = "Timezone";
        $data["system_currency"]["label"] = "Currency";
        $data["system_default_language"]["label"] = "Default Language";

        $data["application_free_trial"]["label"] = "Application free trial";

        $system = new System_Model_Config();
        $system_plateform_name = $system->find(array("code" => "platform_name"));
        $data["platform_name"]["value"] = $system_plateform_name->getValue();

        $system_company_name = $system->find(array("code" => "company_name"));
        $data["company_name"]["value"] = $system_company_name->getValue();
        $system_company_siret = $system->find(array("code" => "company_siret"));
        $data["company_siret"]["value"] = $system_company_siret->getValue();
        $system_company_phone = $system->find(array("code" => "company_phone"));
        $data["company_phone"]["value"] =  $system_company_phone->getValue();
        $system_company_address = $system->find(array("code" => "company_address"));
        $data["company_address"]["value"] =  $system_company_address->getValue();
        $system_company_country = $system->find(array("code" => "company_country"));
        $data["company_country"]["value"] =  $system_company_country->getValue();
        $system_company_vat_number = $system->find(array("code" => "company_vat_number"));
        $data["company_vat_number"]["value"] =  $system_company_vat_number->getValue();

        $system_timezone = $system->find(array("code" => "system_timezone"));
        $data["system_timezone"]["value"] =  $system_timezone->getValue();
        $system_currency = $system->find(array("code" => "system_currency"));
        $data["system_currency"]["value"] =  $system_currency->getValue();
        $system_default_language = $system->find(array("code" => "system_default_language"));
        $data["system_default_language"]["value"] = $system_default_language->getValue();

        $system_application_free_trial = $system->find(array("code" => "application_free_trial"));
        $data["application_free_trial"]["value"] = $system_application_free_trial->getValue();



        $languages = array();
        foreach(Core_Model_Language::getLanguages() as $language) {
            $languages[$language->getCode()] = $language->getName();
        }
        if(!empty($languages) AND count($languages) > 1) {
            $data["languages"] = $languages;
        }

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                if(!empty($data["application_free_trial"]["value"]) AND !is_numeric($data["application_free_trial"]["value"])) {
                    throw new Exception("Free trial period duration must be a numeric value.");
                }
                $this->_save($data);

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved")
                );
            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);

        }

    }

}
