<?php

class Siberian_Version
{
    const TYPE = 'PE';
    const NAME = 'Platform Edition';
    const VERSION = '3.17.1';

    static function is($type) {
        return self::TYPE == strtoupper($type);
    }
}

